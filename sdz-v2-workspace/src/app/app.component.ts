import { Component } from '@angular/core';

@Component({
  selector: 'sdz-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'horizon-scanner-workspace';
}
