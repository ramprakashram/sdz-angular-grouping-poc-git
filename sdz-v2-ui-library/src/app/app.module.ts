import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { WidgetsModule } from '../../projects/sdz-widgets/src/lib/components/widgets/widgets.module'
// import { WidgetsModule } from '@gravityilabs/sdz-widgets'
import { TableModule } from '../../projects/sdz-layouts/src/lib/components/table/table.module'
import { EssentialsModule } from '../../projects/sdz-layouts/src/lib/components/essentials/essentials.module'
// import { EssentialsModule } from '@gravityilabs/sdz-layouts';
import { CardsModule } from '../../projects/sdz-cards/src/lib/components/cards/cards.module'
import { InteractiveWidgetsModule } from '../../projects/sdz-widgets/src/lib/components/interactive-widgets/interactive-widgets.module'
import { NgSelectModule } from '@ng-select/ng-select';
import {SidebarModule} from '../../projects/sdz-layouts/src/lib/components/sidebar/sidebar.module';
import {FormsModule} from "@angular/forms";
import {TopBarModule} from '../../projects/sdz-layouts/src/lib/components/top-bar/top-bar.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    WidgetsModule,
    TableModule,
    EssentialsModule,
    CardsModule,
    InteractiveWidgetsModule,
    NgSelectModule,
    SidebarModule,
    FormsModule,
    TopBarModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
