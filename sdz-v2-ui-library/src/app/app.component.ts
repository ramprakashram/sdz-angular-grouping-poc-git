import { Component, OnInit } from '@angular/core';
import {
  CheckBoxModel, MultiSelectCheckBoxModel, AccordionWithCheckboxModel,
  TableHeaderModel, BadgeModel, MomentLogModel, CategoryFilterModel, HorizontalTabModel, ProgressBarModel
} from '@gravityilabs/sdz-models';
import { SideBarModel } from '../../projects/sdz-models/src/lib/layouts/side-bar-model';

// import { CheckBoxModel,MultiSelectCheckBoxModel,AccordionWithCheckboxModel,TableHeaderModel } from '@gravityilabs/sdz-models'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'sdz-library';

  checkboxes: MultiSelectCheckBoxModel = new MultiSelectCheckBoxModel();
  accordionCheckboxes: AccordionWithCheckboxModel = new AccordionWithCheckboxModel();
  accordionCheckboxesArray: Array<AccordionWithCheckboxModel> = new Array<AccordionWithCheckboxModel>();
  filterTags: Array<CheckBoxModel> = new Array<CheckBoxModel>();
  tableHeader: Array<TableHeaderModel> = new Array<TableHeaderModel>();
  tableData: Array<any> = new Array<any>();
  badgeContent: BadgeModel;
  badgeContentArray: Array<BadgeModel> = new Array<BadgeModel>();
  momentData: MomentLogModel = new MomentLogModel();
  categoryFilters: Array<CategoryFilterModel> = new Array<CategoryFilterModel>();
  currentValue = 7;
  tabData: Array<HorizontalTabModel> = new Array<HorizontalTabModel>();
  hideSideBar = false;
  searchText = '';
  tableContent: Array<any> = new Array<any>();
  lazyLoadCount = 0;
  stopLazyLoad = true;

  showConfirmation = false;
  sideBarData: Array<SideBarModel> = new Array<SideBarModel>();
  progressBar = new Object(ProgressBarModel);
  sdzLogoSrc = 'assets/images/png/StrategyDotZero_dark_background_blank.png';
  clientLogoSrc = 'assets/images/png/clientLogoHeader.png';
  hamburgerUrl = 'assets/images/svg/hamburger_menu.svg';

  constructor() {
    window.addEventListener('resize', this.windowSizeChanged);
  }

  ngOnInit() {
    console.log((new Date(('01/27/2019')).getDate()));
    // console.log((new Date(('01/27/2019')).getUTCDate()));
    // console.log((new Date(('21 July 2019')).getMonth()));
    // console.log((new Date(('01/27/2019')).getUTCMonth()));
    // console.log((new Date(('01/27/2019')).getFullYear()));
    // console.log((new Date(('01/27/2019')).getUTCFullYear()));
    this.dummyDataGeneratorForWidgets();
    this.dummyDataGeneratorForLayouts();
    this.dummyDataGeneratorForMoment();
    this.dummyDataGeneratorForBadge();
    this.dummyDataGeneratorForHorizontalTabs();
    this.dummyDataGeneratorForSideBar();
    this.dummyDataGeneratorForProgressBar();
    this.dummyDataGeneratorForLazyLoadingTable();
  }

  windowSizeChanged = () => {
    // console.log(window.innerWidth);
    // console.log(window.outerWidth);
  }

  dummyDataGeneratorForProgressBar() {
    const progressBaOne = {
      id: 1,
      barText: 'test1',
      isPercentage: true,
      backgroundColor: 'blue',
      leftSideText: 'leftTex',
      leftSideNumber: 50,
      rightSideText: 'righttext ',
      rightSideNumber: 75,
      width: 90,
      height: 15,
      percentageNumber: 50,
      isHorizontal: false,
      // color : "#FFAB00",
      progressBarWidth: 15,
      ProgressBarnumber: 45
    };
    this.progressBar = progressBaOne;
    console.log('this.ProgressBarModel', this.progressBar);
  }

  dummyDataGeneratorForSideBar() {
    this.sideBarData = [
      {
        "name": "Org Explorer",
        "subMenu": [
          {
            "name": "Org Story",
            "url": "#orgStory"
          },
          {
            "name": "Org Pulse",
            "url": "#orgPulse"
          },
          {
            "name": "Business Profile",
            "url": "#businessProfile"
          }
        ]
      },
      {
        "name": "Planning Landscape",
        "subMenu": [
          {
            "name": "Business Plan Explorer",
            "url": "#businessPlanExplorer"
          },
          {
            "name": "Line Of Sight",
            "url": "#lineofsight"
          }
        ]
      },
      {
        "name": "Enterprise Capability Management",
        "subMenu": [
          {
            "name": "Capability Model",
            "url": "#capabilityModel"
          }
        ]
      },
      {
        "name": "Portfolio Planning",
        "subMenu": [
          {
            "name": "Investment Register",
            "url": "#bidRegister"
          }
        ]
      },
      {
        "name": "Portfolio Implementation",
        "subMenu": [
          {
            "name": "Portfolio Overview",
            "url": "p3mboard/portfolio/overview"
          },
          {
            "name": "Portfolio Timeline",
            "url": "#timeline"
          },
          {
            "name": "Portfolio Tracking",
            "url": "#portfolioTracking"
          }
        ]
      },
      {
        "name": "BAU Activity Tracking",
        "subMenu": [],
        "url": "#bauActivityTracking"
      },
      {
        "name": "Horizon Scanning Dashboard",
        "subMenu": [],
        "url": "/hsboard",
      },
      {
        "name": "Governance",
        "subMenu": [],
        "url": "#governance"

      },

      //    {
      //       "name": "angular app link",
      //   "submenu": [],
      //    "url": "/angular/testapp"

      //}
    ];
  }

  dummyDataGeneratorForWidgets() {
    // this.accordionCheckboxes.name = "IMPACT SCORE"
    const temp1: CheckBoxModel = new CheckBoxModel();
    temp1.id = 1;
    temp1.name = 'Regulatory Frameworks';
    temp1.checked = false;

    const temp2: CheckBoxModel = new CheckBoxModel();
    temp2.id = 2;
    temp2.name = 'Regulatory Practice Regulatory Practice Regulatory Practice Regulatory Practice Regulatory Practice';
    temp2.checked = false;

    const temp3: CheckBoxModel = new CheckBoxModel();
    temp3.id = 3;
    temp3.name = 'Regulatory Intelligence';
    temp3.checked = false;

    const temp4: CheckBoxModel = new CheckBoxModel();
    temp4.id = 4;
    temp4.name = 'Regulatory Assurance';
    temp4.checked = false;

    const temp5: CheckBoxModel = new CheckBoxModel();
    temp5.id = 5;
    temp5.name = 'Check Impact';
    temp5.checked = false;

    const temp6: CheckBoxModel = new CheckBoxModel();
    temp6.id = 6;
    temp6.name = 'Validat Impact';
    temp6.checked = false;

    const temp7: CheckBoxModel = new CheckBoxModel();
    temp7.id = 7;
    temp7.name = 'May Impact';
    temp7.checked = false;

    this.checkboxes.title = 'TEST ONE';
    this.checkboxes.checkboxData.push(temp1);
    this.checkboxes.checkboxData.push(temp2);
    this.checkboxes.checkboxData.push(temp3);
    this.checkboxes.checkboxData.push(temp4);
    this.checkboxes.checkboxData.push(temp5);
    this.checkboxes.checkboxData.push(temp6);

    this.accordionCheckboxes.checkboxData.push(temp1);
    this.accordionCheckboxes.checkboxData.push(temp2);
    this.accordionCheckboxes.checkboxData.push(temp3);
    this.accordionCheckboxes.checkboxData.push(temp4);
    this.accordionCheckboxes.checkboxData.push(temp5);
    this.accordionCheckboxes.checkboxData.push(temp6);
    this.accordionCheckboxes.checkboxData.push(temp7);
    this.accordionCheckboxes.name = 'Test Accordion';

    this.accordionCheckboxesArray.push(this.accordionCheckboxes);
    this.accordionCheckboxesArray.push(this.accordionCheckboxes);
    this.accordionCheckboxesArray.push(this.accordionCheckboxes);
    this.accordionCheckboxesArray.push(this.accordionCheckboxes);
    this.accordionCheckboxesArray.push(this.accordionCheckboxes);

    this.filterTags.push(temp1);
    this.filterTags.push(temp2);
    this.filterTags.push(temp3);
    this.filterTags.push(temp4);
    this.filterTags.push(temp5);
    this.filterTags.push(temp6);
    this.filterTags.push(temp7);
  }

  dummyDataGeneratorForLayouts() {
    // let head1 : TableHeaderModel = new TableHeaderModel("Trend Title",true)
    const head1: TableHeaderModel = new TableHeaderModel();
    head1.name = 'Trend Title';
    head1.isSortingEnabled = true;
    head1.key = 'title';
    // head1.style = {
    //   'color' : 'red'
    // }

    // let head2 : TableHeaderModel = new TableHeaderModel("Category",true)
    const head2: TableHeaderModel = new TableHeaderModel();
    head2.name = 'Category';
    head2.isSortingEnabled = true;
    head2.key = 'category';
    // head2.style = {
    //   'color' : 'red'
    // }

    const head3: TableHeaderModel = new TableHeaderModel('Date Added', true, '', true, true);
    // head3.name = "Date Added"
    // head3.isSortingEnabled = true
    // head3.key = "dateAdded"

    // let head4 : TableHeaderModel = new TableHeaderModel("Shared With",false)
    const head4: TableHeaderModel = new TableHeaderModel();
    head4.name = 'Shared With';
    head4.isSortingEnabled = true;
    head4.key = 'sharedWith';
    // head4.style = {
    //   'color' : 'red'
    // }

    // let head5 : TableHeaderModel = new TableHeaderModel("Status",false)
    const head5: TableHeaderModel = new TableHeaderModel();
    head5.name = 'Status';
    head5.isSortingEnabled = true;
    head5.key = 'status';
    // head5.style = {
    //   'color' : 'red'
    // }

    // let head6 : TableHeaderModel = new TableHeaderModel("Action",false)
    const head6: TableHeaderModel = new TableHeaderModel();
    head6.name = 'Action';
    head6.isSortingEnabled = true;
    head6.key = 'action';
    // head6.style = {
    //   'color' : 'red'
    // }

    this.tableHeader.push(head1);
    this.tableHeader.push(head2);
    this.tableHeader.push(head3);
    this.tableHeader.push(head4);
    this.tableHeader.push(head5);
    this.tableHeader.push(head6);

    const data1 = [
      'Dysfunction and uncertainty in the national political environment may change our fate',
      'Politics',
      '21/01/2019',
      'John Brown',
      `<img src='http://atlas-content-cdn.pixelsquid.com/stock-images/pointer-computer-icon-B5mDxM2-600.jpg' style='width: 30px'><div  id="ram"></div>`,
      'Await',
      'TRD0012'
    ];

    const data2 = ['Increase in frequency of major natural disasters and damage to major property and infrastructure', 'Social',
      '18/01/2019', 'Peter Ott', '<img src=\'http://atlas-content-cdn.pixelsquid.com/stock-images/pointer-computer-icon-B5mDxM2-600.jpg\' style=\'width: 30px\'>', 'Await', 'TRD0011'];

    const data3 = ['An increase in vulnerability as a digitally connected society towards systemic risks', 'Technology', '17/02/1994', 'Jim Sigourney', '<img src=\'http://atlas-content-cdn.pixelsquid.com/stock-images/pointer-computer-icon-B5mDxM2-600.jpg\' style=\'width: 30px\'>', 'Async', 'TRD001'];

    const data4 = ['An increase in vulnerability as a digitally connected society towards systemic risks', 'Technology', '18/01/2019', 'Jim Sigourney', '<img src=\'http://atlas-content-cdn.pixelsquid.com/stock-images/pointer-computer-icon-B5mDxM2-600.jpg\' style=\'width: 30px\'>', 'Async', 'TRD002'];

    const data5 = ['An increase in vulnerability as a digitally connected society towards systemic risks', 'Technology', '18/01/2019', 'Jim Sigourney', '<img src=\'http://atlas-content-cdn.pixelsquid.com/stock-images/pointer-computer-icon-B5mDxM2-600.jpg\' style=\'width: 30px\'>', 'Async', 'TRD003'];

    const data6 = ['An increase in vulnerability as a digitally connected society towards systemic risks', 'Technology', '18/01/2019', 'Jim Sigourney', '<img src=\'http://atlas-content-cdn.pixelsquid.com/stock-images/pointer-computer-icon-B5mDxM2-600.jpg\' style=\'width: 30px\'>', 'Async', 'TRD004'];

    const data7 = ['An increase in vulnerability as a digitally connected society towards systemic risks', 'Technology', '18/01/2019', 'Jim Sigourney', '<img src=\'http://atlas-content-cdn.pixelsquid.com/stock-images/pointer-computer-icon-B5mDxM2-600.jpg\' style=\'width: 30px\'>', 'Async', 'TRD005'];

    const data8 = ['An increase in vulnerability as a digitally connected society towards systemic risks', 'Technology', '18/01/2019', 'Jim Sigourney', '<img src=\'http://atlas-content-cdn.pixelsquid.com/stock-images/pointer-computer-icon-B5mDxM2-600.jpg\' style=\'width: 30px\'>', 'Async', 'TRD006'];

    const data9 = ['An increase in vulnerability as a digitally connected society towards systemic risks', 'Technology', '18/01/2019', 'Jim Sigourney', '<img src=\'http://atlas-content-cdn.pixelsquid.com/stock-images/pointer-computer-icon-B5mDxM2-600.jpg\' style=\'width: 30px\'>', 'Async', 'TRD007'];

    const data10 = ['An increase in vulnerability as a digitally connected society towards systemic risks', 'Technology', '18/01/2019', 'Jim Sigourney', '<img src=\'http://atlas-content-cdn.pixelsquid.com/stock-images/pointer-computer-icon-B5mDxM2-600.jpg\' style=\'width: 30px\'>', 'Async', 'TRD008'];

    const data11 = ['An increase in vulnerability as a digitally connected society towards systemic risks', 'Technology', '18/01/2019', 'Jim Sigourney', '<img src=\'http://atlas-content-cdn.pixelsquid.com/stock-images/pointer-computer-icon-B5mDxM2-600.jpg\' style=\'width: 30px\'>', 'Async', 'TRD009'];

    const data12 = ['An increase in vulnerability as a digitally connected society towards systemic risks', 'Technology', '18/01/2019', 'Jim Sigourney', '<img src=\'http://atlas-content-cdn.pixelsquid.com/stock-images/pointer-computer-icon-B5mDxM2-600.jpg\' style=\'width: 30px\'>', 'Async', 'TRD0010'];

    this.tableData.push(data1);
    // this.tableData.push(data2);
    // this.tableData.push(data3);
    // this.tableData.push(data4);
    // this.tableData.push(data5);
    // this.tableData.push(data6);
    // this.tableData.push(data7);
    // this.tableData.push(data8);
    // this.tableData.push(data9);
    // this.tableData.push(data10);
    // this.tableData.push(data11);
    // this.tableData.push(data12);

    const catTemp1: CategoryFilterModel = new CategoryFilterModel();
    catTemp1.columnNumber = 1;
    catTemp1.filter = new Array<string>();
    // catTemp1.filter.push("Politics")
    // catTemp1.filter.push("Social")

    const catTemp2: CategoryFilterModel = new CategoryFilterModel();
    catTemp2.columnNumber = 2;
    catTemp2.filter = new Array<string>();
    catTemp2.filter.push('17/02/1994');
    // catTemp1.filter.push("Social")


    this.categoryFilters.push(catTemp1);
    console.log('categoryFilters', this.categoryFilters);
    // this.categoryFilters.push(catTemp2)


  }

  dummyDataGeneratorForBadge() {
    this.badgeContent = new BadgeModel('Ramprakash', '#2EB3EE');
    // this.badgeContent.name = 'Ramprakash';
    // this.badgeContent.bgColor = '#2EB3EE';

    const tempBatch1: BadgeModel = new BadgeModel('Ram Prakash', '#2EB3EE');
    // tempBatch1.name = "Ram Prakash"
    // tempBatch1.color = "#2EB3EE"

    const tempBatch2: BadgeModel = new BadgeModel('Sin Ashok', 'red');
    // tempBatch2.name = "Sin Ashok"
    // tempBatch2.color = "red"

    // let tempBatch3 : BadgeModel = new BadgeModel()
    // tempBatch3.name = "Ramya Kannan"
    // tempBatch3.color = "blue"
    //
    // let tempBatch4 : BadgeModel = new BadgeModel()
    // tempBatch4.name = "Jerusha Ruban"
    // tempBatch4.color = "green"
    //
    // let tempBatch5 : BadgeModel = new BadgeModel()
    // tempBatch5.name = "Satheesh Kumar"
    // tempBatch5.color = "grey"
    //
    // let tempBatch6 : BadgeModel = new BadgeModel()
    // tempBatch6.name = "Ajay Vijay"
    // tempBatch6.color = "pink"

    this.badgeContentArray.push(tempBatch1);
    this.badgeContentArray.push(tempBatch2);
    // this.badgeContentArray.push(tempBatch3)
    // this.badgeContentArray.push(tempBatch4)
    // this.badgeContentArray.push(tempBatch5)
    // this.badgeContentArray.push(tempBatch6)
  }

  dummyDataGeneratorForMoment() {
    this.momentData.id = 123;
    this.momentData.title = 'Alice Jones';
    this.momentData.subtitle = 'We need to fous on this strategy more for this year';
    this.momentData.buttonText = 'Threat Strategy';
    this.momentData.momentText = '20th February,2019';
    this.momentData.isRoundedBorder = true;
    this.momentData.backgroundColor = '#F3F7F9';
  }

  dummyDataGeneratorForHorizontalTabs() {
    this.tabData.push(new HorizontalTabModel('tab1', 'Category'));
    this.tabData.push(new HorizontalTabModel('tab2', 'Business Plan'));
    console.log('tab data app.ts', this.tabData);
  }

  dummyDataGeneratorForLazyLoadingTable() {
    const tableContentOne = [
      {
        data: 'Dummy Row For Table Project/Program Name Placeholder 2',
        id: '1',
        style: {
          width: '40%'
        }
      },
      {
        data: 'Sponsor 2',
        id: '2',
        largeDeviceWidth: '10%',
        mediumDeviceWidth: '20%',
        
      },
      {
        data: 'Business Unit 2',
        id: '3',
        largeDeviceWidth: '10%',
        mediumDeviceWidth: '20%',
        
      },
      {
        data: 'Manager 2',
        id: '4',
        largeDeviceWidth: '10%',
        mediumDeviceWidth: '20%',
        
      },
      {
        data: 'Overall Status 2',
        id: '5',
        largeDeviceWidth: '10%',
        mediumDeviceWidth: '20%',
        
      },
      {
        data: 'Total Budget 2',
        id: '6',
        largeDeviceWidth: '10%',
        mediumDeviceWidth: '20%',
        
      },
    ];;

    const tableContentTwoWithoutStyle = [
      {
        data: ''
      },
      {
        data: ''
      },
      {
        data: ''
      },
      {
        data: ''
      },
      {
        data: ''
      },
      {
        data: ''
      },
    ];

    this.tableContent.push(tableContentOne);
    this.tableContent.push(tableContentOne);
    // this.tableContent.push(tableContentTwoWithoutStyle);
    this.tableContent.push(tableContentOne);
    this.tableContent.push(tableContentOne);
    this.tableContent.push(tableContentOne);
    this.tableContent.push(tableContentOne);
  }

  selectedTab(event) {
    console.log('selected tab', event);
  }

  selectedCheckbox(event) {
  }

  selectedAccordionCheckboxes(event) {
    // console.log("event",event)
  }

  deletedTag(event) {
    const index = 0;
    this.filterTags.forEach((eachTag) => {
      if (eachTag.id == event.id) {
        this.filterTags.splice(index, 1);
      }
    });
    // console.log("deleted tag parent",this.filterTags)
  }

  selectedValue(event) {
    console.log('selectedValue', event);
    this.currentValue = event;
  }

  tableTrigger(event) {
    console.log('tableTrigger', event);
  }

  dropdownFilters(event) {
    console.log('dropdownFilters', event);
  }

  checkbox(event
    :
    boolean
  ) {
    console.log('event', event);
  }

  selectedSliderOneValue(event
    :
    number
  ) {
    console.log('selectedSliderOneValue', event);
  }

  selectedSliderTwoValue(event
    :
    number
  ) {
    console.log('selectedSliderTwoValue', event);
  }

  showConfirmationBox() {
    this.showConfirmation = true;
  }

  loadNextSetOfData(event) {
    // console.log('count', this.lazyLoadCount);

    if (this.lazyLoadCount < 10) {
      const tableContentOne = [
        {
          data: 'Dummy Row For Table Project/Program Name Placeholder 2',
          id: '1',
          style: {
            width: '40%'
          }
        },
        {
          data: 'Sponsor 2',
          id: '2',
          largeDeviceWidth: '10%',
          mediumDeviceWidth: '20%',
          
        },
        {
          data: 'Business Unit 2',
          id: '3',
          largeDeviceWidth: '10%',
          mediumDeviceWidth: '20%',
          
        },
        {
          data: 'Manager 2',
          id: '4',
          largeDeviceWidth: '10%',
          mediumDeviceWidth: '20%',
          
        },
        {
          data: 'Overall Status 2',
          id: '5',
          largeDeviceWidth: '10%',
          mediumDeviceWidth: '20%',
          
        },
        {
          data: 'Total Budget 2',
          id: '6',
          largeDeviceWidth: '10%',
          mediumDeviceWidth: '20%',
          
        },
      ];

      this.lazyLoadCount++;

      setTimeout(() => {
        this.tableContent = new Array();
        this.tableContent.push(tableContentOne);
        this.tableContent.push(tableContentOne);
        this.tableContent.push(tableContentOne);
        this.tableContent.push(tableContentOne);
        this.tableContent.push(tableContentOne);
        this.tableContent.push(tableContentOne);
      }, 200);

    } else {
      this.stopLazyLoad = false;
    }
  }

  columnClicked(event: any) {
    console.log('col clicked', event);
  }

  hamburgerTriggered() {
    this.hideSideBar = !this.hideSideBar;
  }
}
