# SdzWidgets

SDZ Widgets Library holds widgets like components like : 

[1] Multiselect Checkbox

[2] Accordion With Checkbox

[3] User Badges & Groups

[4] Filter Tags

[5] Step Slider

[6] Dropdown Filter Accordion

[7] Horizontal Tabs

[8] Confirmation Box

[9] Dual Step Slider (IE Support Needed)

This library was generated with Angular version 7.2.0.

## Build

Run `ng build --project=sdz-widgets` to build the project.
After building, files will be present in the dist/sdz-widgets

## Publishing

After building the library.

Navigate to dist folder.

Login in to npm account, if it you have not logged in before.

To login to npm account,
run `npm login` and enter your username,password and email.

Check in to Organisation Account,
run `npm init --scope=@gravityilabs`

Finally publish the current package,
run `npm publish`

## Library Dependency

Make sure you install the following SDZ Libraries, before installing sdz-widgets

1.sdz-models

To install latest sdz-models ==> run `npm i @gravityilabs/sdz-models`

To install specific version of sdz-models ==> run `npm i @gravityilabs/sdz-models@VERSION_NUMBER`

## Version Details

[0.0.1]     Added Multiselect Checkbox component,Accordion component and user badge component

[0.0.2]     Added FilterTags Component and UserBadge component along with few fixes for Multiselect component

[0.0.3]     Added Step Slider component

[0.0.4]     Added Dropdown Filter Accordion Component and added support to filter tags,accordion components to support multiple modes.

[0.0.5]     Fixed sliding issue in Step Slider

[0.0.6]     Added supported for updated User batch model to support bgColor and textColor

[0.0.7]     Added support for dropdown accordion to have its own Styling

[0.0.8]     Fixed font-family styling issues in workspace

[0.0.9]     Added Horizontal Tabs Component

[0.0.10]    Added No data available dropdown to dropdown-accordion component.

[0.0.11]    Added fixes to float dropdown filter accordion and made styling changes to match ng-select.

[0.0.12]    Added spacedNext,bgColor,borderStyle and checkboxColor properties to configure multiselect checkbox.

[0.0.13]    Added disabled Mode feature Multiselect Checkbox Component

[0.0.14]    Removed disabled Mode and added maxSelectableCheckboxes to MultiSelect Checkbox Component

[0.0.15]    Added display count to Horizontal Tabs Component.

[0.0.16]    Made Display Count in Horizontal Tabs Component as Optional.

[0.0.17]    Fixed IE Issues for Step Slider Component.

[0.0.18]    Styling Update for Step Slider Component to support IE.

[0.0.19]    Added Working Skeleton for Dual Step Slider P.S: Needs Styling update and I.E Support.

[0.0.20]    Added Disabled Feature to Dual Step Slider.

[0.0.21]    Added Z-Index to Dropdown Filter Accordion.

[0.0.22]    Fixed Dropdown UI Issues in Dropdown Filter Accordion by adding Z-Index.

[0.0.23]    Added UI Fixes for Dropdown Filter Accordion.

[0.0.24]    Fixed UI for Dropdown Accordion and Accordion with Checkbox.

[0.0.25]    Added diameter and styling updates to User Badge Component.

[0.0.26]    Added Text Size option and re-rendering of component upon object change.

[0.0.27]    Fixed Issue in Filter Tags Breaking down on IE.

[0.0.28]    Added Confirmation Box Component.

[0.0.29]    Added Default Buttons for Confirmation Box.

[0.0.30]    Added Popup Animation to Confirmation Box.

[0.0.31]    Fixed Size Issues in Confirmation Box.

[0.0.32]    Changed Background blur effect to opacity in Configuration Box Component.

[0.0.33]    Changed Checkbox positioning in accordion component.

[0.0.34]    Added title alignment option for checkbox component.

[0.0.35]    Added Styling update for Filter Tags.

[0.0.36]    Added configurable border, background color and text color for Filter Tags.

[0.0.37]    Added support for filter tags to remove based on parent ID.

[0.0.38]    Added padding fixes for filter tags.

[0.0.39]    Added support for confirmation box to render html tags

[0.0.40]    Added name feature as optional in user badge component.

[0.0.41]    Updated arrow and tick icon to have click functionality in accordion component.
