import { TestBed } from '@angular/core/testing';

import { SdzWidgetsService } from './sdz-widgets.service';

describe('SdzWidgetsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SdzWidgetsService = TestBed.get(SdzWidgetsService);
    expect(service).toBeTruthy();
  });
});
