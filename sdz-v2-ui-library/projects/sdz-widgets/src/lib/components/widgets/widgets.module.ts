import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MultiselectCheckboxComponent } from './components/multiselect-checkbox/multiselect-checkbox.component';
import { AccordionWithCheckboxComponent } from './components/accordion-with-checkbox/accordion-with-checkbox.component';
import { FilterTagsComponent } from './components/filter-tags/filter-tags.component';
import { FilterTagCardComponent } from './components/filter-tags/components/filter-tag-card/filter-tag-card.component';
import { ExtendedTagsDropdownComponent } from './components/filter-tags/components/extended-tags-dropdown/extended-tags-dropdown.component';
import { UserBadgeComponent } from './components/user-badge/user-badge.component';
import { UserBadgeGroupComponent } from './components/user-badge-group/user-badge-group.component';
import { DropdownFilterAccordionComponent } from './components/dropdown-filter-accordion/dropdown-filter-accordion.component';
import { HorizontalTabsComponent } from './components/horizontal-tabs/horizontal-tabs.component'

@NgModule({
  declarations: [
    MultiselectCheckboxComponent,
    AccordionWithCheckboxComponent,
    FilterTagsComponent,
    FilterTagCardComponent,
    ExtendedTagsDropdownComponent,
    UserBadgeComponent,
    UserBadgeGroupComponent,
    DropdownFilterAccordionComponent,
    HorizontalTabsComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    MultiselectCheckboxComponent,
    AccordionWithCheckboxComponent,
    FilterTagsComponent,
    UserBadgeComponent,
    UserBadgeGroupComponent,
    DropdownFilterAccordionComponent,
    HorizontalTabsComponent
  ]
})
export class WidgetsModule { }
