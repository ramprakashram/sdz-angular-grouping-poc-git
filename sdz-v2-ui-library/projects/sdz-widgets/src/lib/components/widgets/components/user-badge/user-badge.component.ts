import {Component, OnInit, Input, OnChanges, SimpleChanges} from '@angular/core';
import { BadgeModel } from '@gravityilabs/sdz-models';

@Component({
  selector: 'sdzl-widget-user-badge',
  templateUrl: './user-badge.component.html',
  styleUrls: ['./user-badge.component.scss']
})
export class UserBadgeComponent implements OnInit, OnChanges {

  @Input() batchContent: BadgeModel;
  @Input() diameter: string;
  @Input() textSize: string;
  @Input() showName: boolean;
  @Input() showNameTextColor: string;
  @Input() showNameTextSize: string;
  // @Output() batchWidth = new EventEmitter<number>()

  constructor() { }

  singleText: boolean;
  displayText: string;

  ngOnInit() {
    // let width = document.getElementById('user-badge').offsetWidth
    // this.batchWidth.emit(width)
    this.batchLetterParser();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.batchContent) {
      this.batchLetterParser();
    }
  }

  batchLetterParser() {
    if (this.batchContent && this.batchContent.name && this.batchContent.name.length > 0) {
      const batchTextSegments = this.batchContent.name.split(' ');
      if (batchTextSegments.length > 1) {
        // Batch Text Contains more than one word
        this.displayText = batchTextSegments[0][0].toUpperCase();
        this.displayText += batchTextSegments[batchTextSegments.length - 1][0].toUpperCase();
      } else {
        // Batch Text Contains only one word
        this.singleText = true;
        this.displayText = this.batchContent.name[0].toUpperCase();
      }
    }
  }

}
