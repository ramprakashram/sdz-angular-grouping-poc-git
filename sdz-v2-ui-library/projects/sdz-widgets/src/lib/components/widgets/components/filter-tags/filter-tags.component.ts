import {Component, OnInit, Input, Output, EventEmitter, SimpleChanges, OnChanges} from '@angular/core';
import {CheckBoxModel} from '@gravityilabs/sdz-models';

@Component({
  selector: 'sdzl-widget-filter-tags',
  templateUrl: './filter-tags.component.html',
  styleUrls: ['./filter-tags.component.scss']
})
export class FilterTagsComponent implements OnInit, OnChanges {

  @Input() filterTags: Array<CheckBoxModel> = new Array<CheckBoxModel>();
  @Input() borderStyle: string;
  @Input() bgColor: string;
  @Input() textColor: string;
  // Hides Selected Filters String displaying on left side
  @Input() hideText: boolean;
  @Output() deletedTag = new EventEmitter<CheckBoxModel>();

  constructor() {
  }

  // tests = [1, 2, 3, 4, 5, 6]

  extendedTags: Array<CheckBoxModel> = new Array<CheckBoxModel>();
  displayTags: Array<CheckBoxModel> = new Array<CheckBoxModel>();
  availableWidth = 0;
  currentWidth = 0;
  showExtendedTagDropdown = false;
  left = 0;
  top = 0;

  ngOnInit() {
    this.availableWidth = document.getElementById('filter').offsetWidth - 170;
  }

  ngOnChanges(change: SimpleChanges) {
    if (change.filterTags) {
      if (!this.hideText) {
        this.availableWidth = document.getElementById('filter').offsetWidth - 170;
      } else if (this.hideText) {
        this.availableWidth = document.getElementById('filter').offsetWidth - 80;
        console.log(this.availableWidth);

      }
      this.accommodateData();
    }
  }

  accommodateData() {
    this.currentWidth = 0;
    let count = 0;
    this.displayTags = [];
    this.extendedTags = [];
    this.filterTags.forEach(() => {
      this.currentWidth += 140;
      if (this.currentWidth >= this.availableWidth) {
        this.displayTags = this.filterTags.slice(0, count);
        this.extendedTags = this.filterTags.slice(count, this.filterTags.length);
        return;
      }
      count++;
    });
    if (this.displayTags && this.displayTags.length === 0) {
      this.displayTags = this.filterTags;
    }
  }

  // getElementWidth(event, index) {
  //   this.currentWidth += (event + 30)
  //   if (this.currentWidth > this.availableWidth) {
  //     let temp = this.filterTags
  //     this.extendedTags = temp.slice(index - 1, temp.length)
  //     this.filterTags = temp.slice(0, index - 1)
  //     let temp1 = this.filterTags
  //     this.ref.detectChanges()
  //   }
  // }

  // getCurrentWidth() {
  //   if (this.count < this.tests.length - 1) {
  //     this.currentWidth += document.getElementById('test').offsetWidth
  //     this.count++
  //     return this.currentWidth
  //   } else {
  //     this.count = 0;
  //     this.currentWidth = 0;
  //     this.currentWidth += document.getElementById('test').offsetWidth
  //     return this.currentWidth
  //   }

  // }

  toggleDropdown() {
    this.showExtendedTagDropdown = !this.showExtendedTagDropdown;
    // this.left = event.x - 225
    // this.top = event.y + 5
  }

  removeTagFromDisplayTags(tag: CheckBoxModel) {
    if (this.displayTags && this.displayTags.length > 0) {
      let index = 0;
      this.displayTags.forEach((eachTag) => {
        if (eachTag.id === tag.id) {
          if (eachTag.parentId) {
            if (eachTag.parentId === tag.parentId) {
              this.displayTags.splice(index, 1);
              this.currentWidth = 0;
              this.popFirstElementFromExtendedToFilter();
              this.emitDeletedTag(tag);
              return;
            }
          } else {
            this.displayTags.splice(index, 1);
            this.currentWidth = 0;
            this.popFirstElementFromExtendedToFilter();
            this.emitDeletedTag(tag);
            return;
          }
        }
        index++;
      });
    }
  }

  removeTagFromExtendedTags(tag: CheckBoxModel) {
    if (this.extendedTags && this.extendedTags.length > 0) {
      let index = 0;
      this.extendedTags.forEach((eachTag) => {
        if (eachTag.id === tag.id) {
          this.extendedTags.splice(index, 1);
          this.currentWidth = 0;
          this.emitDeletedTag(tag);
          return;
        }
        index++;
      });
    }
  }

  popFirstElementFromExtendedToFilter() {
    if (this.extendedTags && this.extendedTags.length > 0 && this.filterTags) {
      this.displayTags.push(this.extendedTags[0]);
      this.extendedTags.splice(0, 1);
    }
  }

  emitDeletedTag(tag: CheckBoxModel) {
    if (tag.checked) {
      tag.checked = false;
      this.deletedTag.emit(tag);
    } else {
      this.deletedTag.emit(tag);
    }
  }

}
