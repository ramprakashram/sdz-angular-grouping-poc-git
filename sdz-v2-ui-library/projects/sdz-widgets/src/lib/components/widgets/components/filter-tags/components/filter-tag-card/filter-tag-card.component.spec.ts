import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterTagCardComponent } from './filter-tag-card.component';

describe('FilterTagCardComponent', () => {
  let component: FilterTagCardComponent;
  let fixture: ComponentFixture<FilterTagCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilterTagCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterTagCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
