import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { CheckBoxModel } from '@gravityilabs/sdz-models'

@Component({
  selector: 'sdzl-widget-filter-tag-card',
  templateUrl: './filter-tag-card.component.html',
  styleUrls: ['./filter-tag-card.component.scss']
})
export class FilterTagCardComponent implements OnInit {

  @Input() tag: CheckBoxModel = new CheckBoxModel();
  @Input() textColor: string;
  @Input() bgColor: string;
  @Input() borderStyle: string;
  @Output() removedTag =  new EventEmitter<CheckBoxModel>();
  @Output() cardSchema = new EventEmitter();

  constructor() { }

  filterCardWidth: number;
  ngOnInit() {
    this.filterCardWidth = document.getElementById('filter-card').offsetWidth;
    // ("filter card width",this.filterCardWidth)
    this.cardSchema.emit(this.filterCardWidth)
  }

  removeTag() {
    this.removedTag.emit(this.tag);
  }

}
