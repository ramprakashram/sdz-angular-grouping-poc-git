import {Component, OnInit, Input, Output, EventEmitter, SimpleChanges, OnChanges} from '@angular/core';
import { CheckBoxModel, MultiSelectCheckBoxModel , AccordionWithCheckboxModel } from '@gravityilabs/sdz-models';
import { trigger , state, style, animate, transition} from '@angular/animations';

@Component({
  selector: 'sdzl-widget-accordion-with-checkbox',
  templateUrl: './accordion-with-checkbox.component.html',
  styleUrls: ['./accordion-with-checkbox.component.scss'],
  animations : [
    trigger('arrowTrigger', [
      state('show-rightMode', style({
        top : '-2px',
        right: '8px',
        transform: 'rotate(180deg)'
      })),
      state('hide-rightMode', style({
        top : '-6px',
        right: '15px',
        transform: 'rotate(270deg)'
      })),
      state('show-leftMode', style({
        top : '-2px',
        left : '10px',
        transform: 'rotate(180deg)'
      })),
      state('hide-leftMode', style({
        top : '-6px',
        left : '10px',
        transform: 'rotate(270deg)'
      })),
      // tslint:disable-next-line: max-line-length
      transition('show-rightMode => hide-rightMode,hide-rightMode => show-rightMode,show-leftMode => hide-leftMode,hide-leftMode => show-leftMode', [
        animate('0.2s')
      ])
    ])
  ]
})
export class AccordionWithCheckboxComponent implements OnChanges {

  constructor() { }

  @Input() accordionCheckboxes: AccordionWithCheckboxModel = new AccordionWithCheckboxModel();
  // tslint:disable-next-line:max-line-length
  // Enable tick adds a tick icon to right side of accordion and is enabled when any of checkbox is selected and it mirrors the checkbox i.e checkbox will be on right and text will be on left
  @Input() enableTick: boolean;
  // @Input() isDataAvailable: boolean;
  @Input() bgColor: string;
  @Output() selectedAccordionCheckboxes = new EventEmitter<AccordionWithCheckboxModel>();
  @Output() currentSelectedAccordionCheckbox = new EventEmitter<CheckBoxModel>();

  toggleData: boolean = false;
  // enableMirrorMode : boolean = false
  checkboxes : MultiSelectCheckBoxModel = new MultiSelectCheckBoxModel();
  id: string;

  // ngOnInit() {
  // }

  ngOnChanges(change: SimpleChanges) {
    if (change.accordionCheckboxes) {
      this.id = Math.random().toString();
    }
  }


  showData() {
    this.toggleData = !this.toggleData;
    // var content = event.target.nextSibling
    // var test = content
    const content = document.getElementById(this.id);

    if (content.style.maxHeight) {
      content.style.maxHeight = null;
    } else {
      content.style.maxHeight = content.scrollHeight + 'px';
    }
  }

  selectedCheckboxes(event) {
    const accordionData: AccordionWithCheckboxModel = new AccordionWithCheckboxModel();
    accordionData.name = this.accordionCheckboxes.name;
    accordionData.checkboxData = new Array<CheckBoxModel>();
    accordionData.checkboxData = event;
    // if (accordionData && accordionData.checkboxData && accordionData.checkboxData.length === 0 ) {
    //   this.isDataAvailable = false;
    // } else {
    //   this.isDataAvailable = true;
    // }
    this.selectedAccordionCheckboxes.emit(accordionData);
  }

  currentSelectedCheckbox(checkbox: CheckBoxModel) {
    checkbox.type = 'accordion';
    checkbox.parentId = this.accordionCheckboxes.id;
    this.currentSelectedAccordionCheckbox.emit(checkbox);
  }

  isAnyCheckboxesSelected() {
    if (this.accordionCheckboxes && this.accordionCheckboxes.checkboxData && this.accordionCheckboxes.checkboxData.length > 0) {
      for (const eachCheckbox of this.accordionCheckboxes.checkboxData) {
        if (eachCheckbox.checked) {
          return true;
        }
      }
      return false;
    }
    return false;
  }

}
