import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { CheckBoxModel, MultiSelectCheckBoxModel } from '@gravityilabs/sdz-models';

@Component({
  selector: 'sdzl-widget-multiselect-checkbox',
  templateUrl: './multiselect-checkbox.component.html',
  styleUrls: ['./multiselect-checkbox.component.scss']
})
export class MultiselectCheckboxComponent implements OnInit {

  constructor() { }

  @Input() checkbox: MultiSelectCheckBoxModel;
  // mirrorMode when enabled places checkbox to right end and text to left end
  @Input() mirrorMode: boolean;
  // spacedNext when enabled uses justify-content: flex-start to place checkbox first and text immediately next to the checkbox
  @Input() spacedNext: boolean;
  // Optional Background Color
  @Input() bgColor: string;
  // Optional Border style for each checkbox
  @Input() borderStyle: string;
  // Optional Checkbox color
  @Input() checkboxColor: string;
  // Allows only selecting checkboxes upto this maxSelectableCheckboxes count, if its reaches the count,then other checkboxes are disabled.
  @Input() maxSelectableCheckboxes: number;
  // Align Title to left
  @Input() isTitleAlignLeft: boolean;
  // Emits array of selected checkboxes
  @Output() selectedCheckboxes = new EventEmitter<Array<CheckBoxModel>>();
  // Emits current selected checkbox
  @Output() currentSelectedCheckbox = new EventEmitter<CheckBoxModel>();

  outputArr: Array<CheckBoxModel> = new Array<CheckBoxModel>();

  ngOnInit() {
  }

  checkboxClicked(id) {
    this.checkbox.checkboxData.forEach((element) => {
      if (element.id === id) {
        element.checked = !element.checked;
        this.currentSelectedCheckbox.emit(element);
        this.emitSelectedCheckbox(element);
        return;
      }
    });
  }

  emitSelectedCheckbox(selectedCheckbox) {
    if (this.outputArr && this.outputArr.length > 0) {
      let index = 0;
      let emitted = false;
      // this.outputArr.forEach((eachOutputElement) => {
      for (const eachOutputElement of this.outputArr) {
        if (eachOutputElement.id === selectedCheckbox.id) {
          const temp = this.outputArr.splice(index, 1);
          emitted = true;
          this.selectedCheckboxes.emit(this.outputArr);
          return;
        }
        if (index === this.outputArr.length - 1 && emitted === false) {
          this.outputArr.push(selectedCheckbox);
          this.selectedCheckboxes.emit(this.outputArr);
        }
        index++;
      }
    } else {
      this.outputArr.push(selectedCheckbox);
      this.selectedCheckboxes.emit(this.outputArr);
    }
  }

  isMaxCountReached() {
    // tslint:disable-next-line: max-line-length
    if (this.checkbox && this.checkbox.checkboxData && this.checkbox.checkboxData.length > 0 && this.maxSelectableCheckboxes && this.maxSelectableCheckboxes > 0) {
      let checkboxCount = 0;
      for (const eachCheckbox of this.checkbox.checkboxData) {
        if (eachCheckbox.checked) {
          checkboxCount++;
          // console.l
          if (checkboxCount >= this.maxSelectableCheckboxes) {
            return true;
          }
        }
      }
      return false;
    } else {
      // console.log(this.maxSelectableCheckboxes);
      return false;
    }
  }

}
