import {Component, OnInit, Input, SimpleChanges, OnChanges} from '@angular/core';
import { BadgeModel } from '@gravityilabs/sdz-models';

@Component({
  selector: 'sdzl-widget-user-badge-group',
  templateUrl: './user-badge-group.component.html',
  styleUrls: ['./user-badge-group.component.scss']
})
export class UserBadgeGroupComponent implements OnInit, OnChanges {

  @Input() batchContentArray: Array<BadgeModel> = new Array<BadgeModel>();
  @Input() showName: boolean;
  @Input() showNameTextColor: string;
  @Input() showNameTextSize: string;

  constructor() { }

  availableWidth = 0;
  // currentWidth = 0;
  regularBatch: Array<BadgeModel> = new Array<BadgeModel>();
  extendedBatch: Array<BadgeModel> = new Array<BadgeModel>();
  eachBadgeWidth = 30; // Width of Badge card styled statically.
  showExtendedBadgeDropdown: boolean;

  ngOnInit() {
    this.availableWidth = document.getElementById('badge-group').offsetWidth;
    this.splitBadge();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.batchContentArray) {
      // console.log('data received', this.batchContentArray);
      this.availableWidth = document.getElementById('badge-group').offsetWidth;
      this.splitBadge();
    }
  }

  splitBadge() {
    if ((this.eachBadgeWidth * this.batchContentArray.length) + (this.batchContentArray.length * 20) > this.availableWidth) {
      this.regularBatch = JSON.parse(JSON.stringify(this.batchContentArray));
      for (let i = 1; i <= this.batchContentArray.length; i++) {
        if (((i * this.eachBadgeWidth) + 20) > this.availableWidth) {
          this.extendedBatch = this.regularBatch.splice(i - 1, (this.regularBatch.length - (i - 1)));
          break;
        }
      }
    } else {
      this.regularBatch = JSON.parse(JSON.stringify(this.batchContentArray));
    }
    // console.log('regular batch')
  }

  enableDropDown() {
    this.showExtendedBadgeDropdown = !this.showExtendedBadgeDropdown;
    // this.left = event.x - 225
    // this.top = event.y + 5
  }

}
