import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CheckBoxModel } from '@gravityilabs/sdz-models';

@Component({
  selector: 'sdzl-widget-extended-tags-dropdown',
  templateUrl: './extended-tags-dropdown.component.html',
  styleUrls: ['./extended-tags-dropdown.component.scss']
})
export class ExtendedTagsDropdownComponent implements OnInit {

  @Input() extendedData: Array<CheckBoxModel> = new Array<CheckBoxModel>();
  @Output() removedTag = new EventEmitter<CheckBoxModel>();
  constructor() { }

  ngOnInit() {
  }

  removeTag(tag: CheckBoxModel) {
    this.removedTag.emit(tag);
  }

}
