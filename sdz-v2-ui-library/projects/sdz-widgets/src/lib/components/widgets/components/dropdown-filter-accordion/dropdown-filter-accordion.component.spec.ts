import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DropdownFilterAccordionComponent } from './dropdown-filter-accordion.component';

describe('DropdownFilterAccordionComponent', () => {
  let component: DropdownFilterAccordionComponent;
  let fixture: ComponentFixture<DropdownFilterAccordionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DropdownFilterAccordionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DropdownFilterAccordionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
