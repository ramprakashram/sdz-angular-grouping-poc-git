import {Component, Input, SimpleChanges, Output, EventEmitter, OnChanges} from '@angular/core';
import { HorizontalTabModel } from '@gravityilabs/sdz-models';

@Component({
  selector: 'sdzl-widget-horizontal-tabs',
  templateUrl: './horizontal-tabs.component.html',
  styleUrls: ['./horizontal-tabs.component.scss']
})
export class HorizontalTabsComponent implements OnChanges {

  @Input() tabData: Array<HorizontalTabModel> = new Array<HorizontalTabModel>();
  @Input() showCount: boolean;
  @Input() isBorderBottomVisible: boolean;
  @Output() selectedTab = new EventEmitter();

  constructor() { }

  activeTab: string;

  // ngOnInit() {
  // }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.tabData) {
      setTimeout(() => {
        if (this.tabData && this.tabData.length > 0 ) {
          const firstElementAsActiveTab = document.getElementById(this.tabData[0].id);
          this.activeTab = this.tabData[0].id;
          firstElementAsActiveTab.classList.add('active');
        }
      }, 100);
    }
  }

  removeAndAddActiveClass(tab: string) {
    if (tab !== this.activeTab) {
      const currentActiveElement = document.getElementById(this.activeTab);
      currentActiveElement.classList.remove('active');
      const selectedElement = document.getElementById(tab);
      selectedElement.classList.add('active');
      this.activeTab = tab;
      this.selectedTab.emit(this.activeTab);
    }
  }

}
