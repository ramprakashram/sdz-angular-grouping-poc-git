import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccordionWithCheckboxComponent } from './accordion-with-checkbox.component';

describe('AccordionWithCheckboxComponent', () => {
  let component: AccordionWithCheckboxComponent;
  let fixture: ComponentFixture<AccordionWithCheckboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccordionWithCheckboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccordionWithCheckboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
