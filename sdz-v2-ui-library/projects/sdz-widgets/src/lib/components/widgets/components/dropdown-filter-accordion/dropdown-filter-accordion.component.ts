import {Component, OnInit, Input, Output, EventEmitter, ElementRef, HostListener} from '@angular/core';
import { CheckBoxModel, AccordionWithCheckboxModel } from '@gravityilabs/sdz-models';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'sdzl-widget-dropdown-filter-accordion',
  templateUrl: './dropdown-filter-accordion.component.html',
  styleUrls: ['./dropdown-filter-accordion.component.scss'],
  animations: [
    trigger('arrowTrigger', [
      state('show', style({
        // top : '-8px',
        // right: '8px',
        transform: 'rotate(0deg)'
      })),
      state('hide', style({
        // top : '-12px',
        // right: '15px',
        transform: 'rotate(180deg)'
      })),
      transition('show => hide,hide => show', [
        animate('0.2s')
      ])
    ])
  ]
})
export class DropdownFilterAccordionComponent implements OnInit {

  constructor(private eRef: ElementRef) { }

  @HostListener('document:click', ['$event'])
  clickout(event) {
    if (this.eRef.nativeElement.contains(event.target)) {
      // this.text = "clicked inside";
    } else {
      this.hideData();
    }
  }

  @Input() accordionCheckboxesArray: Array<AccordionWithCheckboxModel> = new Array<AccordionWithCheckboxModel>();
  @Input() placeholder: string;
  @Output() dropdownFilters = new EventEmitter<Array<CheckBoxModel>>();
  @Input() filterTags: Array<CheckBoxModel> = new Array<CheckBoxModel>();
  id: string;
  toggleData: boolean;

  ngOnInit() {
    this.id = Math.random().toString();
  }

  showData() {
    this.toggleData = !this.toggleData;
    const content = document.getElementById(this.id);
    if (content.style.maxHeight) {
      content.style.maxHeight = null;
      content.style.border = null;
    } else {
      // content.style.maxHeight = content.scrollHeight + "px"
      content.style.maxHeight = 'inherit';
      content.style.border = '1px solid #E6E6E6';
      // content.style.transition = '0.2s ease-out;'
    }
    if (this.accordionCheckboxesArray && this.accordionCheckboxesArray.length == 0) {
      if (content.style.padding) {
        content.style.padding = null;
      } else {
        content.style.padding = '16px 0px';
      }
    }
  }

  hideData() {
    this.toggleData = false;
    const content = document.getElementById(this.id);
    if (content.style.maxHeight) {
      content.style.maxHeight = null;
      content.style.border = null;
    }
  }

  selectedAccordionCheckboxes(event) {
    if (event.checked) {
      this.filterTags.push(event);
      this.filterTags = this.filterTags.slice();
    } else {
      this.deletedTag(event);
    }
    this.dropdownFilters.emit(this.filterTags);
    // this.checkForFilterData();
  }

  deletedTag(event) {
    let index = 0;
    for (const eachTag of this.filterTags) {
      if (eachTag.id === event.id) {
        this.filterTags.splice(index, 1);
      }
      index++;
    }
    this.filterTags = this.filterTags.slice();
    this.dropdownFilters.emit(this.filterTags);
    // this.checkForFilterData();
  }

  // checkForFilterData() {
  //   if (this.filterTags && this.filterTags.length === 0) {
  //     this.isDataAvailable = false;
  //   } else {
  //     this.isDataAvailable = true;
  //   }
  // }



}
