import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExtendedTagsDropdownComponent } from './extended-tags-dropdown.component';

describe('ExtendedTagsDropdownComponent', () => {
  let component: ExtendedTagsDropdownComponent;
  let fixture: ComponentFixture<ExtendedTagsDropdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExtendedTagsDropdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtendedTagsDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
