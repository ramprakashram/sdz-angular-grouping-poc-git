import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserBadgeGroupComponent } from './user-badge-group.component';

describe('UserBadgeGroupComponent', () => {
  let component: UserBadgeGroupComponent;
  let fixture: ComponentFixture<UserBadgeGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserBadgeGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserBadgeGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
