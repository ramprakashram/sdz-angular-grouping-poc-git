import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {StepSliderComponent} from './components/step-slider/step-slider.component';
import {DualStepSliderComponent} from './components/dual-step-slider/dual-step-slider.component';
import {ConfirmationBoxComponent} from './components/confirmation-box/confirmation-box.component';
import {SafehtmlPipe} from '../../pipes/safe-html/safe-html.pipe';

@NgModule({
  declarations: [
    StepSliderComponent,
    DualStepSliderComponent,
    ConfirmationBoxComponent,
    SafehtmlPipe,
  ],
  imports: [
    CommonModule
  ],
  exports: [
    StepSliderComponent,
    DualStepSliderComponent,
    ConfirmationBoxComponent
  ]
})
export class InteractiveWidgetsModule {
}
