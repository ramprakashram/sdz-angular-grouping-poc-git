import {Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'sdzl-widget-confirmation-box',
  templateUrl: './confirmation-box.component.html',
  styleUrls: ['./confirmation-box.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ConfirmationBoxComponent implements OnInit {

  constructor() {
  }

  @Input() title: string;
  @Input() subtitle: string;
  @Input() warningImage: string;
  @Input() successButtonText: string;
  @Input() successButtonTextColor: string;
  @Input() successButtonBgColor: string;
  @Input() successButtonBorder: string;
  @Input() cancelButtonText: string;
  @Input() cancelButtonTextColor: string;
  @Input() cancelButtonBgColor: string;
  @Input() cancelButtonBorder: string;
  @Input() blurId: string;
  @Output() successClicked = new EventEmitter();
  @Output() cancelClicked = new EventEmitter();

  successBorder: string;
  cancelBorder: string;

  ngOnInit() {
    if (this.blurId) {
      const t = document.getElementById(this.blurId);
      // t.style.filter = 'blur(4px)';
      t.style.opacity = '0.1';
      // t.style.-ms-filter
      // t.style.filter = ':progid:DXImageTransform.Microsoft.Blur(pixelradius=\'2\', shadowopacity=\'0.0\')';
      t.style.pointerEvents = 'none';
      t.style.transition = 'opacity 0.4s';
      // t.style.overflow = 'hidden';
    }
    this.setSuccessBorder();
    this.setCancelBorder();
  }

  setSuccessBorder() {
    if (!this.successButtonBorder) {
      if (this.successButtonBgColor) {
        this.successBorder = '1px solid ' + this.successButtonBgColor;
      } else {
        this.successBorder = '1px solid black';
      }
    } else {
      this.successBorder = this.successButtonBorder;
    }
  }

  setCancelBorder() {
    if (!this.cancelButtonBorder) {
      if (this.cancelButtonBgColor) {
        this.cancelBorder = '1px solid ' + this.cancelButtonBgColor;
      } else {
        this.cancelBorder = '1px solid black';
      }
    } else {
      this.cancelBorder = this.cancelButtonBorder;
    }
  }

  success() {
    this.clearBlur();
    this.successClicked.emit(true);
  }

  cancel() {
    this.clearBlur();
    this.cancelClicked.emit(true);
  }

  clearBlur() {
    const x = document.getElementById('test1');
    x.classList.add('popOut');
    if (this.blurId) {
      const t = document.getElementById(this.blurId);
      // t.style.filter = 'blur(0px)';
      t.style.opacity = '1';
      t.style.pointerEvents = 'all';
      t.style.transition = 'opacity 0.4s';
      // t.style.overflow = 'auto';
    }
  }
}
