import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DualStepSliderComponent } from './dual-step-slider.component';

describe('DualStepSliderComponent', () => {
  let component: DualStepSliderComponent;
  let fixture: ComponentFixture<DualStepSliderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DualStepSliderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DualStepSliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
