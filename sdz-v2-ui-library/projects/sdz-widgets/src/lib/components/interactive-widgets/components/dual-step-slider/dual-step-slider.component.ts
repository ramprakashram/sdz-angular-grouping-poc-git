import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'sdzl-widget-dual-step-slider',
  templateUrl: './dual-step-slider.component.html',
  styleUrls: ['./dual-step-slider.component.scss']
})
export class DualStepSliderComponent implements OnInit {

  constructor() {
  }

  @Input() minValue: number;
  @Input() minValueText: string;
  @Input() maxValue: number;
  @Input() maxValueText: string;
  @Input() sliderOneValue: number;
  @Input() sliderTwoValue: number;
  @Input() disabled: boolean;
  @Output() selectedSliderOneValue = new EventEmitter<number>();
  @Output() selectedSliderTwoValue = new EventEmitter<number>();

  ngOnInit() {
  }

  getSliderOneValue(event) {
    console.log('slider one clicked');
    this.sliderOneValue = parseInt(event.target.value);
    if (this.sliderOneValue === this.sliderTwoValue) {
      if (this.sliderTwoValue !== 0) {
        this.sliderOneValue = this.sliderTwoValue - 1;
        const val1 = document.getElementById('lower');
        val1['value'] = this.sliderOneValue.toString();
      }
    } else if (this.sliderOneValue > this.sliderTwoValue) {
      this.sliderOneValue = this.sliderOneValue - ((this.sliderOneValue - this.sliderTwoValue) + 1);
      const val1 = document.getElementById('lower');
      val1['value'] = this.sliderOneValue.toString();
    }
    this.selectedSliderOneValue.emit(this.sliderOneValue);
  }

  getSliderTwoValue(event) {
    console.log('slider two clicked');
    this.sliderTwoValue = parseInt(event.target.value);
    if (this.sliderTwoValue === this.sliderOneValue) {
      if (this.sliderOneValue !== 50) {
        this.sliderTwoValue = this.sliderOneValue + 1;
        const val2 = document.getElementById('higher');
        val2['value'] = this.sliderTwoValue.toString();
      }
    } else if (this.sliderTwoValue < this.sliderOneValue) {
      this.sliderTwoValue = this.sliderTwoValue + ((this.sliderOneValue - this.sliderTwoValue) + 1);
      const val2 = document.getElementById('higher');
      val2['value'] = this.sliderTwoValue.toString();
    }
    this.selectedSliderTwoValue.emit(this.sliderTwoValue);
  }
}
