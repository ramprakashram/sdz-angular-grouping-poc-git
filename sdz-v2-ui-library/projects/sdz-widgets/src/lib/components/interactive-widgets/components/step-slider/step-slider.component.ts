import {Component, OnInit, Input, Output, EventEmitter, SimpleChanges, OnChanges} from '@angular/core';

@Component({
  selector: 'sdzl-widget-step-slider',
  templateUrl: './step-slider.component.html',
  styleUrls: ['./step-slider.component.scss']
})
export class StepSliderComponent implements OnInit, OnChanges {

  @Input() minValue: number;
  @Input() maxValue: number;
  @Input() currentValue: number;
  @Output() selectedValue = new EventEmitter();

  constructor() { }

  ngOnInit() {
    if (this.minValue && this.minValue === 0) {
      this.minValue = 1;
      // console.log(this.minValue);
    }
    if (!this.currentValue) {
      this.currentValue = this.minValue;
    }
    // this.minValue = 1
    // this.maxValue = 10
    // this.currentValue = 5
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.currentValue || changes.minValue || changes.maxValue) {
      if (this.currentValue < this.minValue) {
        this.currentValue = this.minValue;
      } else if (this.currentValue > this.maxValue) {
        this.currentValue = this.maxValue;
      }
    }
  }

  getCurrentValue(event) {
    // console.log("current value",event.target.value)
    this.currentValue = event.target.value;
    console.log('cv', this.currentValue);
    this.selectedValue.emit(this.currentValue);
  }

}
