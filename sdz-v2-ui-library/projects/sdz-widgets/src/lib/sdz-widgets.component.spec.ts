import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SdzWidgetsComponent } from './sdz-widgets.component';

describe('SdzWidgetsComponent', () => {
  let component: SdzWidgetsComponent;
  let fixture: ComponentFixture<SdzWidgetsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SdzWidgetsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SdzWidgetsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
