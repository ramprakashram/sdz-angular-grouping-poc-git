import { NgModule } from '@angular/core';
import { SdzWidgetsComponent } from './sdz-widgets.component';

@NgModule({
  declarations: [SdzWidgetsComponent],
  imports: [
  ],
  exports: [SdzWidgetsComponent]
})
export class SdzWidgetsModule { }
