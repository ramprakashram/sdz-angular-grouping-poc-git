/*
 * Public API Surface of sdz-widgets
 */

export * from './lib/sdz-widgets.service';
export * from './lib/sdz-widgets.component';
export * from './lib/sdz-widgets.module';

// Exporting Widgets module
export * from './lib/components/widgets/widgets.module';

// Exporting Interactive Widgets module
export * from './lib/components/interactive-widgets/interactive-widgets.module';

