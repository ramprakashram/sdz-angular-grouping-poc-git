import { NgModule } from '@angular/core';
import { SdzLayoutsComponent } from './sdz-layouts.component';

@NgModule({
  declarations: [
    SdzLayoutsComponent,
  ],
  imports: [
  ],
  exports: [SdzLayoutsComponent]
})
export class SdzLayoutsModule { }
