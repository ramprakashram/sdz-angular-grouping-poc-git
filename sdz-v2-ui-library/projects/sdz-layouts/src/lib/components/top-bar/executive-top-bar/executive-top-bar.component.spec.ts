import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExecutiveTopBarComponent } from './executive-top-bar.component';

describe('ExecutiveTopBarComponent', () => {
  let component: ExecutiveTopBarComponent;
  let fixture: ComponentFixture<ExecutiveTopBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExecutiveTopBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExecutiveTopBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
