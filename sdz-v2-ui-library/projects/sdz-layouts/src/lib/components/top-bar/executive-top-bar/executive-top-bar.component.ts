import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'sdzl-layout-executive-top-bar',
  templateUrl: './executive-top-bar.component.html',
  styleUrls: ['./executive-top-bar.component.scss']
})
export class ExecutiveTopBarComponent implements OnInit {

  @Input() sdzLogoUrl: string;
  @Input() clientLogoUrl: string;
  @Input() hamburgerIconUrl: string;
  @Output() hamburgerTriggered = new EventEmitter<boolean>();

  constructor() {
  }

  ngOnInit() {
  }

  toggleTriggered() {
    this.hamburgerTriggered.emit(true);
  }
}
