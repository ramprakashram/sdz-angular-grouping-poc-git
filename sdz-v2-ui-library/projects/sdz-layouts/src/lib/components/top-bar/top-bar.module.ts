import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ExecutiveTopBarComponent} from './executive-top-bar/executive-top-bar.component';

@NgModule({
  declarations: [
    ExecutiveTopBarComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    ExecutiveTopBarComponent
  ]
})
export class TopBarModule {
}
