import { Component, OnInit, Input } from '@angular/core';
import { SideBarModel } from '@gravityilabs/sdz-models';
import { Router } from '@angular/router';

@Component({
  selector: 'sdzl-layout-exec-dashboard-sidebar',
  templateUrl: './exec-dashboard-sidebar.component.html',
  styleUrls: ['./exec-dashboard-sidebar.component.scss']
})
export class ExecDashboardSidebarComponent implements OnInit {

  constructor(private router: Router) {
  }

  @Input() sideBarData: Array<SideBarModel> = new Array<SideBarModel>();
  @Input() hideSideBar: boolean;
  @Input() customHeight: string;

  selectedParentIndex: number;
  selectedSubMenuIndex: number;

  ngOnInit() {
  }

  rotateArrow(id: any) {
    const t = document.getElementById(id);
    if (t && t.style) {
      if (!t.style.transform) {
        this.showDropdown(id);
      } else if (t.style.transform === 'rotate(90deg)') {
        this.hideDropdown(id);
      } else {
        this.showDropdown(id);
      }
    }
  }

  showDropdown(id: number) {
    const el = document.getElementById(id.toString());
    if (el) {
      el.style.transform = 'rotate(90deg)';
      el.style.right = '20px';
      this.removeActiveClassFromParent();
      if (this.sideBarData && this.sideBarData.length > 0 && this.sideBarData[id]) {
        this.sideBarData[id].active = true;
      }
      const subMenuElement = document.getElementById('submenu' + id);
      subMenuElement.style.display = 'block';
      // subMenuElement.style.transition = 'display 2s ease-out';
    }
  }

  hideDropdown(id: number) {
    const el = document.getElementById(id.toString());
    if (el) {
      el.style.transform = 'rotate(270deg)';
      el.style.right = '26px';
      if (this.sideBarData && this.sideBarData.length > 0 && this.sideBarData[id]) {
        this.sideBarData[id].active = false;
      }
      const subMenuElement = document.getElementById('submenu' + id);
      subMenuElement.style.display = 'none';
      // subMenuElement.style.transition = 'display 2s ease-out';
    }
  }

  removeActiveClassFromParent() {
    if (this.sideBarData && this.sideBarData.length > 0) {
      let index = 0;
      for (const eachSideBarData of this.sideBarData) {
        if (eachSideBarData.active) {
          eachSideBarData.active = false;
          // this.removeActiveClassFromChildren(index);
          const subMenuElement = document.getElementById('submenu' + index);
          subMenuElement.style.display = 'none';
          const arrowElement = document.getElementById(index.toString());
          arrowElement.style.transform = 'rotate(270deg)';
          arrowElement.style.right = '26px';
          return;
        }
        index++;
      }
    }
  }

  // removeActiveClassFromChildren(parentId: number) {
  //   if (this.sideBarData && this.sideBarData.length > 0 && this.sideBarData[parentId] && this.sideBarData[parentId].children && this.sideBarData[parentId].children.length > 0) {
  //     for (const eachChild of this.sideBarData[parentId].subMenu) {
  //       if (eachChild.active) {
  //         eachChild.active = false;
  //         return;
  //       }
  //     }
  //   }
  // }

  redirectToParentTarget(index: number) {
    this.selectedParentIndex = index;
    this.selectedSubMenuIndex = null;
    if (this.sideBarData[index] && this.sideBarData[index].subMenu && this.sideBarData[index].subMenu.length > 0) {
      return;
    } else {
      const sourceUrl = window.location.origin;
      if (this.sideBarData[index].url.includes('#')) {
        window.open(`${sourceUrl}/execdashboard-v2/${this.sideBarData[index].url}`, '_self');
      } else {
        window.open(`${sourceUrl}/${this.sideBarData[index].url}`, '_self');
      }
      // this.router.navigate([this.sideBarData[index].url]);
    }
  }

  redirectToChildrenTarget(parentIndex: number, childrenIndex: number) {
    if (this.sideBarData[parentIndex] && this.sideBarData[parentIndex].subMenu && this.sideBarData[parentIndex].subMenu[childrenIndex]) {
      this.showDropdown(parentIndex);
      // this.removeActiveClassFromChildren(parentIndex);
      // this.sideBarData[parentIndex].children[childrenIndex].active = true;
      const sourceUrl = window.location.origin;
      this.selectedSubMenuIndex = childrenIndex;
      if (this.sideBarData[parentIndex].subMenu[childrenIndex].url.includes('#')) {
        window.open(`${sourceUrl}/execdashboard-v2/${this.sideBarData[parentIndex].subMenu[childrenIndex].url}`, '_self');
      } else {
        window.open(`${sourceUrl}/${this.sideBarData[parentIndex].subMenu[childrenIndex].url}`, '_self');
      }
      // this.router.navigate([this.sideBarData[parentIndex].subMenu[childrenIndex].url]);
    }
  }
}
