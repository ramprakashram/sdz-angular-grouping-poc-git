import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExecDashboardSidebarComponent } from './exec-dashboard-sidebar.component';

describe('ExecDashboardSidebarComponent', () => {
  let component: ExecDashboardSidebarComponent;
  let fixture: ComponentFixture<ExecDashboardSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExecDashboardSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExecDashboardSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
