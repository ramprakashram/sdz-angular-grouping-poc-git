import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ExecDashboardSidebarComponent} from './exec-dashboard-sidebar/exec-dashboard-sidebar.component';
import {RouterModule} from "@angular/router";

@NgModule({
  declarations: [
    ExecDashboardSidebarComponent
  ],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [
    ExecDashboardSidebarComponent
  ]
})
export class SidebarModule {
}
