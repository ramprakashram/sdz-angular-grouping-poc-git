import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableWithSortAndSearchComponent } from './components/table-with-sort-and-search/table-with-sort-and-search.component';
import { SafehtmlPipe } from '../../pipes/safe-html/safehtml.pipe';
import { SearchPipe } from '../../pipes/search/search.pipe';
import { TableWithLazyLoadingComponent } from './components/table-with-lazy-loading/table-with-lazy-loading.component'

@NgModule({
  declarations: [
    TableWithSortAndSearchComponent,
    SafehtmlPipe,
    SearchPipe,
    TableWithLazyLoadingComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    TableWithSortAndSearchComponent,
    SafehtmlPipe,
    TableWithLazyLoadingComponent
  ]
})
export class TableModule { }
