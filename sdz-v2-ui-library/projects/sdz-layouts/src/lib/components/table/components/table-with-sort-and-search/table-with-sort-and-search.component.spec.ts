import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableWithSortAndSearchComponent } from './table-with-sort-and-search.component';

describe('TableWithSortAndSearchComponent', () => {
  let component: TableWithSortAndSearchComponent;
  let fixture: ComponentFixture<TableWithSortAndSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableWithSortAndSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableWithSortAndSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
