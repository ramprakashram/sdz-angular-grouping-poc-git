import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableWithLazyLoadingComponent } from './table-with-lazy-loading.component';

describe('TableWithLazyLoadingComponent', () => {
  let component: TableWithLazyLoadingComponent;
  let fixture: ComponentFixture<TableWithLazyLoadingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableWithLazyLoadingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableWithLazyLoadingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
