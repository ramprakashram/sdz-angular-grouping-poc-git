import {Component, Input, OnChanges, SimpleChanges, AfterViewInit, Output, EventEmitter, OnDestroy, OnInit} from '@angular/core';
import {HorizontalScrollModel} from '@gravityilabs/sdz-models';

// import IntersectionObserver from 'intersection-observer-polyfill';

@Component({
  selector: 'sdzl-layout-table-with-lazy-loading',
  templateUrl: './table-with-lazy-loading.component.html',
  styleUrls: ['./table-with-lazy-loading.component.scss']
})
export class TableWithLazyLoadingComponent implements OnChanges, OnInit, OnDestroy {


  // In order to implement lazy loading in IE, do
  // npm install intersection-observer
  // And add import 'intersection-observer' in polyfills.ts in respective projects.
  @Input() tableContent: Array<any> = new Array<any>();
  @Input() increaseCount: number;
  @Input() isRowSelectable: boolean;
  @Input() selectableColumnNumber: number;
  @Input() hasNextPage: boolean;
  @Input() clearTable: boolean;
  @Input() scrollData: HorizontalScrollModel;
  // If the window is not scrollable, give parentScrollId and it will listen for scroll in that id, if no id is provided, it listens for Window scroll event
  @Input() parentScrollId: string;
  @Output() loadNextSetOfData = new EventEmitter<number>();
  @Output() rowClicked = new EventEmitter<boolean>();
  @Output() columnClicked = new EventEmitter<boolean>();
  @Output() scrollLeftDistance = new EventEmitter<HorizontalScrollModel>();

  id: string;
  currentTableContent: Array<any> = new Array<any>();
  pageNumber = 1;
  dummyRow = [];
  showShimmerRows = false;
  horizontalScrollElements: any;
  userTouchDetected: boolean;

  constructor() {
    this.id = Math.random().toString();
    // window.addEventListener('scroll', this.lazyLoadingForIE);
  }

  ngOnChanges(change: SimpleChanges) {
    // Logic for when table content is received
    if (change.tableContent) {
      // tslint:disable-next-line:max-line-length
      // This tempStartValue is used to save start value of the table row count before appending new content, which will be used to scroll the elements
      const tempStartValue = this.currentTableContent.length === 0 ? 0 : this.currentTableContent.length - 1;
      this.currentTableContent = this.currentTableContent.concat(this.tableContent);
      if (window.outerWidth <= 1199) {
        this.scrollProjects(tempStartValue, this.currentTableContent.length - 1, this.getScrollData(this.scrollData));
      }
      this.showShimmerRows = false;
      if (this.hasNextPage) {
        if (this.isIE()) {
          setTimeout(() => {
            this.lazyLoadingForIE();
          }, 10);
        } else {
          setTimeout(() => {
            this.intersectionObserverForTableRow();
          }, 10);
        }
      } else {
        if (this.parentScrollId) {
          const parentScrollElement = document.getElementById(this.parentScrollId);
          parentScrollElement.removeEventListener('scroll', this.lazyLoadingForIE);
        } else {
          window.removeEventListener('scroll', this.lazyLoadingForIE);
        }
      }
    }

    // Logic when clear table is triggered
    if (change.clearTable) {
      if (this.clearTable) {
        this.currentTableContent = new Array<any>();
        this.pageNumber = 1;
        this.dummyRow = [];
        this.showShimmerRows = false;
      }
    }

    // Logic For when scroll data is received
    if (change.scrollData) {
      if (window.outerWidth <= 1199) {
        this.scrollProjects(0, this.currentTableContent.length - 1, this.getScrollData(this.scrollData));
        this.userTouchDetected = this.scrollData && this.scrollData.userTouchDetected ? this.scrollData.userTouchDetected : false;
      }
    }
  }

  ngOnInit() {
    if (this.isIE()) {
      if (this.parentScrollId) {
        const parentScrollElement = document.getElementById(this.parentScrollId);
        parentScrollElement.addEventListener('scroll', this.lazyLoadingForIE);
      } else {
        window.addEventListener('scroll', this.lazyLoadingForIE);
      }
    }
  }

  ngOnDestroy() {
    if (this.isIE()) {
      if (this.parentScrollId) {
        const parentScrollElement = document.getElementById(this.parentScrollId);
        parentScrollElement.removeEventListener('scroll', this.lazyLoadingForIE);
      } else {
        window.removeEventListener('scroll', this.lazyLoadingForIE);
      }
    } else {

    }
  }

  isIE() {
    const ua = window.navigator.userAgent;
    const msie = ua.indexOf('MSIE '); // IE 10 or older
    const trident = ua.indexOf('Trident/'); // IE 11

    return (msie > 0 || trident > 0);
  }

  getScrollData(scroll: HorizontalScrollModel) {
    if (scroll && scroll.scrollLeftDistance) {
      return scroll.scrollLeftDistance;
    }
    return 0;
  }

  lazyLoadingForIE = () => {
    const selectedNodeLists = document.getElementsByClassName('tableDataRow');
    if (!this.hasNextPage) {
      this.showShimmerRows = false;
      if (this.parentScrollId) {
        const parentScrollElement = document.getElementById(this.parentScrollId);
        parentScrollElement.removeEventListener('scroll', this.lazyLoadingForIE);
      } else {
        window.removeEventListener('scroll', this.lazyLoadingForIE);
      }
    }

    for (let i = 0; i < selectedNodeLists.length; i++) {
      if (this.isInViewport(selectedNodeLists[i])) {
        if (selectedNodeLists[i].id === ('lazyLoadedObserver' + (this.currentTableContent.length - 1))) {
          this.setDummyDataForTableRowShimmer();
          selectedNodeLists[i].removeAttribute('id');
          if (!this.hasNextPage) {
            this.showShimmerRows = false;
            if (this.parentScrollId) {
              const parentScrollElement = document.getElementById(this.parentScrollId);
              parentScrollElement.removeEventListener('scroll', this.lazyLoadingForIE);
            } else {
              window.removeEventListener('scroll', this.lazyLoadingForIE);
            }
          } else {
            this.loadNextSetOfData.emit(this.pageNumber);
            this.pageNumber++;
          }
          return true;
        }
      }
    }
    return false;
  };

  isInViewport(el) {
    const bounding = el.getBoundingClientRect();
    return (
      bounding.top >= 0 &&
      bounding.left >= 0 &&
      bounding.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
      bounding.right <= (window.innerWidth || document.documentElement.clientWidth)
    );
  }

  intersectionObserverForTableRow() {
    const selectedNodeLists = document.getElementsByClassName('tableDataRow');

    const tableIntersectionObserver = new IntersectionObserver((entries, tableIntersectionObserver) => {
      entries.forEach((entry) => {
        if (!this.hasNextPage) {
          this.showShimmerRows = false;
          tableIntersectionObserver.disconnect();
        }
        if (entry.isIntersecting) {
          const el = entry.target;
          if (el.id === ('lazyLoadedObserver' + (this.currentTableContent.length - 1))) {
            // this.currentTableContent = this.currentTableContent.concat(this.setDummyDataForTableRowShimmer());
            this.setDummyDataForTableRowShimmer();
            this.pageNumber++;
            this.loadNextSetOfData.emit(this.pageNumber);
            // setTimeout(() => {
            //   this.triggerObserver(selectedNodeLists, tableIntersectionObserver)
            // }, 10);
            tableIntersectionObserver.unobserve(entry.target);
          }
        }
      });
    });

    // tableIntersectionObserver.USE_MUTATION_OBSERVER = false;
    this.triggerObserver(selectedNodeLists, tableIntersectionObserver);
  }

  triggerObserver(nodeLists: any, observer: any) {
    for (let i = 0; i < nodeLists.length; i++) {
      observer.observe(nodeLists[i]);
    }
  }

  setDummyDataForTableRowShimmer() {
    if (this.dummyRow && this.dummyRow.length === 0) {
      const dummyData = [];
      if (this.tableContent && this.tableContent.length > 0) {
        this.tableContent[0].forEach((eachData) => {
          dummyData.push({
            style: {
              width: eachData.largeDeviceWidth
            }
          });
        });
      }

      for (let i = 0; i < this.increaseCount; i++) {
        this.dummyRow.push(dummyData);
      }
    }

    this.showShimmerRows = true;
    // return this.dummyRow;
  }

  emitRow() {
    this.rowClicked.emit(true);
  }

  emitColumn(b: boolean, id) {
    if (b) {
      this.columnClicked.emit(id);
    }
  }

  triggerScroll(rowIndex: number) {
    const scrolledProject = document.getElementById(this.id + '-' + rowIndex);
    this.userTouchDetected = true;
    this.scrollLeftDistance.emit(new HorizontalScrollModel('project', scrolledProject.scrollLeft, this.userTouchDetected));
    this.scrollProjects(0, this.currentTableContent.length - 1, scrolledProject.scrollLeft, rowIndex);
  }

  scrollProjects(startIndex: number, endIndex: number, scrollDistance: number, skipIndex?: number) {
    if (startIndex < endIndex) {
      // If a skip Index is given, add scroll left value for all elements except for skipindex element.
      if (skipIndex && skipIndex >= 0) {
        for (let i = startIndex; i <= endIndex; i++) {
          if (i !== skipIndex) {
            document.getElementById(this.id + '-' + i).scrollLeft = scrollDistance;
          }
        }
      } else {
        // if no skip index is given, add scroll left value for all elements
        for (let i = startIndex; i <= endIndex; i++) {
          if (document.getElementById(this.id + '-' + i)) {
            document.getElementById(this.id + '-' + i).scrollLeft = scrollDistance;
          } else {
            setTimeout(() => {
              document.getElementById(this.id + '-' + i).scrollLeft = scrollDistance;
            }, 0.1);
          }
        }
      }
    }
  }

  getWidth(lgWidth: string) {
    if (window.outerWidth <= 1199) {
      return '';
    } else if (window.outerWidth >= 1200) {
      return lgWidth;
    }
  }

  getMinWidth(mdWidth: string) {
    if (window.outerWidth <= 1199) {
      return mdWidth;
    } else if (window.outerWidth >= 1200) {
      return '';
    }
  }

  getRemainingWidth(firstElementWidth: string) {
    return (100 - parseInt(firstElementWidth.split('%')[0])) + '%';
  }

  touchCancelled(rowIndex: number) {
    const scrolledProject = document.getElementById(this.id + '-' + rowIndex);
    this.userTouchDetected = false;
    this.scrollLeftDistance.emit(new HorizontalScrollModel('project', scrolledProject.scrollLeft, this.userTouchDetected));
  }
}
