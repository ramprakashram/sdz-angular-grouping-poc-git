import {Component, OnInit, Input, SimpleChanges, Output, EventEmitter, OnChanges} from '@angular/core';
import {TableHeaderModel, CategoryFilterModel} from '@gravityilabs/sdz-models';
import {trigger, state, style, transition, animate} from '@angular/animations';

// import { TableHeaderModel } from 'projects/sdz-models/src/public-api';

@Component({
  selector: 'sdzl-layout-table-with-sort-and-search',
  templateUrl: './table-with-sort-and-search.component.html',
  styleUrls: ['./table-with-sort-and-search.component.scss'],
  animations: [
    trigger('arrowTrigger', [
      state('show', style({
        'padding-left': '5px',
        transform: 'rotate(0deg)'
      })),
      state('hide', style({
        'padding-right': '5px',
        transform: 'rotate(180deg)'
      })),
      transition('show => hide,hide => show', [
        animate('0.2s')
      ])
    ]),
    trigger('sortArrow', [
      state('ascending', style({
        transform: 'rotate(0deg)'
      })),
      state('descending', style({
        transform: 'rotate(180deg)'
      })),
      transition('ascending => descending,descending => ascending', [
        animate('0.1s')
      ])
    ])
  ]
})
export class TableWithSortAndSearchComponent implements OnInit, OnChanges {

  constructor() {
  }

  @Input() tableHeader: Array<TableHeaderModel> = new Array<TableHeaderModel>();
  @Input() searchText: string;
  @Input() contentPerPage: number;
  @Input() tableContent: Array<any> = new Array<any>();
  @Input() categoryFilters: Array<CategoryFilterModel> = new Array<CategoryFilterModel>();
  @Input() isRowSelectable: boolean;

  @Output() tableTrigger = new EventEmitter<any>();

  startIndex: number;
  endIndex: number;
  totalPages: number;
  currentPage: number;
  contentPerPageArray = [10, 20, 50, 100];
  toggleData: boolean;
  tableData: Array<any> = new Array<any>();
  tableDataMasterCopy: Array<any> = new Array<any>();
  disableRowClick: boolean;

  // hoverStyle = {}

  ngOnInit() {
    if (!this.contentPerPage) {
      this.contentPerPage = 5;
    }
    this.mapDataToHeaders();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.tableHeader) {
      this.mapDataToHeaders();
    }
    if (changes.tableContent) {
      this.mapDataToHeaders();
    }
    if (changes.categoryFilters) {
      this.filterCategories();
    }
    if (changes.searchText) {
      this.searchTableData();
    }
  }

  searchTableData() {
    if (this.searchText === '') {
      this.mapDataToHeaders();
    } else {
      if (this.searchText.length > 2) {
        this.mapDataToHeaders();
        this.searchText = this.searchText.toLowerCase();
        const searchData = this.tableData;
        this.tableData = [];
        this.tableData = searchData.filter((eachData) => {
          for (const o of Object.keys(eachData)) {
            if (eachData[o].toLowerCase().includes(this.searchText)) {
              return eachData;
            }
          }
        });
        // this.initializeContentPerPage();
        this.initializePaginator();
      }
    }
  }

  mapDataToHeaders() {
    this.tableData = [];

    this.tableHeader.forEach((eachHead) => {
      if (!eachHead.key) {
        eachHead.key = Math.random().toString();
      }
    });

    if (this.tableContent && this.tableContent.length > 0) {
      this.tableContent.forEach((eachRowContent) => {
        const rowData: any = {};
        let index = 0;
        eachRowContent.forEach((eachData) => {
          if (eachData !== eachRowContent[eachRowContent.length - 1]) {
            rowData[this.tableHeader[index].key] = eachData;
          } else {
            rowData.rowKey = eachData;
          }
          index++;
        });
        this.tableData.push(rowData);
      });
      this.tableDataMasterCopy = this.tableData.slice();

      if (this.categoryFilters && this.categoryFilters.length > 0) {
        this.filterCategories();
      } else {
        // this.initializeContentPerPage();
        this.initializePaginator();
      }
    }
  }

  filterCategories() {
    // tslint:disable-next-line:max-line-length
    if (this.categoryFilters && this.categoryFilters.length > 0 && this.tableHeader && this.tableHeader.length > 0 && this.tableData && this.tableData.length > 0) {
      if (this.checkWhetherCategoryFilterHasFilters()) {
        const columnToBeFiltered = [];
        this.categoryFilters.forEach((eachCategory) => {
          columnToBeFiltered.push(this.tableHeader[eachCategory.columnNumber].key);
        });
        const tableDataCopy = JSON.parse(JSON.stringify(this.tableDataMasterCopy));
        this.tableData = [];
        tableDataCopy.forEach((eachTableDataCopy) => {
          columnToBeFiltered.forEach((eachColumn) => {
            if (eachTableDataCopy[eachColumn]) {
              this.categoryFilters.forEach((eachCategoryFilters) => {
                eachCategoryFilters.filter.forEach((eachFilter) => {
                  if (eachTableDataCopy[eachColumn] === eachFilter) {
                    if (!this.checkWhetherTheColumnIsAvailable(eachTableDataCopy)) {
                      this.tableData.push(eachTableDataCopy);
                      return;
                    } else {
                      return;
                    }
                  }
                });
              });
            }
          });
        });
        // this.initializeContentPerPage();
        this.initializePaginator();
      } else {
        this.tableData = this.tableDataMasterCopy;
        // this.initializeContentPerPage();
        this.initializePaginator();
      }
      // } else if (!this.categoryFilters || (this.categoryFilters && this.categoryFilters.length == 0)) {
    } else {
      // this.mapDataToHeaders();
      this.tableData = this.tableDataMasterCopy;
      // this.initializeContentPerPage();
      this.initializePaginator();
    }
  }

  checkWhetherCategoryFilterHasFilters() {
    for (const eachCategory of this.categoryFilters) {
      if (eachCategory.filter && eachCategory.filter.length > 0) {
        return true;
      }
    }
    return false;
  }

  checkWhetherTheColumnIsAvailable(rowData: any) {
    for (const eachRow of this.tableData) {
      if (eachRow.rowKey === rowData.rowKey) {
        return true;
      }
    }
    return false;
  }

  // initializeContentPerPage() {
  //   this.contentPerPageArray = [];
  //   if (this.tableData.length <= 50) {
  //     this.contentPerPageArray.push(5);
  //     this.contentPerPageArray.push(10);
  //   }
  //   if (this.tableData.length > 50 && this.tableData.length <= 100) {
  //     this.contentPerPageArray.push(10);
  //     this.contentPerPageArray.push(20);
  //     this.contentPerPageArray.push(50);
  //   }
  //   if (this.tableData.length > 100) {
  //     this.contentPerPageArray.push(10);
  //     this.contentPerPageArray.push(20);
  //     this.contentPerPageArray.push(50);
  //     this.contentPerPageArray.push(100);
  //   }
  // }

  initializePaginator() {
    if (this.tableData && this.tableData.length > 0) {
      if (this.tableData.length > this.contentPerPage) {
        this.totalPages = Math.ceil(this.tableData.length / this.contentPerPage);
        this.startIndex = 0;
        this.currentPage = 1;
        this.endIndex = this.currentPage * this.contentPerPage;
      } else {
        this.startIndex = 0;
        this.endIndex = this.tableData.length;
        this.totalPages = 0;
      }
    }
  }

  dropdownTrigger() {
    this.toggleData = !this.toggleData;
    // tslint:disable-next-line:prefer-const
    let content = document.getElementById('content-count-drop-down');
    // const test = content;

    if (content.style.maxHeight) {
      content.style.maxHeight = null;
    } else {
      content.style.maxHeight = content.scrollHeight + 'px';
    }
  }

  selectedContentData(contentData) {
    this.contentPerPage = contentData;
    // this.dropdownTrigger(true);
    this.initializePaginator();
  }

  nextPage() {
    if (this.currentPage !== this.totalPages) {
      this.currentPage += 1;
      this.startIndex += this.contentPerPage;
      if ((this.currentPage * this.contentPerPage) > this.tableData.length) {
        this.endIndex = this.tableData.length;
      } else {
        this.endIndex = this.currentPage * this.contentPerPage;
      }

    }
  }

  previousPage() {
    if (this.currentPage !== 1) {
      if (this.currentPage * this.contentPerPage === this.tableData.length) {
        this.currentPage -= 1;
        this.endIndex = this.currentPage * this.contentPerPage;
      } else {
        this.currentPage -= 1;
        this.endIndex = this.currentPage * this.contentPerPage;
      }
      this.startIndex -= this.contentPerPage;
    }
  }

  sort(key: string, mode: boolean, isDateHeader: boolean) {
    const data = this.tableData.slice();
    this.tableHeader.forEach((eachHead) => {
      if (eachHead.key === key) {
        eachHead.ascending = !eachHead.ascending;
        return;
      }
    });
    this.tableData = data.sort((a, b) => {
      switch (key) {
        case key:
          if (isDateHeader) {
            return this.compareDate(new Date(a[key]), new Date(b[key]), mode);
          } else {
            return this.compare(a[key], b[key], mode);
          }
          break;
        default:
          return 0;
      }
    });

    // To go back to first page
    this.initializePaginator();
  }

  compare(a: number | string, b: number | string, isAscending: boolean) {
    return (a < b ? -1 : 1) * (isAscending ? 1 : -1);
  }

  compareDate(a: Date, b: Date, isAscending: boolean) {
    return (a < b ? -1 : 1) * (isAscending ? 1 : -1);
  }

  // Use this method to trigger event emitter to calling component
  // For passing data, use the 'id' to send the clicked data
  emitResponseFromSDZTable(data: any, resp) {
    const target: any = event.target || event.srcElement || event.currentTarget;
    if (target) {
      const idAttr = target.attributes.id;
      if (idAttr) {
        const value = idAttr.nodeValue;
        if (value) {
          this.disableRowClick = true;
          // let emittedData = {
          //   'data': data,
          //   // 'mode': mode
          // }
          this.tableTrigger.emit(value);
        }
      }
    }
  }

  rowClicked(rowID) {
    if (this.isRowSelectable) {
      if (!this.disableRowClick) {
        const value = 'row-' + rowID;
        this.tableTrigger.emit(value);
      } else {
        this.disableRowClick = false;
      }
    }
  }

}
