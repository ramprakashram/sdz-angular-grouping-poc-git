import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'sdzl-layout-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {

  constructor() { }

  @Input() buttonText: string;
  @Input() bgColor: string;
  @Input() textColor: string;
  @Input() textSize: string;
  @Input() textWeight: string;
  @Input() width: string;
  @Input() borderStyle: string;
  @Input() disabled: boolean;

  ngOnInit() {
  }

}
