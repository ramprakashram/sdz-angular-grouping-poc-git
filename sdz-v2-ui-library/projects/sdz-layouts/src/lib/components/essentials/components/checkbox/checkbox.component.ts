import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'sdzl-layout-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss']
})
export class CheckboxComponent implements OnInit {

  constructor() { }

  @Input() checked: boolean;
  @Input() checkboxColor: string;
  @Input() disabled: boolean;
  @Output() selectedCheckbox = new EventEmitter<boolean>();

  ngOnInit() {
  }

  checkboxClicked() {
    this.checked = !this.checked;
    this.selectedCheckbox.emit(this.checked);
  }
}
