import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonComponent } from './components/button/button.component';
import { CheckboxComponent } from './components/checkbox/checkbox.component';

@NgModule({
  declarations: [
    ButtonComponent,
    CheckboxComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    ButtonComponent,
    CheckboxComponent
  ]
})
export class EssentialsModule { }
