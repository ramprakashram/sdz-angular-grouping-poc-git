import { TestBed } from '@angular/core/testing';

import { SdzLayoutsService } from './sdz-layouts.service';

describe('SdzLayoutsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SdzLayoutsService = TestBed.get(SdzLayoutsService);
    expect(service).toBeTruthy();
  });
});
