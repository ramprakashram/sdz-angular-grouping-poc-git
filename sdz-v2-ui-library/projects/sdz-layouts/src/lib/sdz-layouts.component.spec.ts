import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SdzLayoutsComponent } from './sdz-layouts.component';

describe('SdzLayoutsComponent', () => {
  let component: SdzLayoutsComponent;
  let fixture: ComponentFixture<SdzLayoutsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SdzLayoutsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SdzLayoutsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
