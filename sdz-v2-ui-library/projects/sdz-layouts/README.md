# SdzLayouts

This library was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.2.0.

## Build

Run `ng build sdz-layouts` to build the project.
After building, files will be present in the dist/sdz-layouts

## Publishing

After building the library.
Navigate to dist folder.
Login in to npm account, if you have not logged in before.

To login to npm account,
run `npm login` and enter your username,password and email.

Check in to Organisation Account,
run `npm init --scope=@gravityilabs`

Finally publish the current package,
run `npm publish`

## Library Dependency

Make sure you install the following SDZ Libraries, before installing sdz-layouts

1.sdz-models
To install latest sdz-models ==> run `npm i @gravityilabs/sdz-models`
To install specific version of sdz-models ==> run `npm i @gravityilabs/sdz-models@VERSION_NUMBER`

## CHANGES IN THIS BUILD

[0.0.1]     Removed unused imports and removed some external dependencies.

[0.0.2]     Added support for prod build of sdz-layouts library.

[0.0.3]     Added Searching and Sorting feature to sdz-table-with-sort-and-search along with dynamic key generator.

[0.0.4]     Fixed issue of table not re-rendering after input data is changed.

[0.0.5]     Added fixes for searching and changed global search from pipes to ts and added emitter for buttons.

[0.0.6]     Fixed Category filter bug and added support for going to first page when sorting.

[0.0.7]     Added support for individual dynamic styling for each column, fixed issue regarding table not reloading data when category filter is sent without any filters.

[0.0.8]     Fixed re-rendering of tables when category filter is changed.

[0.0.9]     Added cursor and table row hover features.

[0.0.10]    Added support for row hover select as optional feature and fixes issue when table data is not reloaded when empty filter result is cleared.

[0.0.11]    Added Essentials Module which now contains buttons component.

[0.0.12]    Added borderStyle to Buttons Component.

[0.0.13]    Added checkbox component to essentials module.

[0.0.14]    Added new feature - When isRowClick is disabled, table component will not emit object.

[0.0.15]    Added disable feature checkbox essential component.

[0.0.16]    Added support for date sorting in table using table header.

[0.0.17]    Fixed IE Issues in Table Component.

[0.0.18]    Fixed Issue in Pagination Dropdown displaying page content selector.

[0.0.19]    Updated Default Text weight to 600 and min width as 90px for Sdz button.

[0.0.20]    Added Disabled Mode and Hover functionality to Sdz Button.

[0.0.21]    Added SideBar Module and created Executive Dashboard Sidebar Component.

[0.0.22]    Added static content per page for table.

[0.0.23]    Added Table with Lazy Loading component without lazy loading functionality.

[0.0.24]    Added Table with Lazy Loading component with shimmer effect.

[0.0.25]    Lazy loading Table completed with IE support.

[0.0.26]    Added hasNextPage Condition Check on Lazy loading table.

[0.0.27]    Added Alternate Row Color in Lazy loading table.

[0.0.28]    Updated Alternate Row Color in Lazy loading table.

[0.0.29]    Added Clear Table and removed border color of table rows in Lazy loading table.

[0.0.30]    Added alternate row color as white table rows in Lazy loading table.

[0.0.31]    Added Row Click and Column Click in Lazy loading table.

[0.0.32]    Updated Row Click to emit id in Lazy loading table.

[0.0.33]    Added IE11 support for Lazy loading table.

[0.0.34]    Added Executive Top Bar Component.

[0.0.35]    Updated Executive Top Bar Component to have minimal width.

[0.0.36]    Added Hamburger Toggle Icon to Executive Top Bar.

[0.0.37]    Added z-index to Executive side bar and removed model dependencies from package.json.

[0.0.38]    Fixed Executive Side bar to show Submenu name in dropdown.

[0.0.39]    Updated styling for Executive side bar.

[0.0.40]    No update issue for Executive side bar.

[0.0.41]    Updated IE11 support for Executive side bar.

[0.0.42]    Made static height for IE11 for Executive side bar.

[0.0.43]    Added sliding sidebar support for Executive side bar.

[0.0.44]    Updated Lazy Loaded Table to have fail proof lazy loading and added performance update to remove event listener on getting all data or when ngOndestroy is called for IE alone.

[0.0.45]    Added min width and styling updates for sidebar.

[0.0.46]    Added support for redirection to other urls without # and arrow sign position change in Sidebar component.

[0.0.47]    Added iPad support for Executive Side bar component.

[0.0.48]    Added auto hiding feature for Executive Side bar component.

[0.0.49]    Updated Pagenumber in Lazy loading table.

[0.0.50]    Updated Lazy loaded table to support tab view and Changed from table to div styled code.

[0.0.51]    Added Overflow-y to row of lazyloading table, so that it does not become scrollable on resizing.

[0.0.52]    Added lines after first column in ipad view with trigger for enabling and disabling it.

[0.0.53]    Updated Lazy load table to handle scrollleft error on large screen devices.

[0.0.54]    Updated div to span element in Lazy loading element to prevent clicks for entire width.

[0.0.55]    Updated Sidebar to remove scrollbar, but retain scroll functionality.

[0.0.56]    Added client logo in the header.
