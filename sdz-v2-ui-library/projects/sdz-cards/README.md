# SdzCards

SDZ Cards Library holds widgets like components like : 

[1] Moment Log

This library was generated with Angular version 7.2.0.

## Build

Run `ng build --project=sdz-cards` to build the project.
After building, files will be present in the dist/sdz-cards


## Publishing

After building the library.

Navigate to dist folder.

Login in to npm account, if it you hadn't logged in before.

To login to npm account,
run `npm login` and enter your username,password and email.

Check in to Organisation Account,
run `npm init --scope=@gravityilabs`

Finally publish the current package,
run `npm publish`

## Running unit tests

Run `ng test sdz-cards` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Library Dependency

Make sure you install the following SDZ Libraries, before installing sdz-widgets

1.sdz-models

To install latest sdz-models ==> run `npm i @gravityilabs/sdz-models`

To install specific version of sdz-models ==> run `npm i @gravityilabs/sdz-models@VERSION_NUMBER`

## Version Details

[0.0.1]     Initial setup

[0.0.2]     Added Moment Log Card

[0.0.3]     Updated Style for Moment Log.

[0.0.4]     Added Progress Bar.

[0.0.5]     Updated Style in Progress Bar.

[0.0.6]     Updated Models in Progress Bar.

[0.0.7]     Updated Models for Progress Bar Number.

[0.0.8]     Update Colors for Progress Bar.

[0.0.9]     Update count for Progress Bar.

[0.0.10]    Update count for Progress Bar.

[0.0.11]    Update style for Progress Bar.

[0.0.12]    Updated left position to 24px for Progress Bar.