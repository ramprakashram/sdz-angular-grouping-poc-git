import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MomentLogComponent } from './components/moment-log/moment-log.component';
import { ProgressBarComponent } from './components/progress-bar/progress-bar.component';

@NgModule({
  declarations: [
    MomentLogComponent,
    ProgressBarComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    MomentLogComponent,
    ProgressBarComponent
  ]
})
export class CardsModule { }
