import { Component, OnInit, Input } from '@angular/core';
import { MomentLogModel } from '@gravityilabs/sdz-models';

@Component({
  selector: 'sdzl-card-moment-log',
  templateUrl: './moment-log.component.html',
  styleUrls: ['./moment-log.component.scss']
})
export class MomentLogComponent implements OnInit {

  @Input() momentData : MomentLogModel = new MomentLogModel()

  constructor() { }

  ngOnInit() {
    // console.log("moment data",this.momentData)
  }

}
