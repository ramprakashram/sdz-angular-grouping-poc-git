import { Component, OnInit, Input } from '@angular/core';
import { ProgressBarModel } from '@gravityilabs/sdz-models';

@Component({
  selector: 'sdzl-card-progress-bar',
  templateUrl: './progress-bar.component.html',
  styleUrls: ['./progress-bar.component.scss']
})
export class ProgressBarComponent implements OnInit {
  @Input() progressBar: ProgressBarModel = new ProgressBarModel();
  temporaryObject: any;
  constructor() { }

  ngOnInit() {
    this.temporaryObject = this.progressBar;
  }
  setMyStylesBackground() {
    let styles = {
      'background-color': this.temporaryObject.backgroundColor ? this.temporaryObject.backgroundColor : '#ffffff',
      'height': this.temporaryObject.height ? this.temporaryObject.height + "px" : "8px",
      'width': this.temporaryObject.progressBarWidth ? this.temporaryObject.progressBarWidth + "%" : "100px",
    };
    return styles;
  }

  setMyStylesprogressBar() {
    let styles = {
    //  'background-color': this.temporaryObject.color ? this.temporaryObject.color : '#FFAB00',
      'height': this.temporaryObject.height ? this.temporaryObject.height + "px" : "8px",
      'width': this.temporaryObject.width ? this.temporaryObject.width + "%" : "100%",
    };
    return styles;
  }
}
