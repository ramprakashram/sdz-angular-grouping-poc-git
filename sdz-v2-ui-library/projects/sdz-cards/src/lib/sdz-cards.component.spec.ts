import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SdzCardsComponent } from './sdz-cards.component';

describe('SdzCardsComponent', () => {
  let component: SdzCardsComponent;
  let fixture: ComponentFixture<SdzCardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SdzCardsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SdzCardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
