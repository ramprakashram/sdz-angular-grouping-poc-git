import { NgModule } from '@angular/core';
import { SdzCardsComponent } from './sdz-cards.component';

@NgModule({
  declarations: [SdzCardsComponent],
  imports: [
  ],
  exports: [SdzCardsComponent]
})
export class SdzCardsModule { }
