import { TestBed } from '@angular/core/testing';

import { SdzCardsService } from './sdz-cards.service';

describe('SdzCardsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SdzCardsService = TestBed.get(SdzCardsService);
    expect(service).toBeTruthy();
  });
});
