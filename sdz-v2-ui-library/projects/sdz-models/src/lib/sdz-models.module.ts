import { NgModule } from '@angular/core';
import { SdzModelsComponent } from './sdz-models.component';

@NgModule({
  declarations: [SdzModelsComponent],
  imports: [
  ],
  exports: [SdzModelsComponent]
})
export class SdzModelsModule { }
