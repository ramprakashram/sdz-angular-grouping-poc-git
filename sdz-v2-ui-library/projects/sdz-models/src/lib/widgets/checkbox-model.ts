export class CheckBoxModel {
  id: number;
  name: string;
  checked: boolean;
  companyId: number;
  type: string;
  parentId: number;
  parentCategoryId: number;
}

export class MultiSelectCheckBoxModel {
  id: number;
  title: string;
  checkboxData: Array<CheckBoxModel> = new Array<CheckBoxModel>();
}
