export class HorizontalTabModel {
    id: string;
    displayName: string;
    displayCount: number;

    constructor(id ?: string, displayName ?: string, displayCount ?: number) {
        this.id = id ? id : '';
        this.displayName = displayName ? displayName : '';
        this.displayCount = displayCount ? displayCount : 0;
    }
}
