// export class BadgeModel {
//     color : string
//     name : string
// }

interface BadgeModelInterface {
    name: string;
    bgColor: string;
    textColor: string;
}

export class BadgeModel implements BadgeModelInterface {
    name: string;
    bgColor: string;
    textColor: string;
    constructor(name ?: string, bgColor ?: string, textColor ?: string) {
        this.name = name ? name : '';
        this.bgColor = bgColor ? bgColor : 'red';
        this.textColor = textColor ? textColor : '#FFFFFF';
          // (new AAATextColor()).getAAAColor('#FFFFFF') : (new AAATextColor()).getAAAColor(textColor);
    }
}

class AAATextColor {
    /* From this W3C document: http://www.w3.org/TR/AERT#color-contrast */
    constructor(threshold = 130) {
        this.threshold = threshold;
    }

    protected threshold: number;
    protected hRed: number;
    protected hGreen: number;
    protected hBlue: number;
    protected cBrightness: number;

    private cutHex(h): string {
        return h.charAt(0) === '#' ? h.substring(1, 7) : h;
    }

    private hexToR(h): number {
        return parseInt(this.cutHex(h).substring(0, 2), 16);
    }

    private hexToG(h): number {
        return parseInt(this.cutHex(h).substring(2, 4), 16);
    }

    private hexToB(h): number {
        return parseInt(this.cutHex(h).substring(4, 6), 16);
    }

    public getAAAColor(hex: string): string {
        // Test Hex Code
        if (/^#[0-9a-f]{3}([0-9a-f]{3})?$/i.test(hex)) {
            this.hRed = this.hexToR(hex);
            this.hGreen = this.hexToG(hex);
            this.hBlue = this.hexToB(hex);

            this.cBrightness = (this.hRed * 299 + this.hGreen * 587 + this.hBlue * 114) / 1000;

            if (this.cBrightness > this.threshold) {
                return '#000000';
            } else {
                return '#ffffff';
            }
        } else {
            console.warn('Fallback Color used instead of ', hex);
            return '#ffffff';
        }
    }
}
