import { CheckBoxModel } from './checkbox-model';

export class AccordionWithCheckboxModel {
    id : number;
    name : string;
    companyId: number;
    status: boolean;
    checkboxData : Array<CheckBoxModel> = new Array<CheckBoxModel>()
}