export class MomentLogModel {
    id : number
    title : string
    subtitle : string
    buttonText : string
    momentText : string
    isRoundedBorder : boolean
    backgroundColor : string
}
