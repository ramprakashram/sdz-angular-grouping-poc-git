export class ProgressBarModel {
    id : number
    ProgressBarnumber : number
    barText : string
    isPercentage : boolean
    backgroundColor : string
    leftSideText: string
    leftSideNumber: number
    rightSideText: string
    rightSideNumber: number
    width: number
    height: number
    percentageNumber: number
    isHorizontal: boolean
    color: string
    progressBarWidth: number
}
