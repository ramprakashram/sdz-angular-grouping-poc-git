import { TestBed } from '@angular/core/testing';

import { SdzModelsService } from './sdz-models.service';

describe('SdzModelsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SdzModelsService = TestBed.get(SdzModelsService);
    expect(service).toBeTruthy();
  });
});
