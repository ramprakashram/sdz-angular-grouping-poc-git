// export interface SideBarInterface {
//   // name: string;
//   // base: string;
//   // target: string;
//   // expanded: boolean;
//   // active: boolean;
//   // angularRouter: boolean;
//   // children: Array<SideBarChildrenItemsModel>;
//   name: string;
//   // url: string;
//   subMenu: Array<SideBarChildrenItemsModel>;
// }

// export interface SideBarChildrenItemsInterface {
//   // name: string;
//   // target: string;
//   // active: boolean;
//   name: string;
//   url: string;
// }

export class SideBarChildrenItemsModel {
  // name: string;
  // target: string;
  // active: boolean;
  name: string;
  url: string;

  constructor(name?: string, url?: string) {
    this.name = name ? name : '';
    this.url = url ? url : '';
  }
}

export class SideBarModel {
  // name: string;
  // base: string;
  // target: string;
  // expanded: boolean;
  // active: boolean;
  // angularRouter: boolean;
  // children: Array<SideBarChildrenItemsModel>;
  name: string;
  url?: string;
  active?: boolean;
  subMenu?: Array<SideBarChildrenItemsModel>;

  constructor(name?: string, url?: string, active?: boolean, subMenu?: Array<SideBarChildrenItemsModel>) {
    this.name = name ? name : '';
    this.url = url ? url : '';
    this.active = active ? this.active = active : this.active = false;
    this.subMenu = subMenu ? subMenu : [];
  }
}
