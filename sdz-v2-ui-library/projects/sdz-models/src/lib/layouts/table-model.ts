export class TableHeaderModel {
  name: string;
  key: string;
  isSortingEnabled: boolean;
  ascending: boolean;
  style: any;
  isDateHeader: boolean;

  constructor(name ?: string, isSortingEnabled?: boolean, style?: any, ascending: boolean = false, isDateHeader: boolean = false) {
    this.name = name ? this.name = name : this.name = '';
    this.isSortingEnabled = isSortingEnabled ? this.isSortingEnabled = isSortingEnabled : this.isSortingEnabled = false;
    this.style = style ? style : '';
    this.key = Math.random().toString();
    this.ascending = ascending;
    this.isDateHeader = isDateHeader;
  }
}

export class CategoryFilterModel {
  columnNumber: number;
  filter: Array<string>;

  constructor(columnNumber ?: number, filter ?: Array<string>) {
    if (columnNumber) {
      this.columnNumber = columnNumber;
    }
    if (filter) {
      this.filter = filter;
    }
  }
}

export class HorizontalScrollModel {
  emitterName: string;
  scrollLeftDistance: number;
  userTouchDetected: boolean;

  constructor(emitterName?: string, scrollLeftDistance?: number, userTouchDetected?: boolean) {
    this.emitterName = emitterName ? emitterName : '';
    this.scrollLeftDistance = scrollLeftDistance ? scrollLeftDistance : 0;
    this.userTouchDetected = userTouchDetected ? userTouchDetected : false;
  }
}
