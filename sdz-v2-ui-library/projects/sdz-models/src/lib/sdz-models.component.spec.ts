import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SdzModelsComponent } from './sdz-models.component';

describe('SdzModelsComponent', () => {
  let component: SdzModelsComponent;
  let fixture: ComponentFixture<SdzModelsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SdzModelsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SdzModelsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
