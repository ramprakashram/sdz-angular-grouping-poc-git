# Sdz Models

SDZ Models Library project holds the data model for all other library projects.

It is mandatory and should be installed first.

This library was generated with Angular version 7.2.0.

## Code scaffolding

Run `ng generate component component-name --project sdz-models` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module --project sdz-models`.
> Note: Don't forget to add `--project sdz-models` or else it will be added to the default project in your `angular.json` file. 

## Build

Run `ng build sdz-models` to build the project. The build artifacts will be stored in the `dist/` directory.

## Publishing

After building your library with `ng build sdz-models`, go to the dist folder `cd dist/sdz-models` and run `npm publish`.

## Running unit tests

Run `ng test sdz-models` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Version Details

[0.0.1]       Added Checkbox Models and Accordion Models

[0.0.2]       Added TableHeader Model and User Badge Model

[0.0.3]       Miscellaneous changes and fixes

[0.0.4]       Added Moment Log model and added ascending key to table model.

[0.0.5]       Added constructor to table model and create random key on initialization.

[0.0.6]       Added CategoryFilterModel to support category filter.

[0.0.7]       Added constructor to CategoryFilterModel

[0.0.8]       Added styles to Table Header Model

[0.0.9]       Updated UserBadgeModel to get bgColor and textColor

[0.0.10]      Added Horizontal Tabs Model to sdz-models.

[0.0.11]      Emergency Bug Fix on Models.

[0.0.12]      Changed id from string to number on checkbox and accordion model.Added companyId to checkbox and accordion model.

[0.0.13]      Added parentCategoryId to Checkbox Model.

[0.0.14]      Added Display Count to Horizontal Tab Model.

[0.0.15]      Added isDateHeader to Tables Model to Support Date Sorting.

[0.0.16]      Added Sidebar Model

[0.0.17]      Mapped Interface with classes in Sidebar Model

[0.0.18]      Progress Bar Model Added.

[0.0.19]      Progress Bar Model Updated with percentage and progress Bar.

[0.0.20]      Progress Bar Model Updated with labels and configuration Model.

[0.0.21]      Sidebar Model Updated executive dashboard json format.

[0.0.22]      Added active key to Sidebar Model.

[0.0.23]      Fixed mandatory key to Sidebar Model.

[0.0.24]      Added HorizontalScrollModel to Table Model.

[0.0.25]      Updated Sidebar Model to have subMenu as optional key.
