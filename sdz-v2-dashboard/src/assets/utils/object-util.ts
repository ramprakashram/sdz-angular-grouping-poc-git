export class ObjectUtil {
    
    public static renderArray<T>(json: Array<any>, A?: any): Array<T> {
        let arrayValues = new Array<T>()
        json.forEach((val) => {
            if (A)
                val = ObjectUtil.createInstance<T>(val, A)
            arrayValues.push(val)
        })
        return arrayValues;
    }

    private static createInstance<A>(val: any, c: new (val: any) => A): A {
        return new c(val);
    }
}

