import * as d3 from 'd3';

export const alphabets: any[] = 'abcdefghijklmnopqrstuvwxyz'.split('');

export class D3Method {
  static getLinearScale(domain, range) {
    range = domain[1] === 0 || domain[0] === domain[1] ? [0.7, 0.7] : range;
    return d3.scaleLinear()
      .domain(domain)
      .range(range);
  }
}

export class UniqueID {
  static generateRandomString() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }

  static generateUniqueID() {
    return this.generateRandomString() + this.generateRandomString() +
      '-' + this.generateRandomString() + '-' + this.generateRandomString() +
      '-' + this.generateRandomString() + '-' + this.generateRandomString() +
      this.generateRandomString() + this.generateRandomString();
  }

  static generateUniqueNumber() {
    return Math.floor(new Date().valueOf() * Math.random());
  }

  static getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

}

export class ObjectOperation {
  static shallowCopy(obj: object) {
    return JSON.parse(JSON.stringify(obj));
  }

  static inArray(val, arr: any[]) {
    for (let i = 0; i < arr.length; i++) {
      if (val === arr[i]) {
        return i;
      }
    }
    return -1;
  }

  static getArrayElemByAttributeValue(key, value, arr) {
    for (let i = 0; i < arr.length; i++) {
      const arrElem = arr[i];
      if (arrElem[key] === value) {
        return arrElem;
      }
    }
  }

  static getMinMax(arr, key) {
    const values = arr.map(d => {
      return d[key];
    });

    const min = Math.min.apply(Math, values);
    const max = Math.max.apply(Math, values);
    return [min, max];
  }

}

export class NamingMethod {
  static alphabetBullets(i) {
    return alphabets[i];
  }
}

// Custom Prototype Methods

// String Split By Camel Case Method
String.prototype['splitByCamelCase'] = function () {
  return this.replace(/([a-z](?=[A-Z]))/g, '$1 ');
};
