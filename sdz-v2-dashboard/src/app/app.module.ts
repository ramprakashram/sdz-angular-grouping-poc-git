import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

// App Components
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {TopBarComponent} from './shared/top-bar/top-bar.component';
import {StartingComponentComponent} from './components/landingPage/starting-component/starting-component.component';
import {SearchComponent} from './shared/search/search.component';

// Interceptors
import {HttpAppInterceptor} from './services/https-service/http-app.interceptor';

// External plugins
import {NgxSpinnerModule} from 'ngx-spinner';

// App Services
import {LoaderService} from './services/loaders/loader.service';
import {LanguageStringService} from './services/languageString/language-string.service';
import {HttpServiceService} from './services/https-service/https.service';
import {FiltersService} from './services/globalVariableService/filters.service';
import {ChartDataPreparationService} from './services/globalVariableService/chart-data-preparation.service';
import {JWT_OPTIONS, JwtHelperService} from '@auth0/angular-jwt';
import {OperatingEnvironmentModule} from './components/operating-environment/operating-environment.module';
import {FilterComponent} from './shared/filter/filter.component';
import {WidgetsModule} from '@gravityilabs/sdz-widgets';


@NgModule({
  declarations: [
    AppComponent,
    TopBarComponent,
    StartingComponentComponent,
    SearchComponent,
    FilterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgxSpinnerModule,
    HttpClientModule,
    BrowserAnimationsModule,
    OperatingEnvironmentModule,
    WidgetsModule
  ],
  providers: [
    LoaderService,
    LanguageStringService,
    HttpServiceService,
    FiltersService,
    ChartDataPreparationService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpAppInterceptor,
      multi: true
    },
    {
      provide: JWT_OPTIONS,
      useValue: JWT_OPTIONS
    },
    JwtHelperService
  ],
  bootstrap: [AppComponent],
  exports: []
})
export class AppModule {
}
