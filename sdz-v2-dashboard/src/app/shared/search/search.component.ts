import { Component, OnInit, ViewChild, ElementRef , Directive, Output, EventEmitter, HostListener} from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  // tslint:disable-next-line:variable-name
  constructor(private _elementRef: ElementRef) { }
  @ViewChild('search') searchElement: ElementRef;
  show: boolean;
  name: string;

  @Output()
  public clickOutside = new EventEmitter<MouseEvent>();

  ngOnInit() {
    this.show = false;
  }
  showSearch() {
    this.show = !this.show;
  }

  @HostListener('document:click', ['$event', '$event.target'])
  public onClick(event: MouseEvent, targetElement: HTMLElement): void {
      const clickedInside = this._elementRef.nativeElement.contains(targetElement);
      if (!clickedInside) {
        this.show = false;
        this.clickOutside.emit(event);
      }
  }
}
