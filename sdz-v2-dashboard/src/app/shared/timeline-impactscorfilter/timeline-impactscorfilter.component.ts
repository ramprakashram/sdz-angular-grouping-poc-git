import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {Router} from '@angular/router';
import {HttpServiceService} from '../../services/https-service/https.service';

@Component({
  selector: 'app-timeline-impactscorfilter',
  templateUrl: './timeline-impactscorfilter.component.html',
  styleUrls: ['./timeline-impactscorfilter.component.scss']
})
export class TimelineImpactscorfilterComponent implements OnInit {
  @Input() yearActive;
  @Output() applyFilterEvent = new EventEmitter();
  // tslint:disable-next-line:variable-name
  impactTimeLineName: any;
  timeLineOne: any;
  timeLineTwo: any;
  timeLineThree: any;
  timeLinefour: any;
  timeLinefive: any;

  private expectedTimes: any = [];

  // tslint:disable-next-line:variable-name
  constructor(private router: Router, private _httpService: HttpServiceService) {
  }

  ngOnInit() {
    this._httpService.getExpectedTimes().subscribe(res => {
      const data: any = res;

      this.expectedTimes = data.items;

      // this.impactTimeLineName = data;
      // if (this.impactTimeLineName.items.length) {
      //   this.timeLineOne = this.impactTimeLineName.items[0].yearRange ? this.impactTimeLineName.items[0].yearRange : '';
      //   this.timeLineTwo = this.impactTimeLineName.items[1].yearRange ? this.impactTimeLineName.items[1].yearRange : '';
      //   this.timeLineThree = this.impactTimeLineName.items[2].yearRange ? this.impactTimeLineName.items[2].yearRange : '';
      //   if (this.impactTimeLineName.items[3]) {
      //     this.timeLinefour = this.impactTimeLineName.items[3].yearRange;
      //   }
      //   if (this.impactTimeLineName.items[4]) {
      //     this.timeLinefive = this.impactTimeLineName.items[4].yearRange;
      //   }
      // }
    });
  }

  applyFilterEventClicked(selectedYear) {
    this.applyFilterEvent.emit(selectedYear);
  }

}
