import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimelineImpactscorfilterComponent } from './timeline-impactscorfilter.component';

describe('TimelineImpactscorfilterComponent', () => {
  let component: TimelineImpactscorfilterComponent;
  let fixture: ComponentFixture<TimelineImpactscorfilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimelineImpactscorfilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimelineImpactscorfilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
