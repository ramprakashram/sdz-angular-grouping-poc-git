import {Component, OnInit} from '@angular/core';
import {AccordionWithCheckboxModel, CheckBoxModel, MultiSelectCheckBoxModel} from '@gravityilabs/sdz-models';
import {HttpServiceService} from '../../services/https-service/https.service';
import {AccordionFilterArray, FilterAccordionModel, FilterModel} from '../../models/filter-model';
import {FiltersService} from '../../services/globalVariableService/filters.service';
import {Router} from '@angular/router';

// import { AccordionFilterArray, FilterModel } from '../../models/filter-model';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit {

  // @Input() filterData: FilterModel;
  // @Output() filters = new EventEmitter<Array<CheckBoxModel>>();
  allFilter: FilterModel = new FilterModel();
  checkBoxFilters: Array<MultiSelectCheckBoxModel> = new Array<MultiSelectCheckBoxModel>();
  capabilityAccordionFilters: Array<AccordionFilterArray> = new Array<AccordionFilterArray>();
  strategyAccordionFilters: Array<AccordionFilterArray> = new Array<AccordionFilterArray>();

  constructor(private httpService: HttpServiceService, private filterService: FiltersService, private router: Router) {
    filterService.currentFilter.subscribe((currentFilter: SelectedFiltersModel) => {
      if (currentFilter) {
        const tempFilter = currentFilter;
        this.selectedFilterId = Object.assign({}, tempFilter);
      }
    });
  }

  selectedFilters: Array<CheckBoxModel> = new Array<CheckBoxModel>();
  selectedFilterId: SelectedFiltersModel = new SelectedFiltersModel();
  isLoadedFirstTime = true;

  // filters = {};

  ngOnInit() {
    this.getFilters();
  }

  getFilters() {
    this.httpService.getFilters(this.selectedFilterId).subscribe(
      (res: FilterModel) => {
        this.allFilter = new FilterModel();
        this.allFilter = res;
        this.checkBoxFilters = new Array<MultiSelectCheckBoxModel>();
        this.capabilityAccordionFilters = new Array<AccordionFilterArray>();
        this.strategyAccordionFilters = new Array<AccordionFilterArray>();
        this.renderCheckboxFilters();
        this.renderAccordionFilters();
        this.isLoadedFirstTime = false;
      }, err => {
        // console.log('filter err', err);
      }
    );
  }

  renderCheckboxFilters() {
    if (this.allFilter) {
      // Impact Score
      if (this.allFilter.impactScores && this.allFilter.impactScores.length > 0) {
        const impactCheckBox: MultiSelectCheckBoxModel = new MultiSelectCheckBoxModel();
        impactCheckBox.id = 1;
        impactCheckBox.title = 'IMPACT SCORE';
        impactCheckBox.checkboxData = new Array<CheckBoxModel>();
        this.allFilter.impactScores.forEach((eachImpactScore) => {
          if (eachImpactScore) {
            const checkBoxData = new CheckBoxModel();
            checkBoxData.id = eachImpactScore.id;
            checkBoxData.name = eachImpactScore.name;
            checkBoxData.parentId = impactCheckBox.id;
            if (this.selectedFilterId && this.selectedFilterId.impactScoreIds && this.selectedFilterId.impactScoreIds.length > 0) {
              if (this.selectedFilterId.impactScoreIds.includes(eachImpactScore.id)) {
                checkBoxData.checked = true;
                if (this.isLoadedFirstTime) {
                  this.selectedFilters.push(checkBoxData);
                }
              } else {
                checkBoxData.checked = false;
              }
            }
            impactCheckBox.checkboxData.push(checkBoxData);
          }
        });
        this.checkBoxFilters.push(impactCheckBox);
      }

      // Likelihood Filters
      if (this.allFilter.likelihoods && this.allFilter.likelihoods.length > 0) {
        const likelihoodCheckBox: MultiSelectCheckBoxModel = new MultiSelectCheckBoxModel();
        likelihoodCheckBox.id = 2;
        likelihoodCheckBox.title = 'LIKELIHOOD';
        likelihoodCheckBox.checkboxData = new Array<CheckBoxModel>();
        this.allFilter.likelihoods.forEach((eachLikelihood) => {
          if (eachLikelihood) {
            const checkBoxData = new CheckBoxModel();
            checkBoxData.id = eachLikelihood.id;
            checkBoxData.name = eachLikelihood.name;
            checkBoxData.parentId = likelihoodCheckBox.id;
            if (this.selectedFilterId && this.selectedFilterId.likelihoodIds && this.selectedFilterId.likelihoodIds.length > 0) {
              if (this.selectedFilterId.likelihoodIds.includes(eachLikelihood.id)) {
                checkBoxData.checked = true;
                if (this.isLoadedFirstTime) {
                  this.selectedFilters.push(checkBoxData);
                }
              } else {
                checkBoxData.checked = false;
              }
            }
            likelihoodCheckBox.checkboxData.push(checkBoxData);
          }
        });
        this.checkBoxFilters.push(likelihoodCheckBox);
      }

      // Business Units - Divisions Filter
      if (this.allFilter.divisions && this.allFilter.divisions.length > 0) {
        const divisionsCheckBox: MultiSelectCheckBoxModel = new MultiSelectCheckBoxModel();
        divisionsCheckBox.id = 5;
        divisionsCheckBox.title = 'DIVISIONS';
        divisionsCheckBox.checkboxData = new Array<CheckBoxModel>();
        this.allFilter.divisions.forEach((eachDivision) => {
          if (eachDivision) {
            const checkBoxData = new CheckBoxModel();
            checkBoxData.id = eachDivision.id;
            checkBoxData.name = eachDivision.name;
            checkBoxData.parentId = divisionsCheckBox.id;
            if (this.selectedFilterId && this.selectedFilterId.divisionId) {
              if (this.selectedFilterId.divisionId === eachDivision.id) {
                checkBoxData.checked = true;
                if (this.isLoadedFirstTime) {
                  this.selectedFilters.push(checkBoxData);
                }
              } else {
                checkBoxData.checked = false;
              }
            }
            divisionsCheckBox.checkboxData.push(checkBoxData);
          }
        });
        this.checkBoxFilters.push(divisionsCheckBox);
      }

      // Business Units - Branch Filter
      if (this.allFilter.branches && this.allFilter.branches.length > 0) {
        const branchesCheckBox: MultiSelectCheckBoxModel = new MultiSelectCheckBoxModel();
        branchesCheckBox.id = 4;
        branchesCheckBox.title = 'BRANCHES';
        branchesCheckBox.checkboxData = new Array<CheckBoxModel>();
        this.allFilter.branches.forEach((eachBranch) => {
          if (eachBranch) {
            const checkBoxData = new CheckBoxModel();
            checkBoxData.id = eachBranch.id;
            checkBoxData.name = eachBranch.name;
            checkBoxData.parentId = branchesCheckBox.id;
            if (this.selectedFilterId && this.selectedFilterId.branchId) {
              if (this.selectedFilterId.branchId === eachBranch.id) {
                checkBoxData.checked = true;
                if (this.isLoadedFirstTime) {
                  this.selectedFilters.push(checkBoxData);
                }
              } else {
                checkBoxData.checked = false;
              }
            }
            branchesCheckBox.checkboxData.push(checkBoxData);
          }
        });
        this.checkBoxFilters.push(branchesCheckBox);
      }

      // Business Units - Section Filter
      // if (this.allFilter.sections && this.allFilter.sections.length > 0) {
      //   const sectionsCheckBox: MultiSelectCheckBoxModel = new MultiSelectCheckBoxModel();
      //   sectionsCheckBox.id = 6;
      //   sectionsCheckBox.title = 'SECTIONS';
      //   sectionsCheckBox.checkboxData = new Array<CheckBoxModel>();
      //   this.allFilter.sections.forEach((eachSection) => {
      //     if (eachSection) {
      //       const checkBoxData = new CheckBoxModel();
      //       checkBoxData.id = eachSection.id;
      //       checkBoxData.name = eachSection.name;
      // checkBoxData.parentId = sectionsCheckBox.id;
      //       if (this.selectedFilterId && this.selectedFilterId.impactScoreIds && this.selectedFilterId.impactScoreIds.length > 0) {
      //         if (this.selectedFilterId.impactScoreIds.includes(eachSection.id)) {
      //           checkBoxData.checked = false;
      // if (this.isLoadedFirstTime) {
      //   this.selectedFilters.push(checkBoxData);
      // }
      //         } else {
      //           checkBoxData.checked = true;
      //         }
      //       }
      //       sectionsCheckBox.checkboxData.push(checkBoxData);
      //     }
      //   });
      //   this.checkBoxFilters.push(sectionsCheckBox);
      // }

      // Trend Sources Filters
      if (this.allFilter.trendSources && this.allFilter.trendSources.length > 0) {
        const trendSourcesCheckBox: MultiSelectCheckBoxModel = new MultiSelectCheckBoxModel();
        trendSourcesCheckBox.id = 3;
        trendSourcesCheckBox.title = 'TREND SOURCES';
        trendSourcesCheckBox.checkboxData = new Array<CheckBoxModel>();
        this.allFilter.trendSources.forEach((eachTrendSource) => {
          if (eachTrendSource) {
            const checkBoxData = new CheckBoxModel();
            checkBoxData.id = eachTrendSource.id;
            checkBoxData.name = eachTrendSource.name;
            checkBoxData.parentId = trendSourcesCheckBox.id;
            if (this.selectedFilterId && this.selectedFilterId.trendSourceIds && this.selectedFilterId.trendSourceIds.length > 0) {
              if (this.selectedFilterId.trendSourceIds.includes(eachTrendSource.id)) {
                checkBoxData.checked = true;
                if (this.isLoadedFirstTime) {
                  this.selectedFilters.push(checkBoxData);
                }
              } else {
                checkBoxData.checked = false;
              }
            }
            trendSourcesCheckBox.checkboxData.push(checkBoxData);
          }
        });
        this.checkBoxFilters.push(trendSourcesCheckBox);
      }
    }
  }

  renderAccordionFilters() {
    if (this.allFilter) {
      if (this.allFilter.capabilityGroups && this.allFilter.capabilityGroups.length > 0) {
        const capabilityData: AccordionFilterArray = new AccordionFilterArray();
        capabilityData.id = 11;
        capabilityData.title = 'CAPABILITY GROUP';
        capabilityData.accordionFilterData = new Array<AccordionWithCheckboxModel>();
        this.allFilter.capabilityGroups.forEach((eachCapabilityGroup) => {
          if (eachCapabilityGroup) {
            const capabilityGroupAccordion: AccordionWithCheckboxModel = new AccordionWithCheckboxModel();
            capabilityGroupAccordion.id = eachCapabilityGroup.id;
            capabilityGroupAccordion.name = eachCapabilityGroup.name;
            capabilityGroupAccordion.checkboxData = new Array<CheckBoxModel>();
            if (eachCapabilityGroup.capabilities && eachCapabilityGroup.capabilities.length > 0) {
              eachCapabilityGroup.capabilities.forEach((eachCapability) => {
                if (eachCapability) {
                  const capability: CheckBoxModel = new CheckBoxModel();
                  capability.id = eachCapability.id;
                  capability.name = eachCapability.name;
                  capability.companyId = eachCapability.companyId;
                  if (this.selectedFilterId && this.selectedFilterId.capabilityIds && this.selectedFilterId.capabilityIds.length > 0) {
                    if (this.selectedFilterId.capabilityIds.includes(eachCapability.id)) {
                      capability.checked = true;
                      if (this.isLoadedFirstTime) {
                        this.selectedFilters.push(capability);
                      }
                    } else {
                      capability.checked = false;
                    }
                  }
                  capabilityGroupAccordion.checkboxData.push(capability);
                }
              });
            }
            capabilityData.accordionFilterData.push(capabilityGroupAccordion);
          }
        });
        this.capabilityAccordionFilters.push(capabilityData);
      }
      if (this.allFilter.strategyGroups && this.allFilter.strategyGroups.length > 0) {
        const strategyData: AccordionFilterArray = new AccordionFilterArray();
        strategyData.id = 12;
        strategyData.title = 'STRATEGY GROUP';
        strategyData.accordionFilterData = new Array<AccordionWithCheckboxModel>();
        this.allFilter.strategyGroups.forEach((eachStrategyGroup) => {
          if (eachStrategyGroup) {
            const strategyGroupAccordion: AccordionWithCheckboxModel = new AccordionWithCheckboxModel();
            strategyGroupAccordion.id = eachStrategyGroup.id;
            strategyGroupAccordion.name = eachStrategyGroup.name;
            strategyGroupAccordion.checkboxData = new Array<CheckBoxModel>();
            if (eachStrategyGroup.strategies && eachStrategyGroup.strategies.length > 0) {
              eachStrategyGroup.strategies.forEach((eachStrategy) => {
                if (eachStrategy) {
                  const strategy: CheckBoxModel = new CheckBoxModel();
                  strategy.id = eachStrategy.id;
                  strategy.name = eachStrategy.name;
                  strategy.companyId = eachStrategy.companyId;
                  if (this.selectedFilterId && this.selectedFilterId.strategyIds && this.selectedFilterId.strategyIds.length > 0) {
                    if (this.selectedFilterId.strategyIds.includes(eachStrategy.id)) {
                      strategy.checked = true;
                      if (this.isLoadedFirstTime) {
                        this.selectedFilters.push(strategy);
                      }
                    } else {
                      strategy.checked = false;
                    }
                  }
                  strategyGroupAccordion.checkboxData.push(strategy);
                }
              });
            }
            strategyData.accordionFilterData.push(strategyGroupAccordion);
          }
        });
        this.strategyAccordionFilters.push(strategyData);
      }
    }
  }

  selectedCheckbox(checkbox: CheckBoxModel, parentID: number) {
    checkbox.parentId = parentID;
    if (checkbox.checked) {
      if (!this.checkForCheckBox(checkbox, 'push')) {
        this.selectedFilters.push(checkbox);
        this.selectedFilters = JSON.parse(JSON.stringify(this.selectedFilters));
        this.addToFilterIdArray(parentID, checkbox.id);
      } else {
        this.addToFilterIdArray(parentID, checkbox.id);
      }
    } else {
      this.checkForCheckBox(checkbox, 'pop');
      this.removeFromFilterIdArray(parentID, checkbox.id);
    }
    this.getFilters();
  }

  checkForCheckBox(checkbox: CheckBoxModel, mode: string) {
    if (this.selectedFilters && this.selectedFilters.length > 0) {
      // let index = 0;
      const parentData = this.getFilterDataBasedOnParentId(checkbox.parentId);
      if (this.addToFiltersBasedOnParentId(parentData, checkbox.id, checkbox.parentId, mode)) {
        return true;
      }
      // for (const eachFilter of this.selectedFilters) {
      //   if (eachFilter.id === checkbox.id) {
      //     if (mode === 'push') {
      //       return true;
      //     } else if (mode === 'pop') {
      //       console.log(index);
      //       this.selectedFilters.splice(index, 1);
      //       this.selectedFilters = JSON.parse(JSON.stringify(this.selectedFilters));
      //       return true;
      //     }
      //   }
      //   index++;
      // }
      return false;
    } else {
      if (this.selectedFilters && mode === 'push') {
        this.selectedFilters.push(checkbox);
        this.selectedFilters = JSON.parse(JSON.stringify(this.selectedFilters));
        return true;
      }
    }
  }

  getFilterDataBasedOnParentId(id: number) {
    if (id && this.selectedFilterId) {
      switch (id) {
        case 1:
          return this.selectedFilterId.impactScoreIds;
        case 2:
          return this.selectedFilterId.likelihoodIds;
        case 3:
          return this.selectedFilterId.trendSourceIds;
        case 4:
          return this.selectedFilterId.branchId;
        case 5:
          return this.selectedFilterId.divisionId;
        case 6:
          return this.selectedFilterId.sectionId;
        case 11:
          return this.selectedFilterId.capabilityIds;
        case 12:
          return this.selectedFilterId.strategyIds;
      }
    }
  }

  addToFiltersBasedOnParentId(parentData: any, currentFilterId: number, parentId: number, mode: string) {
    if (parentData && parentData.length > 0) {
      for (const eachParentData of parentData) {
        if (eachParentData === currentFilterId) {
          if (mode === 'push') {
            return true;
          } else if (mode === 'pop') {
            this.removeFromFilters(parentId, currentFilterId);
          }
        }
      }
    } else if (parentData === currentFilterId) {
      if (mode === 'push') {
        return true;
      } else if (mode === 'pop') {
        parentData = null;
        this.removeFromFilters(parentId, currentFilterId);
        return true;
      }
    }
  }

  removeFromFilters(parentId: number, id: number) {
    let index = 0;
    for (const eachFilter of this.selectedFilters) {
      if (eachFilter.parentId === parentId && eachFilter.id === id) {
        this.selectedFilters.splice(index, 1);
        this.selectedFilters = JSON.parse(JSON.stringify(this.selectedFilters));
        return true;
      }
      index++;
    }
  }

  deletedFilterTags(checkbox: CheckBoxModel) {
    this.removeFromFilterIdArray(checkbox.parentId, checkbox.id);
    if (checkbox.parentId <= 10) {
      if (this.checkBoxFilters && this.checkBoxFilters.length > 0) {
        this.checkBoxFilters.forEach((eachParentFilter) => {
          if (eachParentFilter.id === checkbox.parentId) {
            eachParentFilter.checkboxData.forEach((eachCheckboxFilter) => {
              if (eachCheckboxFilter.id === checkbox.id) {
                eachCheckboxFilter.checked = false;
                return;
              }
            });
          }
        });
      }
    } else if (checkbox.parentId === 11) {
      if (this.capabilityAccordionFilters && this.capabilityAccordionFilters.length > 0) {
        this.capabilityAccordionFilters.forEach((eachRootAccordion) => {
          if (eachRootAccordion && eachRootAccordion.id === checkbox.parentId) {
            if (eachRootAccordion.accordionFilterData && eachRootAccordion.accordionFilterData.length > 0) {
              eachRootAccordion.accordionFilterData.forEach((eachParentAccordion) => {
                if (eachParentAccordion) {
                  // Mapping Parent ID
                  // if (eachParentAccordion.id === checkbox.parentId) {
                  if (eachParentAccordion.checkboxData && eachParentAccordion.checkboxData.length > 0) {
                    eachParentAccordion.checkboxData.forEach((eachAccordionCheckbox) => {
                      if (eachAccordionCheckbox.id === checkbox.id) {
                        eachAccordionCheckbox.checked = false;
                      }
                    });
                  }
                  // }
                }
              });
            }
          }
        });
      }
    } else if (checkbox.parentId === 12) {
      if (this.strategyAccordionFilters && this.strategyAccordionFilters.length > 0) {
        this.strategyAccordionFilters.forEach((eachRootAccordion) => {
          if (eachRootAccordion && eachRootAccordion.id === checkbox.parentId) {
            if (eachRootAccordion.accordionFilterData && eachRootAccordion.accordionFilterData.length > 0) {
              eachRootAccordion.accordionFilterData.forEach((eachParentAccordion) => {
                if (eachParentAccordion) {
                  // Mapping Parent ID
                  // if (eachParentAccordion.id === checkbox.parentId) {
                  if (eachParentAccordion.checkboxData && eachParentAccordion.checkboxData.length > 0) {
                    eachParentAccordion.checkboxData.forEach((eachAccordionCheckbox) => {
                      if (eachAccordionCheckbox.id === checkbox.id) {
                        eachAccordionCheckbox.checked = false;
                      }
                    });
                  }
                  // }
                }
              });
            }
          }
        });
      }
    }
    this.getFilters();
  }

  addToFilterIdArray(parentId: number, selectedCheckboxId: number) {
    switch (parentId) {
      case 1:
        if (!this.selectedFilterId.impactScoreIds) {
          this.selectedFilterId.impactScoreIds = new Array<number>();
        }
        this.selectedFilterId.impactScoreIds.push(selectedCheckboxId);
        break;
      case 2:
        if (!this.selectedFilterId.likelihoodIds) {
          this.selectedFilterId.likelihoodIds = new Array<number>();
        }
        this.selectedFilterId.likelihoodIds.push(selectedCheckboxId);
        break;
      case 3:
        if (!this.selectedFilterId.trendSourceIds) {
          this.selectedFilterId.trendSourceIds = new Array<number>();
        }
        this.selectedFilterId.trendSourceIds.push(selectedCheckboxId);
        break;
      case 4:
        this.selectedFilterId.branchId = selectedCheckboxId;
        break;
      case 5:
        this.selectedFilterId.divisionId = selectedCheckboxId;
        break;
      case 6:
        this.selectedFilterId.sectionId = selectedCheckboxId;
        break;
      case 11:
        if (!this.selectedFilterId.capabilityIds) {
          this.selectedFilterId.capabilityIds = new Array<number>();
        }
        this.selectedFilterId.capabilityIds.push(selectedCheckboxId);
        break;
      case 12:
        if (!this.selectedFilterId.strategyIds) {
          this.selectedFilterId.strategyIds = new Array<number>();
        }
        this.selectedFilterId.strategyIds.push(selectedCheckboxId);
        break;
    }
  }

  removeFromFilterIdArray(parentId: number, unSelectedCheckboxId: number) {
    switch (parentId) {
      case 1:
        this.selectedFilterId.impactScoreIds = this.selectedFilterId.impactScoreIds.filter((eachImpactScore) => {
          return eachImpactScore !== unSelectedCheckboxId;
        });
        break;
      case 2:
        this.selectedFilterId.likelihoodIds = this.selectedFilterId.likelihoodIds.filter((eachLikelihood) => {
          return eachLikelihood !== unSelectedCheckboxId;
        });
        break;
      case 3:
        this.selectedFilterId.trendSourceIds = this.selectedFilterId.trendSourceIds.filter((eachTrendSource) => {
          return eachTrendSource !== unSelectedCheckboxId;
        });
        break;
      case 4:
        this.selectedFilterId.branchId = null;
        break;
      case 5:
        this.selectedFilterId.divisionId = null;
        break;
      case 6:
        this.selectedFilterId.sectionId = null;
        break;
      case 11:
        this.selectedFilterId.capabilityIds = this.selectedFilterId.capabilityIds.filter((eachCapability) => {
          return eachCapability !== unSelectedCheckboxId;
        });
        break;
      case 12:
        this.selectedFilterId.strategyIds = this.selectedFilterId.strategyIds.filter((eachStrategy) => {
          return eachStrategy !== unSelectedCheckboxId;
        });
    }
  }

  applyFilters() {
    this.filterService.updateFilterState(true);
    const tempFilterFinal = Object.assign({}, this.selectedFilterId);
    this.filterService.updateFilters(tempFilterFinal);
    if (this.router.url !== '/operative') {
      this.router.navigate(['/operative']);
    }
  }

  isMaxSelectable(eachCheckboxData: MultiSelectCheckBoxModel) {
    if (eachCheckboxData.id === 4 || eachCheckboxData.id === 5 || eachCheckboxData.id === 6) {
      return true;
    }
    return false;
  }

  clearFilters() {
    this.selectedFilterId = new SelectedFiltersModel();
    this.applyFilters();
  }
}

export class SelectedFiltersModel {
  impactScoreIds: Array<number> = new Array<number>();
  likelihoodIds: Array<number> = new Array<number>();
  strategyIds: Array<number> = new Array<number>();
  capabilityIds: Array<number> = new Array<number>();
  divisionId: number;
  branchId: number;
  sectionId: number;
  expectedTimeId: number;
  trendSourceIds: Array<number> = new Array<number>();
}
