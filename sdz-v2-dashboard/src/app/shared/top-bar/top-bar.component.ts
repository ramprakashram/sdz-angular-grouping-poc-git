import {Component, OnInit, OnChanges, Input} from '@angular/core';
import {Location} from '@angular/common';
import {Router, NavigationEnd} from '@angular/router';
import {Observable, Subscriber} from 'rxjs';
import {tap, map, filter} from 'rxjs/operators';
import {FiltersService} from '../../services/globalVariableService/filters.service';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {SelectedFiltersModel} from '../filter/filter.component';
import {PlatformLocation} from '@angular/common';
@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.scss'],
  animations: [
    trigger('arrowTrigger', [
      state('show-filter', style({
        right: '65px',
        transform: 'rotate(270deg)'
      })),
      state('hide-filter', style({
        transform: 'rotate(90deg)'
      })),
      transition('show-filter => hide-filter,hide-filter => show-filter', [
        animate('0.3s')
      ])
    ])
  ]
})
export class TopBarComponent implements OnInit {
  checkpass: any[] = [];
  headinglabel: string;
  titleCapabilities: string;
  titleBarLinke: boolean;
  applyActiveClass: boolean;
  backImage: boolean;
  isFilterEnabled = false;
  showFilters = false;
  filterCount = 0;
  private currentUrl: any;
  private titleStream: any;
  capabilityGruopUrl : any;
  constructor(private location: Location, private router: Router, private filterService: FiltersService,private platform: PlatformLocation) {
    this.router.events
      .subscribe((event) => {
        if (event instanceof NavigationEnd) {
          this.checkpass = event.url.split('/');
          switch (this.checkpass[1]) {
            case ' ':
              this.titleBarLinke = false;
              this.headinglabel = 'Futures Horizon Scan';
              this.backImage = true;
              this.isFilterEnabled = false;
              break;
            case 'operative':
              this.titleBarLinke = false;
              this.headinglabel = 'Operating Environment';
              this.backImage = false;
              this.isFilterEnabled = true;
              break;
            case 'threat_opportunity':
              this.titleBarLinke = false;
              this.headinglabel = 'Threats & Opportunities';
              this.backImage = false;
              this.isFilterEnabled = false;
              break;
            case 'organize':
              this.titleBarLinke = true;
              this.headinglabel = 'Organisational Impact';
              this.backImage = false;
              this.isFilterEnabled = false;
              break;
            default:
              this.titleBarLinke = false;
              this.headinglabel = 'Futures Horizon Scan';
              this.backImage = true;
              this.isFilterEnabled = false;
          }
        }
      });
    filterService.currentFilterState.subscribe((state) => {
      this.showFilters = false;
    });
    filterService.currentFilter.subscribe((currentFilter: SelectedFiltersModel) => {
      if (currentFilter) {
        this.getAppliedFilterCount(currentFilter);
      }
    });
  }

  ngOnInit() {
    //this.applyActiveClass = true;
  
    this.capabilityGruopUrl  = JSON.parse(localStorage.getItem('capabilityStrategicURLCheck'));
    if(this.capabilityGruopUrl == "strategic"){
      this.applyActiveClass = true;
    }else{
      this.applyActiveClass = false;
    }
    this.titleStream = 'Strategic Streams';
    this.titleCapabilities = 'Capability Groups';
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd ) {
        this.currentUrl = event.url;
        if (event.url === '/organize') {
          this.applyActiveClass = false;
        } else if (event.url === '/organize') {
          this.applyActiveClass = true;
        }
        if (event.url === '/organize/CapabilityGroupsComponentLink') {
          this.applyActiveClass = false;
        } else if (event.url === '/organize') {
          this.applyActiveClass = true;
        }
      }
    });
  }

  windowBack() {
    this.capabilityGruopUrl  = JSON.parse(localStorage.getItem('capabilityStrategicURLCheck'));
    if (this.isFilterEnabled) {
      this.filterService.updateFilterState(true);
      if (this.router.url === '/operative') {
        this.filterService.updateFilters(new SelectedFiltersModel());
      }
    }
    if(this.currentUrl == "/organize" || this.currentUrl == "/organize/CapabilityGroupsComponentLink" ){ 
      this.router.navigate(['/']);
    } else{
    if (this.checkpass[2]) {
      if(this.titleBarLinke){
           if(this.capabilityGruopUrl == "strategic"){
             if(this.currentUrl == "/organize"){
              this.router.navigate(['/']);
             } else{
             this.router.navigate(['/organize']);
             }
        
           } else{
          //  this.router.navigate([this.checkpass[1]]);
            this.router.navigate(['/organize/CapabilityGroupsComponentLink']);
          }
      }else{
        this.router.navigate([this.checkpass[1]]);
      }
   
    } else if (this.checkpass[1] !== '') {
      this.router.navigate(['/']);
    }
  }}

  getAppliedFilterCount(currentFilter: SelectedFiltersModel) {
    this.filterCount = 0;
    if (currentFilter.impactScoreIds) {
      this.filterCount += currentFilter.impactScoreIds.length;
    }
    if (currentFilter.likelihoodIds) {
      this.filterCount += currentFilter.likelihoodIds.length;
    }
    if (currentFilter.strategyIds) {
      this.filterCount += currentFilter.strategyIds.length;
    }
    if (currentFilter.capabilityIds) {
      this.filterCount += currentFilter.capabilityIds.length;
    }
    if (currentFilter.divisionId) {
      this.filterCount += 1;
    }
    if (currentFilter.branchId) {
      this.filterCount += 1;
    }
    if (currentFilter.sectionId) {
      this.filterCount += 1;
    }
    // if (currentFilter.expectedTimeId) {
    //   this.filterCount += 1;
    // }
    if (currentFilter.trendSourceIds) {
      this.filterCount += currentFilter.trendSourceIds.length;
    }
  }

  getNavigationLink(text) {
      if (text === 'Strategic Streams') {
        this.applyActiveClass = true;
        this.router.navigate(['/organize']);
      } else if (text === 'Capability Groups') {
        this.applyActiveClass = false;
        this.router.navigate(['/organize/CapabilityGroupsComponentLink']);
      }
  }

  navigateReport(){
    const origin: string = (this.platform as any).location.origin;
    window.open( origin + "/HorizonScan/FuturesReport")
  }
}
