import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThredopportunityListpageComponent } from './thredopportunity-listpage.component';

describe('ThredopportunityListpageComponent', () => {
  let component: ThredopportunityListpageComponent;
  let fixture: ComponentFixture<ThredopportunityListpageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThredopportunityListpageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThredopportunityListpageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
