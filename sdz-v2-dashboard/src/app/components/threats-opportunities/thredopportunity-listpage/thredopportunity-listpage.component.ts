import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  Output,
  EventEmitter,
  Input,
  ViewChildren,
  QueryList
} from '@angular/core';

@Component({
  selector: 'app-thredopportunity-listpage',
  templateUrl: './thredopportunity-listpage.component.html',
  styleUrls: ['./thredopportunity-listpage.component.scss']
})
export class ThredopportunityListpageComponent implements OnInit {
  @ViewChild('detailsView') searchElement: ElementRef;
  @Output() countChanged: EventEmitter<any> = new EventEmitter();
  @Input() temp: any;
  public tempJSON: any = {};
  public tempJSONIcheck: any = {};
  public tempJSONLoop: any = [];
  public ImgURL: string;
  show: boolean;
  selectedIndex = -1;
  name: string;
  showDiv: number;
  timeLineImapctYearID: any;
  nativeDiv: any;
  @ViewChildren('div') divs: QueryList<any>;

  constructor() {
  }

  ngOnInit() {

    this.timeLineImapctYearID = JSON.parse(localStorage.getItem('threatsOpportunityTimeID'));
    this.tempJSONIcheck = JSON.parse(localStorage.getItem('ThreadOpportunityDetail'));
    this.tempJSON = this.temp;
    this.name = !this.tempJSONIcheck.isOpportunity ? 'Threats' : 'Opportunities';
    // tslint:disable-next-line:max-line-length
    this.ImgURL = !this.tempJSONIcheck.isOpportunity ? './assets/Images/rightOrangeArrow.svg' : './assets/Images/rightGreenDropDownImg.svg';
    this.show = false;
    this.showDiv = 0;
  }

  // showdetailsView(e) {
  //   const childImage = e.target;
  //   const showDiv = e.target.parentNode.nextSibling;
  //   if (showDiv.style.display === '') {
  //     childImage.className = 'rotateDownDirection';
  //     showDiv.style.display = 'block';
  //   } else if (showDiv.style.display === 'block') {
  //     childImage.className = '';
  //     showDiv.style.display = 'none';
  //   } else if (showDiv.style.display === 'none') {
  //     childImage.className = 'rotateDownDirection';
  //     showDiv.style.display = 'block';
  //   }
  //
  // }
  showdetailsView(i) {

    this.divs.toArray().forEach((x, index) => {
      if (index === i) {
        const selectDIvImage = x;
        this.nativeDiv = selectDIvImage.nativeElement;
      }
    });
    const childImage = this.nativeDiv.children[2].children[0];
    const showDiv = this.nativeDiv.children[3];
      if (showDiv.style.display === '') {
        childImage.className = 'rotateDownDirection';
        showDiv.style.display = 'block';
      } else if (showDiv.style.display === 'block') {
        childImage.className = '';
        showDiv.style.display = 'none';
      } else if (showDiv.style.display === 'none') {
        childImage.className = 'rotateDownDirection';
        showDiv.style.display = 'block';
      }
  }
  navigationLink(data) {
    localStorage.setItem('ThreadOpportunitySubcategoryDetail', JSON.stringify(data));
    this.showDiv = 1;
    this.countChanged.emit(this.showDiv);
  }
}
