import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ThreatsOpportunitiesRoutingModule} from './threats-opportunities-routing.module';
import {ThreatsopportLandingComponent} from './threatsopport-landing/threatsopport-landing.component';
import {ThredopportSVGComponent} from './thredopport-svg/thredopport-svg.component';
import {ThredopportunityListpageComponent} from './thredopportunity-listpage/thredopportunity-listpage.component';
import {ThredopportunityDetailsPageComponent} from './thredopportunity-details-page/thredopportunity-details-page.component';


// tslint:disable-next-line:max-line-length
import {CommonthredopportunityListDetailPageComponent} from './commonthredopportunity-list-detail-page/commonthredopportunity-list-detail-page.component';


import {OperatingEnvironmentModule} from '../operating-environment/operating-environment.module';
import {LanguageStringPipe} from "../../pipes/language-string.pipe";
import {TrendCategoryDetailsComponent} from "../operating-environment/trend-category-details/trend-category-details.component";
import {TimelineImpactscorfilterComponent} from "../../shared/timeline-impactscorfilter/timeline-impactscorfilter.component";
import {OrderByPipe} from "../../pipes/order-by.pipe";
import {TransitionGroupComponent, TransitionGroupItemDirective} from "../../shared/animations/animationGroup";

// tslint:disable-next-line:import-spacing
// import {trendCategoryComponent} from './../operating-environment/show-trend-category/show-trend-category.component';

@NgModule({
  declarations: [ThreatsopportLandingComponent,
    ThredopportSVGComponent,
    ThredopportunityListpageComponent,
    ThredopportunityDetailsPageComponent,
    CommonthredopportunityListDetailPageComponent,],
  imports: [
    CommonModule,
    ThreatsOpportunitiesRoutingModule,
    OperatingEnvironmentModule
  ],
  exports: [ThredopportunityDetailsPageComponent]
})
export class ThreatsOpportunitiesModule {
}
