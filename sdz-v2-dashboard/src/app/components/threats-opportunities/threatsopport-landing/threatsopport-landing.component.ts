import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {HttpServiceService} from '../../../services/https-service/https.service';

import {FiltersService} from '../../../services/globalVariableService/filters.service';
import {
  animate,
  state,
  style,
  transition,
  trigger,
  keyframes
} from '@angular/animations';
import * as d3 from 'd3';
import {D3Method, ObjectOperation} from '../../../helpers/commonUtils';

@Component({
  selector: 'app-threatsopport-landing',
  templateUrl: './threatsopport-landing.component.html',
  styleUrls: ['./threatsopport-landing.component.scss'],
  animations: [
    // trigger('move', [
    //   state('in', style({transform: 'translateX(0)'})),
    //   transition('void => left', [
    //     style({transform: 'translateX(300%)'}),
    //     animate(200)
    //   ]),
    //   transition('left => void', [
    //     animate(200, style({transform: 'translateX(0)'}))
    //   ]),
    //   transition('void => right', [
    //     style({transform: 'translateX(-100%)'}),
    //     animate(200)
    //   ]),
    //   transition('right => void', [
    //     animate(200, style({transform: 'translateX(0)'}))
    //   ])
    // ])
  ]
})
export class ThreatsopportLandingComponent implements OnInit {
  public threadJSON: any;
  // public OpportunityJson: any = [];
  public renderData: any = {};
  public state = 'void';
  public threatsState = 'void';
  private isOpportunity = true;
  // isSliderShow = false;
  private timeLineImapctYearID: any;
  public disableSliderButtons = false;
  threatsCountLabel: any;
  opportunityCountLabel: any;
  filteTimeID: any;
  private filters: any = {};
  private carousalItemWidth = 200;
  private carousalOffsetWidth = 200;
  private carousaLimit: number = Math.floor((window.innerWidth - this.carousalOffsetWidth) / this.carousalItemWidth);
  private carousaStep: number = this.carousaLimit;
  private threatCarousalStart: number = 0;
  windowHeight: any;
  marginTop: any;

  // tslint:disable-next-line:variable-name
  constructor(private router: Router, private _httpService: HttpServiceService, private filtersService: FiltersService) {
  }

  ngOnInit() {
    this.windowHeight = JSON.parse(localStorage.getItem('windowOuterHeight'));
    if (this.windowHeight < 0.8) {
      this.marginTop = -100;
    }
    // @ts-ignore
    this.filters = this.filtersService.ThreatOpportunityData();
    this.getThreatsOpportuntyDataFun(this.filters.expectedTime);
  }

  navigationLink(data: any) {
    localStorage.setItem('ThreadOpportunityDetail', JSON.stringify(data));
    this.router.navigate(['/threat_opportunity/listDetail']);
  }

  getThreatsOpportuntyDataFun(data) {
    this.timeLineImapctYearID = data;
    this._httpService.getHorizonThreatsandOpportunities(data).subscribe(res => {
      if (!Object.keys(this.renderData).length) {
        this.renderData = res;
        this.threatsCountLabel = this.renderData.threatsCount;
        this.opportunityCountLabel = this.renderData.opportunitiesCount;
      } else {
        this.threatsCountLabel = res.threatsCount;
        this.opportunityCountLabel = res.opportunitiesCount;
        this.renderData.threatCategorySummary.map((d, i) => {
          const dataElem = ObjectOperation.getArrayElemByAttributeValue('categoryName', d.categoryName, res.threatCategorySummary);
          // tslint:disable-next-line:forin

          for (const k in d) {
            d[k] = dataElem[k];
          }
          return d;
        });
        this.renderData.opportunityCategorySummary.map((d, i) => {
          const dataElem = ObjectOperation.getArrayElemByAttributeValue('categoryName', d.categoryName, res.opportunityCategorySummary);
          // tslint:disable-next-line:forin
          for (const k in d) {
            d[k] = dataElem[k];
          }
          return d;
        });
      }

      const impactScoreRangeThreat = ObjectOperation.getMinMax(this.renderData.threatCategorySummary, 'impactScore');
      const scaleThreat = D3Method.getLinearScale(impactScoreRangeThreat, [0.7, 1]);
      // tslint:disable-next-line:only-arrow-functions
      this.renderData.threatCategorySummary.map((d) => {
        d.isThreat = false;
        d.scale = scaleThreat(d.impactScore);
      });

      const impactScoreRangeOpportunity = ObjectOperation.getMinMax(this.renderData.opportunityCategorySummary, 'impactScore');
      const scaleOpportunity = D3Method.getLinearScale(impactScoreRangeOpportunity, [0.7, 1]);
      this.renderData.opportunityCategorySummary.map((d) => {
        d.isOpportunity = true;
        d.scale = scaleOpportunity(d.impactScore);
      });
    });
  }

  yearFilterSelected(filter) {
    this.filters.expectedTime = filter;
    localStorage.setItem('threatsOpportunityTimeID', JSON.stringify(filter));
    this.getThreatsOpportuntyDataFun(filter);
  }

  imageRotate(arr, reverse) {
    if (reverse) {
      arr.unshift(arr.pop());
    } else {
      arr.push(arr.shift());
    }
    return arr;
  }

  moveLeft() {
    if (this.disableSliderButtons) {
      return;
    }
    this.state = 'right';
    this.imageRotate(this.threadJSON, true);
  }

  moveRight() {
    if (this.disableSliderButtons) {
      return;
    }
    this.state = 'left';
    this.imageRotate(this.threadJSON, false);
  }

  onFinish($event) {
    this.state = 'void';
    this.disableSliderButtons = false;
  }

  onStart($event) {
    this.disableSliderButtons = true;
  }


  moveLeftSecond(data) {
    if (this.disableSliderButtons) {
      return;
    }
    this.threatsState = 'left';
    this.imageRotate(data, true);
  }

  moveRightSecond(data) {
    if (this.disableSliderButtons) {
      return;
    }
    this.threatsState = 'right';
    this.imageRotate(data, false);
  }

  onFinishSecond($event) {
    this.threatsState = 'void';
    this.disableSliderButtons = false;
  }

  onStartSecond($event) {
    this.disableSliderButtons = true;
  }
}
