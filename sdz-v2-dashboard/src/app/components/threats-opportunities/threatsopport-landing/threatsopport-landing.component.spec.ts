import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThreatsopportLandingComponent } from './threatsopport-landing.component';

describe('ThreatsopportLandingComponent', () => {
  let component: ThreatsopportLandingComponent;
  let fixture: ComponentFixture<ThreatsopportLandingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThreatsopportLandingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThreatsopportLandingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
