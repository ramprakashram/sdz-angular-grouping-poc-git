import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ThreatsopportLandingComponent } from './threatsopport-landing/threatsopport-landing.component';
import { ThredopportSVGComponent } from './thredopport-svg/thredopport-svg.component';
import { ThredopportunityListpageComponent } from './thredopportunity-listpage/thredopportunity-listpage.component';
import { ThredopportunityDetailsPageComponent } from './thredopportunity-details-page/thredopportunity-details-page.component';
// tslint:disable-next-line:max-line-length
import { CommonthredopportunityListDetailPageComponent } from './commonthredopportunity-list-detail-page/commonthredopportunity-list-detail-page.component';


const routes: Routes = [
  {
    path : '',
    component : ThreatsopportLandingComponent, children: [
      { path: 'ThredopportSVG', component: ThredopportSVGComponent },
    ]
  },
  { path: 'listDetail', component: CommonthredopportunityListDetailPageComponent },
   { path: 'ThredopportunityListpage', component: ThredopportunityListpageComponent },
   { path: 'ThredopportunityDetailsPage', component: ThredopportunityDetailsPageComponent },
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ThreatsOpportunitiesRoutingModule { }
