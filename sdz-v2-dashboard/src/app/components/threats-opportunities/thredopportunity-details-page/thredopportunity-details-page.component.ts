import { Component, OnInit, ElementRef, ViewChild, Output, EventEmitter, Input } from '@angular/core';
@Component({
  selector: 'app-thredopportunity-details-page',
  templateUrl: './thredopportunity-details-page.component.html',
  styleUrls: ['./thredopportunity-details-page.component.scss']
})
export class ThredopportunityDetailsPageComponent implements OnInit {
  @ViewChild('detailsView') searchElement: ElementRef;
  @Output() countChanged: EventEmitter<number> =   new EventEmitter();
  @Input() temp: any = {} ;
  show: boolean;
  trendDetailDIv: boolean;
  threadDetailDiv: boolean;
  name: string;
  showDiv: number;
  public tempJSON: any = {};
  public tempJSONRender: any = {};
  public tempJSONIcheck: any = {};

  constructor() { }

  ngOnInit() {
    this.threadDetailDiv = true;
    this.trendDetailDIv = false;
    this.tempJSON = this.temp;
    this.tempJSONRender = JSON.parse(localStorage.getItem('ThreadOpportunitySubcategoryDetail'));
    this.tempJSONIcheck = JSON.parse(localStorage.getItem('ThreadOpportunityDetail'));
    this.name = !this.tempJSONIcheck.isOpportunity ? 'THREAT' : 'OPPORTUNITY';
    console.log("tempJSONRender ",this.tempJSONRender)
  }
  navigationLink() {
    this.showDiv = 2;
    this.countChanged.emit(this.showDiv);
  }
  trendDetaildPopupFun() {
    localStorage.setItem('trendIDForAPICall', JSON.stringify(this.tempJSONRender.trendId));
    this.trendDetailDIv = !this.trendDetailDIv;
    this.threadDetailDiv = !this.threadDetailDiv;
  }
  countChangedHandler(count: number) {
    this.trendDetailDIv = !this.trendDetailDIv;
    this.threadDetailDiv = !this.threadDetailDiv;
  }
}
