import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThredopportunityDetailsPageComponent } from './thredopportunity-details-page.component';

describe('ThredopportunityDetailsPageComponent', () => {
  let component: ThredopportunityDetailsPageComponent;
  let fixture: ComponentFixture<ThredopportunityDetailsPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThredopportunityDetailsPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThredopportunityDetailsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
