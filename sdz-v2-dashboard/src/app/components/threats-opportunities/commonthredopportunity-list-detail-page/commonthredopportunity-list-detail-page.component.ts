import {Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import {HttpServiceService} from '../../../services/https-service/https.service';

@Component({
  selector: 'app-commonthredopportunity-list-detail-page',
  templateUrl: './commonthredopportunity-list-detail-page.component.html',
  styleUrls: ['./commonthredopportunity-list-detail-page.component.scss']
})
export class CommonthredopportunityListDetailPageComponent implements OnInit {
  @ViewChild('detailsView') searchElement: ElementRef;
  showDiv: boolean;
  showthreadDetailsDiv: boolean;
  threadOppsubCategoryRenderData: any;
  threadOppsubCategory: any;
  threadOppsubCategoryummaryData: any;
  threadOppsubDetailCountData: any;
  selectCetagory: any;
  selectCetagoryName: string;
  timeID: any;

  // tslint:disable-next-line:variable-name
  constructor(private _httpService: HttpServiceService) {
  }

  ngOnInit() {
    this.threadOppsubCategory = JSON.parse(localStorage.getItem('ThreadOpportunityDetail'));
    setTimeout(() => {
      this.showDiv = false;
      this.showthreadDetailsDiv = true;
    }, 500);
    this.selectCetagory = this.threadOppsubCategory.isOpportunity ? 'opportunity' : 'threat';
    this.selectCetagoryName = this.threadOppsubCategory.isOpportunity ? 'Opportunities' : 'Threats';
    this.timeID = JSON.parse(localStorage.getItem('threatsOpportunityTimeID'));
    if (this.threadOppsubCategory.isOpportunity) {
      this._httpService.getOpportunityCategories(this.threadOppsubCategory.categoryId, this.timeID).subscribe(res => {
        this.threadOppsubCategoryRenderData = res;
        this.threadOppsubCategoryummaryData = this.threadOppsubCategoryRenderData.opportunitiesSummary;
        this.threadOppsubDetailCountData = this.threadOppsubCategoryRenderData.totalOpportunitiesCount;
      });
    } else {
      this._httpService.getThreatCategories(this.threadOppsubCategory.categoryId, this.timeID).subscribe(res => {
        this.threadOppsubCategoryRenderData = res;
        this.threadOppsubCategoryummaryData = this.threadOppsubCategoryRenderData.threatsSummary;
        this.threadOppsubDetailCountData = this.threadOppsubCategoryRenderData.totalThreatCount;
      });
    }
  }

  countChangedHandler(count: number) {
    setTimeout(() => {
      this.showDiv = !this.showDiv;
      this.showthreadDetailsDiv = !this.showthreadDetailsDiv;
    }, 500);
  }
}


