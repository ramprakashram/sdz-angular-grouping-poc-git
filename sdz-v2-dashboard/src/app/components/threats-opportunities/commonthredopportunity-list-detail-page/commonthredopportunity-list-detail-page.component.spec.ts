import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommonthredopportunityListDetailPageComponent } from './commonthredopportunity-list-detail-page.component';

describe('CommonthredopportunityListDetailPageComponent', () => {
  let component: CommonthredopportunityListDetailPageComponent;
  let fixture: ComponentFixture<CommonthredopportunityListDetailPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommonthredopportunityListDetailPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommonthredopportunityListDetailPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
