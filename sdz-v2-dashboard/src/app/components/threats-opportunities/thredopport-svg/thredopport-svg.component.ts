import {Component, OnInit, Input} from '@angular/core';
import * as d3 from 'd3';
import {Router} from '@angular/router';
import {animate, state, style, transition, trigger} from "@angular/animations";

@Component({
  selector: 'app-thredopport-svg',
  templateUrl: './thredopport-svg.component.html',
  styleUrls: ['./thredopport-svg.component.scss'],
  animations: [
    trigger('simpleFadeAnimation', [
      state('in', style({opacity: 1})),
      transition(':enter', [
        style({opacity: 0}),
        animate(1500 )
      ]),
      transition(':leave',
        animate(1500, style({opacity: 0})))
    ])
  ]
})
export class ThredopportSVGComponent implements OnInit {
  @Input() categoryName: string;
  @Input() categoryCount: number;
  @Input() svgScale: any = {};
  @Input() threadSVG: string;
  @Input() ladingPage: boolean;
  constructor(private router: Router) {
  }

  ngOnInit() {
  }

  // draw() {
  //   const SVGChart = d3.select('svg');
  //   SVGChart.attr('viewBox', '0 0 235.878 235.878');
  //   SVGChart.attr('width', '200');
  //   SVGChart.attr('height', '200');
  //   const SVGGroup = SVGChart.append('g');
  //   SVGGroup.attr('transform', 'translate(3862.439 -182.561)');
  //   const rectA = SVGGroup.append('rect')
  //            .attr('class', 'a')
  //           .attr('width', '221')
  //           .attr('height', '221')
  //           .attr('rx', '13')
  //           .attr('fill', '#ff6060')
  //           .attr('opacity', '0.2')
  //           .attr('transform', 'matrix(0.998, -0.07, 0.07, 0.998, -3862.439, 197.977)');
  //   const rectB = SVGGroup.append('rect')
  //            .attr('class', 'b')
  //           .attr('width', '221')
  //           .attr('height', '221')
  //           .attr('rx', '13')
  //           .attr('fill', '#ff6060')
  //           .attr('opacity', '0.5')
  //           .attr('transform', 'matrix(0.998, 0.07, -0.07, 0.998, -3847.023, 182.561)');
  //   const rectC = SVGGroup.append('rect')
  //           .attr('class', 'C')
  //          .attr('width', '219')
  //          .attr('height', '219')
  //          .attr('rx', '13')
  //          .attr('fill', '#fff')
  //          .attr('transform', 'translate(-3854 191)');

  //   SVGChart.append('text')
  //          .text('Operational')
  //          .attr('y', 120)
  //          .attr('x', 50)
  //          .attr('font-size', 25)
  //          .attr('font-family', 'SegoeUI-SemiBold')
  //          .attr('fill', '#4C5E7A');
  //   SVGChart.append('text')
  //          .text('302')
  //          .attr('y', 150)
  //          .attr('x', 100)
  //          .attr('font-size', 25)
  //          .attr('font-family', 'SegoeUI-SemiBold')
  //          .attr('fill', '#008AD1');
  // }
  // navigationLink() {
  //   const SVGChart = d3.select('svg');
  //   SVGChart.transition()
  //   .duration(1000)
  //   .attr('viewBox', '12 24 430 450')
  //   .attr('width', '600')
  //   .attr('height', '600');
  //   setTimeout(() => {
  //      this.router.navigate(['/ThreatsOpportunitiesModule/CommonthredopportunityListDetailPage']);
  //         }, 1000);
  // }
}
