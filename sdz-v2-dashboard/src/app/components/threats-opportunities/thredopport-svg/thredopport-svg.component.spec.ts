import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThredopportSVGComponent } from './thredopport-svg.component';

describe('ThredopportSVGComponent', () => {
  let component: ThredopportSVGComponent;
  let fixture: ComponentFixture<ThredopportSVGComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThredopportSVGComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThredopportSVGComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
