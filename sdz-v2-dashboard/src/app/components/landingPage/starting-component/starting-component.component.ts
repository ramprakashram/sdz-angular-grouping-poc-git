import {Component, OnInit} from '@angular/core';
import {trigger, state, style, animate, transition} from '@angular/animations';
import {HttpServiceService} from '../../../services/https-service/https.service';
import {Router, NavigationEnd} from '@angular/router';
import {FiltersService} from '../../../services/globalVariableService/filters.service';

@Component({
  selector: 'app-starting-component',
  templateUrl: './starting-component.component.html',
  styleUrls: ['./starting-component.component.scss'],
  animations: [
    trigger('OperatingEnvironmentModule', [
      state('initial', style({})),
      state('final', style({
        zIndex: 5,
        width: window.innerWidth + 'px',
        height: (window.innerHeight - 56) + 'px',
        position: 'relative',
        top: '-208px',
        right: '115px',
        'border-radius': 3
      })),
      transition('initial=>final', animate('1500ms')),
      transition('final=>initial', animate('1500ms'))
    ]),
    trigger('ThreatsOpportunitiesModule', [
      state('initial', style({})),
      state('final', style({
        zIndex: 5,
        top: '-208px',
        right: '600px',
        width: window.innerWidth + 'px',
        height: (window.innerHeight - 56) + 'px',
        position: 'relative',
        'border-radius': 3
      })),
      transition('initial=>final', animate('1500ms')),
      transition('final=>initial', animate('1500ms'))
    ]),
    trigger('OrganisationImpactModule', [
      state('initial', style({})),
      state('final', style({
        zIndex: 5,
        top: '-208px',
        right: '1050px',
        width: window.innerWidth + 'px',
        height: (window.innerHeight - 56) + 'px',
        position: 'relative',
        'border-radius': 3
      })),
      transition('initial=>final', animate('1500ms')),
      transition('final=>initial', animate('1500ms'))
    ])
  ]
})
export class StartingComponentComponent implements OnInit {
  public renderTempData: any = {};
  public OperatingEnvironmentModuleEnvironment: any = {};
  public threadOpportunity: any = {};
  public impactOrganization: any = {};
  currentStateDivOne = 'initial';
  currentStateDivTwo = 'initial';
  currentStateDivThird = 'initial';
  statusDivOne = false;
  statusDivSecond = false;
  statusDivThird = false;
  introText = 'The Futures Horizon Scan enables the AFP to monitor changes and impact on the operating environment';
  private operatingEnvironmentFilters: any = {};
  private threatsAndOpportunitiesFilters: any = {};
  private capabilityImpactFilters: any = {};

  // tslint:disable-next-line:variable-name
  constructor(private router: Router, private _httpService: HttpServiceService, private filterService: FiltersService) {
  }


  ngOnInit() {
    localStorage.removeItem('windowOuterHeight');
    localStorage.removeItem('capabilityStrategicURLCheck');
    localStorage.removeItem('timeLineYearIDImapct');
    localStorage.removeItem('OrganizationImpactDetail');
    localStorage.removeItem('TrendCategorData');
    localStorage.removeItem('capabilityGroups');
    localStorage.removeItem('trendIDForAPICall');
    localStorage.removeItem('trendIDCapabilityForAPICall');
    localStorage.removeItem('ThreadOpportunitySubcategoryDetail');
    localStorage.removeItem('ThreadOpportunityDetail');
    localStorage.removeItem('trendIDForAPICall');
    localStorage.removeItem('ThreadOpportunityDetail');
    localStorage.removeItem('trendIDForTimeIDAPICall');
    localStorage.removeItem('ThreadOpportunitySubcategoryDetail');
    localStorage.removeItem('ThreadOpportunityDetail');
    localStorage.removeItem('ThreadOpportunityDetail');
    localStorage.removeItem('threatOpportunityImapctScore');
    localStorage.removeItem('threatsOpportunityTimeID');
    localStorage.removeItem('SubCategoriesDetail');
    localStorage.removeItem('timeLineYearID');
    localStorage.removeItem('ThreadOpportunitySubcategoryDetail');
    localStorage.removeItem('trendIDForAPICall');
    this.operatingEnvironmentFilters = this.filterService.getOperatingEnvironmentFilters();
    this.operatingEnvironmentFilters.expectedTimeId = null;
    this.capabilityImpactFilters = this.filterService.getimpactOrganisationFilter();
    this.capabilityImpactFilters = this.filterService.getimpactOrganisationStrategicFilter();

    this.capabilityImpactFilters.expectedTimeId = null;
    this.threatsAndOpportunitiesFilters = this.filterService.ThreatOpportunityData();
    this.threatsAndOpportunitiesFilters.expectedTimeId = null;


    this._httpService.getHorizonSummary().subscribe(res => {
      const renderTempDataconst = res;
      this.renderTempData = renderTempDataconst;
    });

    this.OperatingEnvironmentModuleEnvironment = {
      isCheckValue: true,
      CountLabel: this.renderTempData.totalOpportunitiesCount,
      HyperLinks1: '@OperatingEnvironmentModule',
      HyperLinks: 'operative',
      isValid: true,
      ImgLinks: './assets/gif/operating-environment.gif',
      textHeading: 'OUR',
      textHeadingSecond: 'OPERATING ENVIRONMENT',
      textparagraph: 'Know the various trends affecting the operating environment in your organisation',
      totalTrends: this.renderTempData.totalTrendsCount,
      trendCategories: this.renderTempData.totalCategoriesCount,
      // tslint:disable-next-line:max-line-length
      lastUpdate: !this.renderTempData.trendLastUpdatedDaysAgo ? '0' : this.renderTempData.trendLastUpdatedDaysAgo,
    };
    this.threadOpportunity = {
      isCheckValue: false,
      CountLabel: '0',
      HyperLinks: 'threat_opportunity',
      HyperLinks1: '@ThreatsOpportunitiesModule',
      isValid: true,
      ImgLinks: './assets/gif/ThreatsOpportunites.gif',
      textHeading: 'EMERGING',
      textHeadingSecond: ' THREATS & OPPORTUNITIES',
      textparagraph: 'Understand the threats and opportunities emerging in the operating environment',
      totalTrends: this.renderTempData.totalThreatsCount,
      trendCategories: this.renderTempData.totalOpportunitiesCount,
      // tslint:disable-next-line:max-line-length
      lastUpdate: !this.renderTempData.threatAndOpportunityLastUpdatedDaysAgo ? '0' : this.renderTempData.threatAndOpportunityLastUpdatedDaysAgo,
    };

    this.impactOrganization = {
      isCheckValue: false,
      CountLabel: '0',
      HyperLinks: 'organize',
      HyperLinks1: '@OrganisationImpactModule',
      isValid: false,
      ImgLinks: './assets/gif/Organisation.gif',
      textHeading: 'IMPACT ON OUR',
      textHeadingSecond: 'ORGANISATION',
      textparagraph: 'Understand the impact on the organisational structure, strategies and capabilities',
      totalTrends: this.renderTempData.totalThreatsCount,
      trendCategories: this.renderTempData.totalCategoriesCount,
      // tslint:disable-next-line:max-line-length
      lastUpdate: !this.renderTempData.impactedLastUpdatedDaysAgo ? '0' : this.renderTempData.impactedLastUpdatedDaysAgo,
    };
  }

  navigateTOSecondComponent(JsonData: any, event: any) {
    // this._globalVar.animationPrvURL = JsonData;
    if (!this.statusDivOne) {
      this.statusDivOne = !this.statusDivOne;
    } else {
      setTimeout(() => {
        this.statusDivOne = !this.statusDivOne;
      }, 1500);
    }
    // this.router.navigate([JsonData]);
    this.currentStateDivOne = this.currentStateDivOne === 'initial' ? 'final' : 'initial';
    setTimeout(() => {
     this.router.navigate([JsonData]);
      }, 0);
  }

  navigateTOSecondComponent1(JsonData: any, event: any) {
    // this._globalVar.animationPrvURL = JsonData;
    if (!this.statusDivSecond) {
      this.statusDivSecond = !this.statusDivSecond;
    } else {
      setTimeout(() => {
        this.statusDivSecond = !this.statusDivSecond;
      }, 1500);
    }
   // this.router.navigate([JsonData]);
    this.currentStateDivTwo = this.currentStateDivTwo === 'initial' ? 'final' : 'initial';
    setTimeout(() => {
   this.router.navigate([JsonData]);
    }, 0);
  }

  navigateTOSecondComponen2(JsonData: any, event: any) {
    // this._globalVar.animationPrvURL = JsonData;
    if (!this.statusDivThird) {
      this.statusDivThird = !this.statusDivThird;
    } else {
      setTimeout(() => {
        this.statusDivThird = !this.statusDivThird;
      }, 1500);
    }
    // this.router.navigate([JsonData]);
    this.currentStateDivThird = this.currentStateDivThird === 'initial' ? 'final' : 'initial';
    setTimeout(() => {
      this.router.navigate([JsonData]);
    }, 0);
  }

}
