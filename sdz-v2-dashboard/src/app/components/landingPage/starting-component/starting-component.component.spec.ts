import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StartingComponentComponent } from './starting-component.component';

describe('StartingComponentComponent', () => {
  let component: StartingComponentComponent;
  let fixture: ComponentFixture<StartingComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StartingComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StartingComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
