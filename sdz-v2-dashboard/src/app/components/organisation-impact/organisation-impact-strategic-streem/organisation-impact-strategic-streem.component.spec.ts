import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganisationImpactStrategicStreemComponent } from './organisation-impact-strategic-streem.component';

describe('OrganisationImpactStrategicStreemComponent', () => {
  let component: OrganisationImpactStrategicStreemComponent;
  let fixture: ComponentFixture<OrganisationImpactStrategicStreemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrganisationImpactStrategicStreemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganisationImpactStrategicStreemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
