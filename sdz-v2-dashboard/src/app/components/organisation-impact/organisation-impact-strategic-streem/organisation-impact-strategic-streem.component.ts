import {Component, OnInit} from '@angular/core';
import {until} from 'selenium-webdriver';
import {animate, keyframes, query, stagger, state, style, transition, trigger} from '@angular/animations';
import {Router} from '@angular/router';
import {D3Method, NamingMethod, ObjectOperation} from '../../../helpers/commonUtils';
import {HttpServiceService} from '../../../services/https-service/https.service';
import {FiltersService} from '../../../services/globalVariableService/filters.service';
import {filter} from 'rxjs/operators';

@Component({
  selector: 'app-organisation-impact-strategic-streem',
  templateUrl: './organisation-impact-strategic-streem.component.html',
  styleUrls: ['./organisation-impact-strategic-streem.component.scss'],
  animations: [
    trigger('listAnimation', [
      transition('* => *', [

        query(':enter', style({opacity: 0}), {optional: true}),

        query(':enter', stagger('100ms', [
          animate('1s ease-in', keyframes([
            style({opacity: 0, transform: 'translateY(-75%)', offset: 0}),
            style({opacity: .5, transform: 'translateY(35px)', offset: 0.3}),
            style({opacity: 1, transform: 'translateY(0)', offset: 1.0}),
          ]))]), {optional: true}),

        query(':leave', stagger('100ms', [
          animate('1s ease-in', keyframes([
            style({opacity: 1, transform: 'translateY(0)', offset: 0}),
            style({opacity: .5, transform: 'translateY(35px)', offset: 0.3}),
            style({opacity: 0, transform: 'translateY(-75%)', offset: 1.0}),
          ]))]), {optional: true})
      ])
    ]),
    trigger('fadeInOut', [
      state('void', style({
        opacity: 0, transform: 'translateX(-100px) translateY(0px)'
      })),
      transition('void <=> *', animate(800)),
    ])
  ]
})
export class OrganisationImpactStrategicStreemComponent implements OnInit {
  lineVisibility: boolean = true;
  suggestionDivShow: boolean;
  viewDroupDownDiv: boolean;
  subimpactDetailsData: any;
  public renderImpactJson: any = {};
  public setcapabilityGroups: any = {};
  public setparams: any = {};
  thredopportunityDiv: boolean;
  trendCategoryDiv: boolean;
  svgColor: any;
  horizantal: any = [];
  imapctPopUpDiv: boolean;
  timeLineYearIDImapct: any;
  horizantalCount: any;
  private filters: any = {};
  private filtersStratigic: any = {};

  categoryId: any;
  dummyArray: Array<any>;
  categoryGroup: any;
  categoryName: any;
  categoryCount:any;
  categoryGroupName: any;
  categorylistName: any;
  // tslint:disable-next-line:variable-name
  constructor(private router: Router, private _httpService: HttpServiceService, private filtersService: FiltersService) {
  }

  ngOnInit() {
    this.imapctPopUpDiv = false;
    this.thredopportunityDiv = false;
    this.trendCategoryDiv = false;
    this.subimpactDetailsData = JSON.parse(localStorage.getItem('OrganizationImpactDetail'));
    this.timeLineYearIDImapct = JSON.parse(localStorage.getItem('timeLineYearIDImapct'));


    if(this.subimpactDetailsData.groupName == "strategyGroup"){
      this.filtersStratigic = this.filtersService.getimpactOrganisationStrategicFilter();
      this.filters.expectedTimeId = JSON.parse(localStorage.getItem('timeLineYearIDImapct'));
      this.yearFilterSelected(this.filters.expectedTimeId);
    }else{
      this.filters = this.filtersService.getimpactOrganisationFilter();
      this.yearFilterSelected(this.filters.expectedTimeId);
      this.filters.expectedTimeId = JSON.parse(localStorage.getItem('timeLineYearIDImapct'));
    }

    // this.getCapabilityGroupSummary();
    this.suggestionDivShow = true;
    this.viewDroupDownDiv = false;
  if(this.categoryGroup){
    this.categoryGroup.map(d => {
      d.active = false;
    });
  }

  }

  getCapabilityGroupSummary() {
    if(this.subimpactDetailsData.groupName == "strategyGroup"){
      this._httpService.getStrategicGroupSummary(this.subimpactDetailsData.strategyGroupId, this.timeLineYearIDImapct).subscribe(res => {
        this.lineVisibility = false;
        const data: any = res;
      //  this.categoryGroup = data.strategies;
        this.categoryName = data.groupName;
        this.categoryCount = data.strategiesCount;
        this.svgColor = '#8F9ADD';
        this.categoryGroupName = 'Strategies';
        this.categorylistName= "Strategy"
        if (!Object.keys(this.renderImpactJson).length){
          this.renderImpactJson = data;
        } else {
          this.renderImpactJson.strategies.map((d, i) => {
            const dataElem = ObjectOperation.getArrayElemByAttributeValue('id', d.id, res.strategies);
            for (const k in d) {
              d[k] = dataElem[k];
            }
            d.active = false;
            return d;
          });
          this.renderImpactJson.overallImpactScore = res.overallImpactScore;
          this.renderImpactJson.totalTrends = res.totalTrends;
        }

        this.horizantalCount = Math.ceil(this.renderImpactJson.strategies.length / 3);
        if (this.horizantalCount === 0) {
          this.horizantalCount = 1;
        }

        if (this.renderImpactJson.strategies.length < 9) {
          this.dummyArray = new Array<any>();
          for (let i = 0; i < 9 - this.renderImpactJson.strategies.length; i++) {
            this.dummyArray.push('dummy');
          }
        } else if (this.renderImpactJson.strategies.length % 3 !== 0) {
          this.dummyArray = new Array<any>();
          for (let i = 0; i < 3 - Math.ceil(this.renderImpactJson.strategies.length % 3); i++) {
            this.dummyArray.push('dummy');
          }
        }

        const impactScoreRangeThreat = ObjectOperation.getMinMax(this.renderImpactJson.strategies, 'impactScore');
        const scaleThreat = D3Method.getLinearScale(impactScoreRangeThreat, [0.7, 1]);
        this.renderImpactJson.strategies.map((d) => {
          d.scale = scaleThreat(d.impactScore);
        });
        this.horizantal = [];
        for (let i = 0; i < this.horizantalCount; i++) {
          this.horizantal.push(0);
        }
        this.categoryGroup =this.renderImpactJson.strategies;
        setTimeout(() => {
          this.lineVisibility = true;
        }, 500);

      });

    } else {
      this._httpService.getCapabilityGroupSummary(this.subimpactDetailsData.capabilityGroupId, this.timeLineYearIDImapct).subscribe(res => {
        this.lineVisibility = false;
        const data: any = res;
     //   this.categoryGroup = data.capabilities;
        this.categoryName = data.groupName;
       this.categoryCount = data.capabilitiesCount;
        this.categoryGroupName = 'Capabilities';
        this.categorylistName= "Capability"
        this.svgColor = '#FEDB6E';
        if (!Object.keys(this.renderImpactJson).length) {
          this.renderImpactJson = data;
        } else {
          this.renderImpactJson.capabilities.map((d, i) => {
            const dataElem = ObjectOperation.getArrayElemByAttributeValue('id', d.id, res.capabilities);
            for (const k in d) {
              d[k] = dataElem[k];
            }
            d.active = false;
            return d;
          });
          this.renderImpactJson.overallImpactScore = res.overallImpactScore;
          this.renderImpactJson.totalTrends = res.totalTrends;
        }

        this.horizantalCount = Math.ceil(this.renderImpactJson.capabilities.length / 3);
        if (this.horizantalCount === 0) {
          this.horizantalCount = 1;
        }

        if (this.renderImpactJson.capabilities.length < 9) {
          this.dummyArray = new Array<any>();
          for (let i = 0; i < 9 - this.renderImpactJson.capabilities.length; i++) {
            this.dummyArray.push('dummy');
          }
        } else if (this.renderImpactJson.capabilities.length % 3 !== 0) {
          this.dummyArray = new Array<any>();
          for (let i = 0; i < 3 - Math.ceil(this.renderImpactJson.capabilities.length % 3); i++) {
            this.dummyArray.push('dummy');
          }
        }

        const impactScoreRangeThreat = ObjectOperation.getMinMax(this.renderImpactJson.capabilities, 'impactScore');
        const scaleThreat = D3Method.getLinearScale(impactScoreRangeThreat, [0.7, 1]);
        this.renderImpactJson.capabilities.map((d) => {
          d.scale = scaleThreat(d.impactScore);
        });
        this.horizantal = [];
        for (let i = 0; i < this.horizantalCount; i++) {
          this.horizantal.push(0);
        }
        this.categoryGroup =this.renderImpactJson.capabilities;
        setTimeout(() => {
          this.lineVisibility = true;
        }, 500);

      });
    }


  }

  yearFilterSelected(yearID: any) {
    localStorage.setItem('timeLineYearIDImapct', JSON.stringify(yearID));
    this.filters.expectedTimeId = yearID;
    this.filtersStratigic.expectedTimeId = yearID;
    this.timeLineYearIDImapct = yearID;
    this.getCapabilityGroupSummary();
    this.viewDroupDownDiv = false;
    this.suggestionDivShow = true;
  }

  viewDropDown(active: boolean, capGroup: any) {
    this.categoryGroup.map(d => {
      d.active = false;
    });
    capGroup.active = active;
    this.suggestionDivShow = !active;
    this.setcapabilityGroups.id = capGroup.id;
    this.setcapabilityGroups.timeLine = this.timeLineYearIDImapct;
    this.setcapabilityGroups.name =  this.categoryGroupName;
    localStorage.setItem('capabilityGroups', JSON.stringify(this.setcapabilityGroups));
    if (this.viewDroupDownDiv) {
      this.viewDroupDownDiv = false;
      setTimeout(() => {
        this.viewDroupDownDiv = active;
      }, 100);
    } else {
      this.viewDroupDownDiv = active;
    }
  }
  SetAPICall(data) {
    if (data.APIid === 1) {
      localStorage.setItem('trendIDForAPICall', JSON.stringify(data.id.id));
      localStorage.setItem('trendIDCapabilityForAPICall', JSON.stringify(data.id));
      this.imapctPopUpDiv = !this.imapctPopUpDiv;
      this.trendCategoryDiv = !this.trendCategoryDiv;
    } else if (data.APIid === 2) {
      this._httpService.getThreatByID(data.id).subscribe(res => {
        // tslint:disable-next-line:no-shadowed-variable
        // tslint:disable-next-line:no-shadowed-variable
        const data: any = res;
        this.setparams.isOpportunity = false;
        localStorage.setItem('ThreadOpportunitySubcategoryDetail', JSON.stringify(data));
        localStorage.setItem('ThreadOpportunityDetail', JSON.stringify(this.setparams));
        this.imapctPopUpDiv = !this.imapctPopUpDiv;
        this.thredopportunityDiv = !this.thredopportunityDiv;
      });
    } else if (data.APIid === 3) {
      this._httpService.getOpportunityByID(data.id).subscribe(res => {
        // tslint:disable-next-line:no-shadowed-variable
        // tslint:disable-next-line:no-shadowed-variable
        const data: any = res;
        this.setparams.isOpportunity = true;
        localStorage.setItem('ThreadOpportunitySubcategoryDetail', JSON.stringify(data));
        localStorage.setItem('ThreadOpportunityDetail', JSON.stringify(this.setparams));
        this.imapctPopUpDiv = !this.imapctPopUpDiv;
        this.thredopportunityDiv = !this.thredopportunityDiv;
      });
    }
  }

  countChangedHandler(count: number) {
    this.imapctPopUpDiv = !this.imapctPopUpDiv;
    this.trendCategoryDiv = !this.trendCategoryDiv;
  }

  countChangedHandlerthread(count: number) {
    this.imapctPopUpDiv = !this.imapctPopUpDiv;
    this.thredopportunityDiv = !this.thredopportunityDiv;
  }
}
