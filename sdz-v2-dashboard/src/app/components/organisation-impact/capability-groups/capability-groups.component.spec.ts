import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CapabilityGroupsComponent } from './capability-groups.component';

describe('CapabilityGroupsComponent', () => {
  let component: CapabilityGroupsComponent;
  let fixture: ComponentFixture<CapabilityGroupsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CapabilityGroupsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CapabilityGroupsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
