import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmptyCircleOrganizationImpactSVGComponent } from './empty-circle-organization-impact-svg.component';

describe('EmptyCircleOrganizationImpactSVGComponent', () => {
  let component: EmptyCircleOrganizationImpactSVGComponent;
  let fixture: ComponentFixture<EmptyCircleOrganizationImpactSVGComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmptyCircleOrganizationImpactSVGComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmptyCircleOrganizationImpactSVGComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
