import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import * as d3 from 'd3';

@Component({
  selector: 'app-empty-circle-organization-impact-svg',
  templateUrl: './empty-circle-organization-impact-svg.component.html',
  styleUrls: ['./empty-circle-organization-impact-svg.component.scss']
})
export class EmptyCircleOrganizationImpactSVGComponent implements OnInit {
  @Input() color: any = {};
  @Input() active: boolean;
  @Output() svgClickedEvent = new EventEmitter();
  @Input() svgScale: any = {};
  @Input() capabilityName: any;
  @Input() scaleImapct: any;
  @Input() categoryGroup: any;
  public localTemp: any = {};
  public nameSpilt: any = [];
  isActiveDiv: boolean;
  trendNameone: string;
  trendNametwo: string;
  trendNamethree: string;
  transformScale: string;
  isSplitWord: boolean;

  constructor() {
  }

  ngOnInit() {
    this.isActiveDiv = this.active;
    this.isSplitWord = true;
    const svg = d3.select('svg');
    // svg.on('click', function () {
    //   d3.select('.b').attr('fill', '#FEDB6E');
    //   d3.select('.c').attr('fill', '#FEDB6E');
    //   d3.select('.textCount').attr('fill', '#fff');
    // });
    // this.trendNameone = this.capabilityName;
    this.nameSpilt = this.capabilityName.split(' ');
    this.nameSpilt.join(' <br> ');
    while (this.isSplitWord) {
      if (this.nameSpilt[0].length < 14) {
        this.trendNameone = this.nameSpilt[0];
        if (this.trendNameone.length < 6 && this.nameSpilt[1].length < 7) {
          const tempText = this.trendNameone;
          this.trendNameone = tempText.concat(' ', this.nameSpilt[1]);
          if (this.trendNameone.length < 12 && this.trendNameone.concat(' ', this.nameSpilt[2]).length < 12) {
            this.trendNameone = this.trendNameone.concat(' ', this.nameSpilt[2]);
            if (this.nameSpilt[3].length < 12) {
              this.trendNametwo = this.nameSpilt[3];
              if (this.trendNametwo.length < 12 && this.trendNametwo.concat(' ', this.nameSpilt[4]).length < 12) {
                this.trendNametwo = this.trendNametwo.concat(' ', this.nameSpilt[4]);
                if (this.nameSpilt[5].length < 12) {
                  this.trendNamethree = this.nameSpilt[5];
                  if (this.trendNamethree.length < 12 && this.trendNamethree.concat(' ', this.nameSpilt[6]).length < 12) {
                    this.trendNamethree = this.trendNamethree.concat(' ', this.nameSpilt[6]);
                  } else {
                    this.trendNamethree = this.trendNamethree.concat(' ', '....');
                  }
                }
              } else {
                this.trendNamethree = this.nameSpilt[4];
              }
            }

          } else if (this.nameSpilt[2].length < 12) {
            this.trendNametwo = this.nameSpilt[3];

          }
        } else {
          this.trendNametwo = this.nameSpilt[1];
        }
      } else if ( this.nameSpilt[0].length >= 14 || this.nameSpilt[1].length >= 14) {
        this.trendNameone = (this.nameSpilt[0].length >= 14) ? this.nameSpilt[0].substring(0, 12) + '...' : this.nameSpilt[0];
        this.trendNametwo = (this.nameSpilt[1].length >= 14) ? this.nameSpilt[1].substring(0, 6) + '...' : this.nameSpilt[1];
      } 
      else {
        this.isSplitWord = false;
      }
      this.isSplitWord = false;
    }
    if (this.nameSpilt[2]) {
      if (this.nameSpilt[2].length > 0) {
        this.trendNametwo = this.trendNametwo.concat('   ....');
      }
    }
    if(this.trendNameone == undefined){
      this.trendNameone = this.capabilityName
    }
  }

  svgClicked(scaleImapct) {
    if(scaleImapct != 0){
      this.active = !this.active;
      this.svgClickedEvent.emit(this.active);
    }

  }

}
