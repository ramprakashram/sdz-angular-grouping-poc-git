import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganisationImpactCircleSVGComponent } from './organisation-impact-circle-svg.component';

describe('OrganisationImpactCircleSVGComponent', () => {
  let component: OrganisationImpactCircleSVGComponent;
  let fixture: ComponentFixture<OrganisationImpactCircleSVGComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrganisationImpactCircleSVGComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganisationImpactCircleSVGComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
