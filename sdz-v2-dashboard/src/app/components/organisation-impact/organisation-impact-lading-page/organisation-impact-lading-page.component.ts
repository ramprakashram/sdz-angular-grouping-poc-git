import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import { Router } from '@angular/router';
import {HttpServiceService} from '../../../services/https-service/https.service';
import {FiltersService} from '../../../services/globalVariableService/filters.service';
import {D3Method, ObjectOperation} from '../../../helpers/commonUtils';
import {filter} from 'rxjs/operators';

@Component({
  selector: 'app-organisation-impact-lading-page',
  templateUrl: './organisation-impact-lading-page.component.html',
  styleUrls: ['./organisation-impact-lading-page.component.scss']
})
export class OrganisationImpactLadingPageComponent implements OnInit {
  @ViewChild('widgetsContent', {read: ElementRef}) public widgetsContent: ElementRef<any>;
  private renderData: any = [];
  svgColor: string;
  private filters: any = {};
  parameter: boolean;
  parameterright: boolean;
  windowHeight: any;
  marginTop: any;
  screenHeight: any;
  timeLineImapctYearID: any;
  screenWidth: any;
  countClick: number;
  countLength: number;
  filtersForcapability : any;
  // tslint:disable-next-line:variable-name
  constructor(private router: Router, private _httpService: HttpServiceService, private filtersService: FiltersService) {
  }

  ngOnInit() {
    this.parameter = false;
    this.parameterright = true;
    this.countClick = 0;
    this.getScreenSize();
    this.windowHeight = JSON.parse(localStorage.getItem('windowOuterHeight'));
    if (this.windowHeight < 0.8) {
      this.marginTop = -100;
    }
    // this.windowHeight = 'scale(' + this.windowHeight + ')';
    //
    this.svgColor = '#8F9ADD';
    this.filtersForcapability = this.filtersService.getimpactOrganisationFilter();
    this.filtersForcapability.expectedTimeId = null;
    this.filters = this.filtersService.getimpactOrganisationStrategicFilter();

    this.yearFilterSelected(this.filters.expectedTimeId);
  }


  navigationLink(data: any) {
    data.groupName = "strategyGroup";
    localStorage.setItem('OrganizationImpactDetail', JSON.stringify(data));
    const URLName = 'strategic'
    localStorage.setItem('capabilityStrategicURLCheck', JSON.stringify(URLName));
    this.router.navigate(['/organize/impactStreams']);
  }

  yearFilterSelected(yearID: any) {
    localStorage.setItem('timeLineYearIDImapct', JSON.stringify(yearID));
    this.filters.expectedTimeId = yearID;
    this.timeLineImapctYearID = yearID;
    this._httpService.getStrategicStreamsummary(yearID).subscribe(res => {
      const impactScoreRangeThreat = ObjectOperation.getMinMax(res, 'strategyGroupImpactScore');
      const scaleThreat = D3Method.getLinearScale(impactScoreRangeThreat, [0.7, 1]);
      if (!this.renderData.length) {
        res.map((d) => {
          d.scale = scaleThreat(d.strategyGroupImpactScore);
          // tslint:disable-next-line:align max-line-length
        });
        this.renderData = res;
        this.countLength = this.renderData.length;
        this.countLength = this.countLength - 6;
      } else {
        // tslint:disable-next-line:only-arrow-functions

        res.map((d) => {
          d.scale = scaleThreat(d.strategyGroupImpactScore);
          // tslint:disable-next-line:align max-line-length
        });
        this.countLength = this.renderData.length;
        this.countLength = this.countLength - 6;
        this.renderData.map((d, i) => {
          const dataElem = ObjectOperation.getArrayElemByAttributeValue('strategyGroupName', d.strategyGroupName, res);
          // tslint:disable-next-line:forin
          for (const k in d) {
            d[k] = dataElem[k];
          }
          return d;
        });
      }
    });
  }

  getScreenSize(event?) {
    this.screenHeight = window.innerHeight;
    this.screenWidth = window.innerWidth;
    if (700 > this.screenHeight || this.screenHeight < 850) {
      this.windowHeight = 0.7;
      localStorage.setItem('windowOuterHeight', JSON.stringify(this.windowHeight));
    } else {
      this.windowHeight = 1;
      localStorage.setItem('windowOuterHeight', JSON.stringify(this.windowHeight));
    }
  }

  public scrollRight(): void {
    this.countClick--;
    this.widgetsContent.nativeElement.scrollTo({
      left: (this.widgetsContent.nativeElement.scrollLeft + 300),
      behavior: 'smooth'
    });
    if (this.countClick < 0) {
      this.parameter = true;
    } else {
      this.parameter = false;
    }
    if (-this.countLength < this.countClick) {
      this.parameterright = true;
    } else {
      this.parameterright = false;
    }
  }

  public scrollLeft(): void {
    this.widgetsContent.nativeElement.scrollTo({
      left: (this.widgetsContent.nativeElement.scrollLeft - 300),
      behavior: 'smooth'
    });
    this.countClick++;
    if (this.countClick < 0) {
      this.parameter = true;
    } else {
      this.parameter = false;
    }
    if (-this.countLength < this.countClick) {
      this.parameterright = true;
    } else {
      this.parameterright = false;
    }
  }
}
