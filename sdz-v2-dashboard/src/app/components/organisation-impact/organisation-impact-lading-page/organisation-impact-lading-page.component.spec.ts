import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganisationImpactLadingPageComponent } from './organisation-impact-lading-page.component';

describe('OrganisationImpactLadingPageComponent', () => {
  let component: OrganisationImpactLadingPageComponent;
  let fixture: ComponentFixture<OrganisationImpactLadingPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrganisationImpactLadingPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganisationImpactLadingPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
