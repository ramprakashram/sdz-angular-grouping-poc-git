import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrganisationImpactLadingPageComponent } from './organisation-impact-lading-page/organisation-impact-lading-page.component';
import { OrganisationImpactCircleSVGComponent } from './organisation-impact-circle-svg/organisation-impact-circle-svg.component';
// tslint:disable-next-line:max-line-length
import { OrganisationImpactStrategicStreemComponent } from './organisation-impact-strategic-streem/organisation-impact-strategic-streem.component';
// tslint:disable-next-line:max-line-length
import { OrganisationImpactDrowpDownListComponent } from './organisation-impact-drowp-down-list/organisation-impact-drowp-down-list.component';
import { CapabilityGroupsComponent } from './capability-groups/capability-groups.component';

const routes: Routes = [
  {
    path : '',
    component : OrganisationImpactLadingPageComponent },
   { path: 'circleSVGImage', component: OrganisationImpactCircleSVGComponent },
   { path: 'impactStreams', component: OrganisationImpactStrategicStreemComponent },
   { path: 'OrganisationImpactDrowpDownList', component: OrganisationImpactDrowpDownListComponent },
   { path: 'CapabilityGroupsComponentLink', component: CapabilityGroupsComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrganisationImpactRoutingModule { }
