import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganisationImpactDrowpDownListComponent } from './organisation-impact-drowp-down-list.component';

describe('OrganisationImpactDrowpDownListComponent', () => {
  let component: OrganisationImpactDrowpDownListComponent;
  let fixture: ComponentFixture<OrganisationImpactDrowpDownListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrganisationImpactDrowpDownListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganisationImpactDrowpDownListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
