import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {trigger, state, style, transition, animate} from '@angular/animations';
import {Router} from '@angular/router';
import {HttpServiceService} from '../../../services/https-service/https.service';
import {BadgeModel} from '@gravityilabs/sdz-models';
import {PlatformLocation} from '@angular/common';
@Component({
  selector: 'app-organisation-impact-drowp-down-list',
  templateUrl: './organisation-impact-drowp-down-list.component.html',
  styleUrls: ['./organisation-impact-drowp-down-list.component.scss'],
  animations: [
    trigger('expandableState', [
      transition(':enter', [
        style({transform: 'translateY(100%)', opacity: 0}),
        animate('500ms', style({transform: 'translateY(0)', opacity: 1})),
      ]),
      transition(':leave', [
        style({transform: 'translateY(0)', opacity: 1, display: 'none'}),
        animate('1000ms', style({transform: 'translateY(100%)', opacity: 0})),
      ]),
    ])
  ]
})
export class OrganisationImpactDrowpDownListComponent implements OnInit {
  linkedDiv: boolean;
  threadDiv: boolean;
  trendDiv: boolean;
  futureDiv: boolean;
  firstDivShow: boolean;
  secondtDivShow: boolean;
  thirdDivShow: boolean;
  fourthDiv: boolean;
  positionAbs: boolean;
  public tempJSON: any = {};
  listName: any;
  public capabilityGroupsJson: any = {};
  @Output() dropDownClickedEvent = new EventEmitter();
  public capabilityDataJson: any = {};
  isCollapsed: boolean = true;
  capabilityGroups : any;
  baseURL: any;
  isCapability: boolean;
  capabilitySuggestion: string;
  sliceObject: string;
  concatObject: string;
  isReadMore: boolean = false;
  normalObject : string;
  // tslint:disable-next-line:variable-name

  constructor(private router: Router, private _httpService: HttpServiceService,private platform: PlatformLocation) {
    const origin: string = (this.platform as any).location.origin;
    this.baseURL = origin;
  }

  ngOnInit() {
    this.capabilityGroupsJson = JSON.parse(localStorage.getItem('capabilityGroups'));
    this.capabilityGroups = JSON.parse(localStorage.getItem('OrganizationImpactDetail'));

    //getStrategyLinkedSummary
    if(this.capabilityGroupsJson.name == "Capabilities"){
      this._httpService.getCapabilityLinkedSummary(this.capabilityGroupsJson.id, this.capabilityGroupsJson.timeLine).subscribe(res => {
        const data: any = res;
        this.capabilityDataJson = data;
        this.listName = data.capabilityName;   
        this.normalObject = this.capabilityDataJson.capabilityDescription;
        if(this.capabilityDataJson.capabilityDescription){       
          if(this.capabilityDataJson.capabilityDescription.length > 250){
            this.isReadMore = true;
            this.capabilitySuggestion = this.capabilityDataJson.capabilityDescription;
            if(window.outerHeight < 750){
              this.sliceObject = this.capabilitySuggestion.slice( 0,180);
            } else{
              this.sliceObject = this.capabilitySuggestion.slice( 0,220);
            }

            this.concatObject =this.sliceObject.concat(' ', '....')
          } else{
            this.concatObject =this.capabilityDataJson.capabilityDescription;
          }
        }
    
        this.isCapability = true;
      });
    }else{
      this._httpService.getStrategyLinkedSummary(this.capabilityGroupsJson.id, this.capabilityGroupsJson.timeLine).subscribe(res => {
        const data: any = res;
        this.capabilityDataJson = data;
        this.listName = data.strategyName;
        this.isCapability = false;
      });
    }

    this.positionAbs = false;
    this.linkedDiv = false;
    this.futureDiv = false;
    this.trendDiv = false;
    this.threadDiv = false;
    this.firstDivShow = true;
    this.secondtDivShow = true;
    this.thirdDivShow = true;
    this.fourthDiv = true;
  }

  futureDivFun(linkedSuggestionsCount: any) {
    if (linkedSuggestionsCount) {
      this.positionAbs = !this.positionAbs;
      this.firstDivShow = this.secondtDivShow = this.thirdDivShow = this.futureDiv;
      this.futureDiv = !this.futureDiv;
    }

  }

  threadDivFun(linkedThreatsCount: any) {
    if (linkedThreatsCount) {
      this.positionAbs = !this.positionAbs;
      this.firstDivShow = this.fourthDiv = this.thirdDivShow = this.threadDiv;
      this.threadDiv = !this.threadDiv;
    }

  }

  trendDivDivFun(linkedOpportunitiesCount: any) {
    if (linkedOpportunitiesCount) {
      this.positionAbs = !this.positionAbs;
      this.firstDivShow = this.fourthDiv = this.secondtDivShow = this.trendDiv;
      this.trendDiv = !this.trendDiv;
    }

  }
  setMyStylesBackground() {
    let styles = {
      'height': window.outerHeight < 750 ? "90px" : "80px",
    };
    return styles;
  }

  linkedDivFun(linkedTrendsCount: any) {
    if (linkedTrendsCount) {
      this.positionAbs = !this.positionAbs;
      this.thirdDivShow = this.fourthDiv = this.secondtDivShow = this.linkedDiv;
      this.linkedDiv = !this.linkedDiv;
    }
  }

  getTrendData(data) {
    // @ts-ignore
    this.tempJSON.id = data;
    this.tempJSON.name = 'trend';
    this.tempJSON.APIid = 1;
    this.dropDownClickedEvent.emit(this.tempJSON);
  }

  getThreatData(data) {
    // @ts-ignore
    this.tempJSON.id = data.id;
    this.tempJSON.name = 'Threat';
    this.tempJSON.APIid = 2;
    this.dropDownClickedEvent.emit(this.tempJSON);
  }

  getOpportunityData(data) {
    // @ts-ignore
    this.tempJSON.id = data.id;
    this.tempJSON.name = 'Opportunity';
    this.tempJSON.APIid = 3;
    this.dropDownClickedEvent.emit(this.tempJSON);
  }

  getBatchData(data: any) {
    let name = '';
    if (data.analystFirstName) {
      name = data.analystFirstName;
    }
    if (data.analystLastName) {
      name += ' ' + data.analystLastName;
    }
    return (new BadgeModel(name, '#A85ACE'));
  }
  capabilityDetailPage(){
    location.href =  this.baseURL+"/execdashboard-v2/#capabilityDetail="+ this.capabilityGroupsJson.id;
   }
}


import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'orderByImpactScoreDescription',
  pure: false
})
export class OrderByImpactScoreDescriptionPipe implements PipeTransform {

  transform(array: any, field: string): any[] {
    if (!Array.isArray(array)) {
      return;
    }
    array.sort((a: any, b: any) => {
      if (parseInt(a[field].split('/')[0]) < parseInt(b[field].split('/')[0])) {
        return 1;
      } else if (parseInt(a[field].split('/')[0]) > parseInt(b[field].split('/')[0])) {
        return -1;
      } else {
        return 0;
      }
    });
    return array;
  }
 
}
