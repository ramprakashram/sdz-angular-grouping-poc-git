import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrganisationImpactRoutingModule } from './organisation-impact-routing.module';
import { OrganisationImpactLadingPageComponent } from './organisation-impact-lading-page/organisation-impact-lading-page.component';
import { OrganisationImpactCircleSVGComponent } from './organisation-impact-circle-svg/organisation-impact-circle-svg.component';
// tslint:disable-next-line:max-line-length
import { OrganisationImpactStrategicStreemComponent } from './organisation-impact-strategic-streem/organisation-impact-strategic-streem.component';
// tslint:disable-next-line:max-line-length
import { OrganisationImpactDrowpDownListComponent, OrderByImpactScoreDescriptionPipe } from './organisation-impact-drowp-down-list/organisation-impact-drowp-down-list.component';
// tslint:disable-next-line:max-line-length
import { EmptyCircleOrganizationImpactSVGComponent } from './empty-circle-organization-impact-svg/empty-circle-organization-impact-svg.component';
import { CapabilityGroupsComponent } from './capability-groups/capability-groups.component';
import { OperatingEnvironmentModule } from '../operating-environment/operating-environment.module';
import { ThreatsOpportunitiesModule } from '../threats-opportunities/threats-opportunities.module';
import { WidgetsModule } from '@gravityilabs/sdz-widgets';
@NgModule({
  declarations: [OrganisationImpactLadingPageComponent,
    OrganisationImpactCircleSVGComponent,
    OrganisationImpactStrategicStreemComponent,
    OrganisationImpactDrowpDownListComponent,
    EmptyCircleOrganizationImpactSVGComponent,
    CapabilityGroupsComponent,
    OrderByImpactScoreDescriptionPipe],
  imports: [
    CommonModule,
    OrganisationImpactRoutingModule,
    OperatingEnvironmentModule,
    ThreatsOpportunitiesModule,
    WidgetsModule
  ]
})
export class OrganisationImpactModule { }
