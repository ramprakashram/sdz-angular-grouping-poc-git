import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OperatingEnvironmentComponent } from './operating-environment.component';

describe('OperatingEnvironmentComponent', () => {
  let component: OperatingEnvironmentComponent;
  let fixture: ComponentFixture<OperatingEnvironmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OperatingEnvironmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperatingEnvironmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
