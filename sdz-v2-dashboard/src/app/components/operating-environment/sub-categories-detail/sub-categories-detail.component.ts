import {Component, OnInit, Input} from '@angular/core';
import {Router} from '@angular/router';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {FiltersService} from '../../../services/globalVariableService/filters.service';

// tslint:disable-next-line:import-spacing
@Component({
  selector: 'app-sub-categories-detail',
  templateUrl: './sub-categories-detail.component.html',
  styleUrls: ['./sub-categories-detail.component.scss'],
  animations: [
    trigger('fadeInOut', [
      state('void', style({
        opacity: 0, transform: 'translateX(-70px)'
      })),
      transition('void <=> *', animate(800)),
    ])
  ]
})
export class SubCategoriesDetailComponent implements OnInit {
  sumOfImapctScoreCal: number;
  timeLineImapctYearID: any;
  filters: any = {};
  public subCategoryDetailsData: any = {};

  // tslint:disable-next-line:variable-name
  constructor(private router: Router, private filtersService: FiltersService) {
    this.subCategoryDetailsData = JSON.parse(localStorage.getItem('SubCategoriesDetail'));
    this.filters = this.filtersService.getOperatingEnvironmentFilters();
    this.subCategoryDetailsData.checkOperatingEnvironmentModule = 2;
    this.sumOfImapctScoreCal = this.subCategoryDetailsData.totalImpactScore;
  }

  ngOnInit() {
  }

  linkNavigation(data) {
    if (data.trendSummary.length > 0) {
      localStorage.setItem('TrendCategorData', JSON.stringify(data));
      this.router.navigate(['/operative/trend']);
    }

  }

}
