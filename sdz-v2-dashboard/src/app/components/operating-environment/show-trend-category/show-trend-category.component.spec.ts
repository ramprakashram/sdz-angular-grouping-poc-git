import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { trendCategoryComponent } from './show-trend-category.component';

describe('trendCategoryComponent', () => {
  let component: trendCategoryComponent;
  let fixture: ComponentFixture<trendCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ trendCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(trendCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
