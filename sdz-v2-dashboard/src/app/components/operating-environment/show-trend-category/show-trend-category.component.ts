import {Component, OnInit, ElementRef, ViewChild} from '@angular/core';
import {trigger, state, style, animate, transition} from '@angular/animations';

@Component({
  selector: 'app-show-trend-category',
  templateUrl: './show-trend-category.component.html',
  styleUrls: ['./show-trend-category.component.scss'],
  animations: [
    trigger('EnterLeave', [
      state('flyIn', style({transform: 'translateX(0)'})),
      transition(':enter', [
        style({transform: 'translateX(-100%)'}),
        animate('0.6s 300ms ease-in')
      ]),
      transition(':leave', [
        animate('0.5s ease-out', style({transform: 'translateX(100%)'}))
      ])
    ])
  ]
})
// tslint:disable-next-line:class-name
export class trendCategoryComponent implements OnInit {
  @ViewChild('detailsView') searchElement: ElementRef;
  groupOfHexagonal: boolean;
  threadDetailsDiv: boolean;
  subCategory: any;
  subCategorysummaryData: any;
  subCategoryimpactScoreData: any;
  subCategoryName: any;
  categoryName: any;
  subCategorycategoryName: any;

  constructor() {

  }

  ngOnInit() {
    this.groupOfHexagonal = false;
    this.threadDetailsDiv = true;
    this.subCategory = JSON.parse(localStorage.getItem('TrendCategorData'));
    this.subCategorycategoryName = JSON.parse(localStorage.getItem('SubCategoriesDetail'));
    this.subCategorysummaryData = this.subCategory.categoryDescription;
    this.subCategoryimpactScoreData = this.subCategory.totalImpactScore;
    this.subCategoryName = this.subCategory.category;
    this.categoryName = this.subCategorycategoryName.category;
  }

  countChangedHandler(count: number) {
    this.groupOfHexagonal = !this.groupOfHexagonal;
    this.threadDetailsDiv = !this.threadDetailsDiv;
  }
}
