import {Component, OnInit, Input} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-hexagond3',
  templateUrl: './hexagond3.component.html',
  styleUrls: ['./hexagond3.component.scss']
})
export class Hexagond3Component implements OnInit {
  @Input() hexaData: any;
  @Input() svgWidth: number;
  @Input() svgScale: string;
  @Input() svgScore: string;
  @Input() svgScaleNum: number;

  private svgViewBox: string;

  public respones: any = {};
  public trednName: any = {};
  public trednCount: any = {};
  public impactCount: any = {};
  public hexaOperatingEnvironmentModule = false;
  public hexaUbdetails = false;

  constructor(private router: Router) {
  }

  ngOnInit() {
    const topValue = (this.svgScaleNum < 1 ? this.svgScaleNum * 2 : this.svgScaleNum);
    this.svgViewBox = '0 -' + topValue + ' 320 ' + this.svgWidth;
    // this.svgViewBox = '10 -100 320 ' + this.svgWidth;
    this.respones = this.hexaData.checkOperatingEnvironmentModule;
    if (this.respones === 2) {
      this.hexaOperatingEnvironmentModule = true;
      this.trednName = this.hexaData.category;
      this.trednCount = this.hexaData.totalImpactScore;
      this.impactCount = this.hexaData.trendCount;
    } else {
      this.hexaUbdetails = true;
      this.trednName = this.hexaData.category;
      this.trednCount = this.svgScore;
    }
    // this.draw();
  }

//   draw() {
//     if (this.localTemp.name === '1') {
//       this.objHexaJson = Object.assign(this.startHexaJson);
//     } else {
//       this.objHexaJson = Object.assign(this.trendHexaJson);
//     }
//     const chart = d3.select('.hexagonalSVG');
//     chart.attr('viewBox', this.objHexaJson.viewBox);
//     chart.attr('width', this.objHexaJson.width);
//     chart.attr('height', this.objHexaJson.height);
//     if (this.localTemp.name === '1') {
//       chart.append('line')
//       .attr('class', 'lineStating')
//       .style('stroke', 'black')
//       .attr('x1', 130)
//       .attr('x2', 130)
//       .attr('y1', 30)
//       .attr('y2', -60);
//       chart.append('line')
//       .attr('class', 'lineEnding')
//       .style('stroke', 'black')
//       .attr('x1', 130)
//       .attr('x2', 200)
//       .attr('y1', -60)
//       .attr('y2', -60);
//       chart.append('text')
//       .attr('class', 'lineText')
//       .text('361')
//       .attr('y', -65)
//       .attr('x', 150)
//       .attr('font-size', 28)
//       .attr('font-family', 'SegoeUI-SemiBold')
//       .attr('fill', '#008AD1');
//       chart.append('text')
//       .text('TECHNOLOGY')
//       .attr('y', 140)
//       .attr('x', 80)
//       .attr('font-size', 16)
//       .attr('font-family', 'SegoeUI-SemiBold')
//       .attr('fill', '#4C5E7A');
//     } else {
//       chart.append('text')
//       .text('TECHNOLOGY')
//       .attr('y', 120)
//       .attr('x', 80)
//       .attr('font-size', 16)
//       .attr('font-family', 'SegoeUI-SemiBold')
//       .attr('fill', '#4C5E7A');
//       chart.append('text')
//       .text('Overall Impact')
//       .attr('y', 140)
//       .attr('x', 90)
//       .attr('font-size', 12)
//       .attr('font-family', 'SegoeUI-SemiBold')
//       .attr('fill', '#78A6EF');
//       chart.append('text')
//       .text('Score 361')
//       .attr('y', 155)
//       .attr('x', 100)
//       .attr('font-size', 12)
//       .attr('font-family', 'SegoeUI-SemiBold')
//       .attr('fill', '#78A6EF');
//       chart.append('text')
//       .text('79 trends')
//       .attr('y', 170)
//       .attr('x', 100)
//       .attr('font-size', 12)
//       .attr('font-family', 'SegoeUI-Italian')
//       .attr('fill', '#4C5E7A');
//     }
//   }
//   navigationLink() {
//     const chart2 = d3.select('.hexagonalSVG');
//     d3.selectAll('.lineStating').remove();
//     d3.selectAll('.lineEnding').remove();
//     d3.selectAll('.lineText').remove();
//     chart2.transition()
//     .duration(1000)
//     .attr('viewBox', '12 24 430 450')
//     .attr('width', '800')
//     .attr('height', '800');
//     setTimeout(() => {
//       this.router.navigate(['/OperatingEnvironmentModule/SubCategoriesDetail']);
//  }, 1000);
//   }
}


// chart.on('click', function() {
//   this.router.navigate(['/OperatingEnvironmentModule/SubCategoriesDetail']);
//       });

// [routerLink]="['/OperatingEnvironmentModule/SubCategoriesDetail']"
// svg.on("click", function () {
//   var mouse = d3.mouse(this);
//   svg
//       .append("use")
//       .attr("href", "#pointer")
//       .attr("x", mouse[0])
//       .attr("y", mouse[1])
//       .attr("fill", "#039BE5")
//       .attr("stroke", "#039BE5")
//       .attr("stroke-width", "1px");
// });
// d3.select('#saveButton').on('click', function(){
// 	var svgString = getSVGString(svg.node());
// 	svgString2Image( svgString, 2*width, 2*height, 'png', save ); // passes Blob and filesize String to the callback

// 	function save( dataBlob, filesize ){
// 		saveAs( dataBlob, 'D3 vis exported to PNG.png' ); // FileSaver.js function
// 	}
// });
