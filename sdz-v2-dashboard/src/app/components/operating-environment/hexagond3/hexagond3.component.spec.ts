import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Hexagond3Component } from './hexagond3.component';

describe('Hexagond3Component', () => {
  let component: Hexagond3Component;
  let fixture: ComponentFixture<Hexagond3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Hexagond3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Hexagond3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
