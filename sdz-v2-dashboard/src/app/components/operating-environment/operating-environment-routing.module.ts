import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OperatingEnvironmentComponent } from './operating-environment.component';
import { Hexagond3Component } from './hexagond3/hexagond3.component';
import { SubCategoriesDetailComponent } from './sub-categories-detail/sub-categories-detail.component';
import { trendCategoryComponent } from './show-trend-category/show-trend-category.component';
import { TrendCategoryComponent } from './trend-category/trend-category.component';
import { TrendsComponent } from './trends/trends.component';
import { TrendhexagonalComponent } from './trendhexagonal/trendhexagonal.component';
import { TrendCategoryDetailsComponent } from './trend-category-details/trend-category-details.component';
const routes: Routes = [
  {
    path : '',
    component : OperatingEnvironmentComponent, children: [
      { path: 'Hexagond3Component', component: Hexagond3Component },
    ]
  },
  { path: 'subCategory', component: SubCategoriesDetailComponent },
  { path: 'trend', component: trendCategoryComponent },
  { path: 'TrendCategory', component: TrendCategoryComponent },
  { path: 'Trendshexagonal', component: TrendsComponent },
  { path: 'TrendhexagonalComponent', component: TrendhexagonalComponent },
  { path: 'TrendhexagonalDetails', component: TrendCategoryDetailsComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OperatingEnvironmentRoutingModule { }
