import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {OperatingEnvironmentRoutingModule} from './operating-environment-routing.module';
import {Hexagond3Component} from './hexagond3/hexagond3.component';
import {OperatingEnvironmentComponent} from './operating-environment.component';
import {SubCategoriesDetailComponent} from './sub-categories-detail/sub-categories-detail.component';
import {trendCategoryComponent} from './show-trend-category/show-trend-category.component';
import {TrendCategoryComponent} from './trend-category/trend-category.component';
import {TrendCategoryDetailsComponent} from './trend-category-details/trend-category-details.component';
import {TrendsComponent} from './trends/trends.component';
import {TrendGraphsComponent} from './trend-graphs/trend-graphs.component';
import {TrendhexagonalComponent} from './trendhexagonal/trendhexagonal.component';
import {TimelineImpactscorfilterComponent} from '../../shared/timeline-impactscorfilter/timeline-impactscorfilter.component';

import {LanguageStringPipe} from '../../pipes/language-string.pipe';
import {OrderByPipe} from '../../pipes/order-by.pipe';

import {TransitionGroupComponent, TransitionGroupItemDirective} from '../../shared/animations/animationGroup';

@NgModule({
  declarations: [
    Hexagond3Component,
    OperatingEnvironmentComponent,
    SubCategoriesDetailComponent,
    trendCategoryComponent,
    TrendCategoryComponent,
    TrendCategoryDetailsComponent,
    TrendsComponent,
    TrendGraphsComponent,
    TrendhexagonalComponent,
    TimelineImpactscorfilterComponent,
    LanguageStringPipe,
    OrderByPipe,
    TransitionGroupComponent,
    TransitionGroupItemDirective
  ],
  imports: [
    CommonModule,
    OperatingEnvironmentRoutingModule,
  ],
  exports: [LanguageStringPipe, TrendCategoryDetailsComponent, TimelineImpactscorfilterComponent, OrderByPipe, TransitionGroupComponent, TransitionGroupItemDirective]
})
export class OperatingEnvironmentModule {
}
