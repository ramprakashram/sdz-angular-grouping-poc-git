import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrendCategoryComponent } from './trend-category.component';

describe('TrendCategoryComponent', () => {
  let component: TrendCategoryComponent;
  let fixture: ComponentFixture<TrendCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrendCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrendCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
