import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter, Input } from '@angular/core';
import {
  animate,
  keyframes,
  query,
  stagger, state,
  style,
  transition,
  trigger} from '@angular/animations';
import * as d3 from 'd3';
import {FiltersService} from '../../../services/globalVariableService/filters.service';

@Component({
  selector: 'app-trend-category',
  templateUrl: './trend-category.component.html',
  styleUrls: ['./trend-category.component.scss'],
  animations: [
    trigger('listAnimation', [
      transition('* => *', [

        query(':enter', style({ opacity: 0 }), { optional: true }),

        query(':enter', stagger('300ms', [
          animate('1s ease-in', keyframes([
            style({ opacity: 0, transform: 'translateY(-75%)', offset: 0 }),
            style({ opacity: .5, transform: 'translateY(35px)', offset: 0.3 }),
            style({ opacity: 1, transform: 'translateY(0)', offset: 1.0 }),
          ]))]), { optional: true }),

        query(':leave', stagger('300ms', [
          animate('1s ease-in', keyframes([
            style({ opacity: 1, transform: 'translateY(0)', offset: 0 }),
            style({ opacity: .5, transform: 'translateY(35px)', offset: 0.3 }),
            style({ opacity: 0, transform: 'translateY(-75%)', offset: 1.0 }),
          ]))]), { optional: true })
      ])
    ]),
    trigger('fadeInOut', [
      state('void', style({
        opacity: 0, transform: 'translateX(-100px) translateY(0px)'
      })),
      transition('void <=> *', animate(800)),
    ])
  ]
})
export class TrendCategoryComponent implements OnInit {
  @ViewChild('detailsView') searchElement: ElementRef;
  @ViewChild('detailsViewEventHexagonal') searchElementEventHexagonal: ElementRef;
  @Output() countChanged: EventEmitter<number> =   new EventEmitter();
  @Input() temp: any = {} ;
  @ViewChild('detailsViewSecond') searchElementSecond: ElementRef;
  @Output() countChangedSecond: EventEmitter<number> =   new EventEmitter();
  @Input() tempSecond: any = {} ;
  show: boolean;
  public TrendCategorData: any = {};
  public TrendCategorDataCount: any = {};
  public HexagonalData: any = {};
  public hexDetailstemp1: any = [];
  public hexDetailstemp2: any = [];
  public hexDetailstemp3: any = [];
  public tempJSON: any = [];
  public tempJSONScale: any = [];
  public tempJSONOne: any = [];
  public tempJSONTwo: any = [];
  public tempJSONThree: any = [];
  timeLineImapctYearID: any;
  filters: any = {};
  public hexaTrendDetails = false;
  showDiv: number;
  // tslint:disable-next-line:variable-name
  constructor(private filtersService: FiltersService ) {

  }

ngOnInit() {
  // this.timeLineImapctYearID = JSON.parse(localStorage.getItem('timeLineYearID'));
  this.filters = this.filtersService.getOperatingEnvironmentFilters();
  this.TrendCategorData = JSON.parse(localStorage.getItem('TrendCategorData'));
  this.TrendCategorDataCount = JSON.parse(localStorage.getItem('TrendCategorData'));
  this.tempJSON = (this.TrendCategorData.trendSummary.sort((a, b) => parseFloat(a.impactScore) - parseFloat(b.impactScore)));
  this.tempJSONScale =  JSON.parse(JSON.stringify(this.tempJSON));
  // tslint:disable-next-line:max-line-length
  while (this.tempJSON.length > 0) {
           if (this.tempJSON.length > 0) {
          this.tempJSONOne =  this.tempJSON.splice(this.tempJSON.length - 1, 1);
          const scale = this.countingScale(this.tempJSONOne[0].impactScore);
          this.tempJSONOne[0].scale = scale(this.tempJSONOne[0].impactScore);
          const temp =  this.tempJSONOne;
          this.hexDetailstemp1 = this.hexDetailstemp1.concat(temp);
         }
           if (this.tempJSON.length > 0) {
            this.tempJSONTwo =  this.tempJSON.splice(this.tempJSON.length - 1, 1);
            const scale = this.countingScale(this.tempJSONTwo[0].impactScore);
            this.tempJSONTwo[0].scale = scale(this.tempJSONTwo[0].impactScore);
            const temp1 = this.tempJSONTwo;
            this.hexDetailstemp2 = this.hexDetailstemp2.concat(temp1);
         }
           if (this.tempJSON.length > 0) {
            this.tempJSONThree =  this.tempJSON.splice(this.tempJSON.length - 1, 1);
            const scale = this.countingScale(this.tempJSONThree[0].impactScore);
            this.tempJSONThree[0].scale = scale(this.tempJSONThree[0].impactScore);
            const temp2 = this.tempJSONThree;
            this.hexDetailstemp3 = this.hexDetailstemp3.concat(temp2);
         }
    }
  for (let i = 0; i < 2; i++) {
    this.hexDetailstemp1.push(0);
    this.hexDetailstemp2.push(0);
    this.hexDetailstemp3.push(0);
  }
  }
showTrendHexaDetails(event, data) {
   event.stopPropagation();
   if (data.impactScore) {
     this.HexagonalData = data;
     this.hexaTrendDetails = true;
   }
  }
  countingScale(data) {
    return d3.scaleLinear()
    .domain([this.tempJSONScale[0].impactScore, this.tempJSONScale[this.tempJSONScale.length - 1].impactScore])
    .range([0.7, 1]);
  }

countChangedHandlerSecond(count: number) {
    this.showDiv = 2;
    this.countChanged.emit(this.showDiv);
  }

  EventHexagonalfun(data) {
  }
  //  yearFilterSelected(yearID) {
  // // tslint:disable-next-line:only-arrow-functions
  //   this.hexDetailstemp1.map( function(d) {
  //     d.scale = Math.random();
  //     return d;
  //   });
  //   // tslint:disable-next-line:only-arrow-functions
  //   this.hexDetailstemp2.map( function(d) {
  //     d.scale = Math.random();
  //     return d;
  //   });
  //   // tslint:disable-next-line:only-arrow-functions
  //   this.hexDetailstemp3.map( function(d) {
  //     d.scale = Math.random();
  //     return d;
  //   });
  // }
}
