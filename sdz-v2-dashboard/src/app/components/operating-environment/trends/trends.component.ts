import { Component, OnInit, Input, ViewChild, Output, EventEmitter, ElementRef } from '@angular/core';
import * as d3 from 'd3';
@Component({
  selector: 'app-trends',
  templateUrl: './trends.component.html',
  styleUrls: ['./trends.component.scss']
})
export class TrendsComponent implements OnInit {
  @ViewChild('detailsViewSecond') searchElementSecond: ElementRef;
  @Output() EventHexagonal: EventEmitter<number> =   new EventEmitter();
  @Input() temp: string ;
  @Input() svgScale: any ;
  public localTemp: any = {};
  public nameSpilt: any = [];
  trendNametemp: string;
  trendNameone: string;
  trendNametwo: string;
  trendNamethree: string;
  transformScale: string;
  isSplitWord: boolean;
    constructor() { }
    ngOnInit() {
      this.isSplitWord = true;
      this.localTemp = this.temp;
      this.trendNametemp = this.localTemp.trendName;
      this.transformScale = 'scale(' + this.svgScale + ')';
      this.trendNameone = this.localTemp.trendName;
      this.nameSpilt = this.trendNametemp.split(' ');
      this.nameSpilt.join(' <br> ');
      while (this.isSplitWord) {
        if (this.nameSpilt[0].length < 14) {
          this.trendNameone =   this.nameSpilt[0];
          if (this.trendNameone && this.trendNameone.length < 6 && this.nameSpilt && this.nameSpilt.length > 0 && this.nameSpilt[1] && this.nameSpilt[1].length < 7 ) {
            const tempText = this.trendNameone;
            this.trendNameone = tempText.concat(' ', this.nameSpilt[1]);
            if (this.trendNameone.length < 12 && this.trendNameone.concat(' ', this.nameSpilt[2]).length < 12) {
              this.trendNameone =   this.trendNameone.concat(' ', this.nameSpilt[2]);
              if (this.nameSpilt[3].length < 12) {
                this.trendNametwo =   this.nameSpilt[3];
                if ( this.trendNametwo.length < 12 && this.trendNametwo.concat(' ', this.nameSpilt[4]).length < 12) {
                  this.trendNametwo =   this.trendNametwo.concat(' ', this.nameSpilt[4]);
                  if (this.nameSpilt[5].length < 12) {
                    this.trendNamethree =   this.nameSpilt[5];
                    if ( this.trendNamethree.length < 12 && this.trendNamethree.concat(' ', this.nameSpilt[6]).length < 12) {
                      this.trendNamethree =   this.trendNamethree.concat(' ', this.nameSpilt[6]);
                    } else {
                      this.trendNamethree =   this.trendNamethree.concat(' ', '....');
                    }
                  }
                } else {
                  this.trendNamethree =   this.nameSpilt[4];
                }
              }

            } else if (this.nameSpilt[2].length < 12) {
              this.trendNametwo =   this.nameSpilt[3];
            }
          } else {
            this.trendNametwo = this.nameSpilt[1];
          }
        } else {
          this.isSplitWord = false;
        }
        this.isSplitWord = false;
      }
      if (this.nameSpilt[2]) {
        if (this.nameSpilt[2].length > 0 && this.trendNametwo) {
          this.trendNametwo = this.trendNametwo.concat('   ....');
        }
      }
      this.draw();
     // this.svg_textMultiline();
    }


  //  svg_textMultiline() {
  //
  //    const x = 0;
  //    const y = 20;
  //    const width = 260;
  //    const lineHeight = 10;
  //
  //
  //
  //   /* get the text */
  //    const element = document.getElementById('test');
  //    const text = "this techomloy trend word so tha this techomloy trend word so tha";
  //
  //   /* split the words into array */
  //    const words = text.split(' ');
  //    const line = '';
  //
  //   /* Make a tspan for testing */
  //   element.innerHTML = '<tspan id="PROCESSING">busy</tspan >';
  //
  //   for (const n = 0; n < words.length; n++) {
  //     const testLine = line + words[n] + ' ';
  //     const testElem = document.getElementById('PROCESSING');
  //     /*  Add line in testElement */
  //     testElem.innerHTML = testLine;
  //     /* Messure textElement */
  //     const metrics = testElem.getBoundingClientRect();
  //     const testWidth = metrics.width;
  //
  //     if (testWidth > width && n > 0) {
  //       element.innerHTML += '<tspan x="0" dy="' + y + '">' + line + '</tspan>';
  //       line = words[n] + ' ';
  //     } else {
  //       line = testLine;
  //     }
  //   }
  //   element.innerHTML += '<tspan x="0" dy="' + y + '">' + line + '</tspan>';
  //   document.getElementById('PROCESSING').remove();
  //
  // }

  draw() {
      // const chart = d3.select('.hexagonalSVG1');
      // d3.select('path.a').remove();
      // d3.select('path.b').remove();
      // d3.select('path.c').remove();
      // // tslint:disable-next-line:only-arrow-functions
      // d3.select('path.d').style('stroke', function(d) { return '#5484D1'; })
      // // tslint:disable-next-line:only-arrow-functions
      // .style('stroke-width', function(d) { return '2.5'; } );
      // chart.attr('viewBox', '0 0 300 200');
      // chart.attr('width', 250);
      // chart.attr('height', 300);
      // chart.append('text')
      // .text('Increasing cyber')
      // .attr('y', 130)
      // .attr('x', 80)
      // .attr('font-size', 14)
      // .attr('font-family', 'SegoeUI-SemiBold')
      // .attr('fill', '#4C5E7A');
      // chart.append('text')
      // .text('threats')
      // .attr('y', 150)
      // .attr('x', 100)
      // .attr('font-size', 14)
      // .attr('font-family', 'SegoeUI-SemiBold')
      // .attr('fill', '#4C5E7A');
    }
    navigationGraph() {
      this.EventHexagonal.emit(this.localTemp);
    }
  }
