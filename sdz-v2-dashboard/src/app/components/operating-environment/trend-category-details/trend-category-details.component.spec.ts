import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrendCategoryDetailsComponent } from './trend-category-details.component';

describe('TrendCategoryDetailsComponent', () => {
  let component: TrendCategoryDetailsComponent;
  let fixture: ComponentFixture<TrendCategoryDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrendCategoryDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrendCategoryDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
