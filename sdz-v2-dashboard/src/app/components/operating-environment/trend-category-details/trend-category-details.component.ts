import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter, Input } from '@angular/core';
import { Router } from '@angular/router';
import { HttpServiceService } from '../../../services/https-service/https.service';
import { NamingMethod } from '../../../helpers/commonUtils';
import { ChartDataPreparationService } from '../../../services/globalVariableService/chart-data-preparation.service';
import { ThreatChartData } from '../../../models/filter-model';
import { FiltersService } from '../../../services/globalVariableService/filters.service';
import { SelectedFiltersModel } from '../../../shared/filter/filter.component';
import { OrderByPipe } from 'src/app/pipes/order-by.pipe';

@Component({
  selector: 'app-trend-category-details',
  templateUrl: './trend-category-details.component.html',
  styleUrls: ['./trend-category-details.component.scss']
})
export class TrendCategoryDetailsComponent implements OnInit {
  @ViewChild('detailsView') searchElement: ElementRef;
  @Output() countChanged: EventEmitter<number> = new EventEmitter();
  @Input() temp: any = {};
  public TrendID: any;
  public strategiesCount: any;
  public capabilityCount: any;
  public trendRenderData: any = {};
  showDiv: number;
  public ImpactJson: any = [];
  public threadOpportunty: any = [];
  subCategoryData: any;
  categoryName: any;
  subCategoryDetail: any;
  sourceName: string;
  timeID: any;
  capabilityID: any;
  private showChart = false;
  capabilityGroupsID: any;
  private chartData: ThreatChartData[];
  filters = {};

  // tslint:disable-next-line:variable-name
  constructor(private router: Router, private _httpService: HttpServiceService, private chartService: ChartDataPreparationService, private filtersService: FiltersService) {
    this.subCategoryData = JSON.parse(localStorage.getItem('ShowTrendCategorData'));
    this.subCategoryDetail = JSON.parse(localStorage.getItem('SubCategoriesDetail'));
    filtersService.currentFilter.subscribe((currentFilter) => {
      if (currentFilter) {
        this.filters = currentFilter;
      }
    });
  }

  ngOnInit() {

    this.TrendID = JSON.parse(localStorage.getItem('trendIDForAPICall'));
    this.timeID = JSON.parse(localStorage.getItem('trendIDForTimeIDAPICall'));
    this.capabilityGroupsID = JSON.parse(localStorage.getItem('capabilityGroups'));
    if (this.capabilityGroupsID === null) {
      this.capabilityID = null;
    } else {
      this.capabilityID = this.capabilityGroupsID.id;
    }
    // this._httpService.getTrendImpactedSummary(this.TrendID, this.timeID = this.timeID , this.capabilityID, null).subscribe(res => {
    this._httpService.getTrendImpactedSummaryBasedOnFilters(this.TrendID, this.filters).subscribe((res) => {
      const data: any = res;
      this.trendRenderData = data;
      this.ImpactJson = this.trendRenderData.trendSources;
      this.strategiesCount = this.trendRenderData.strategies.length;
      this.capabilityCount = this.trendRenderData.capabilities.length;
      let alphabetCount = 0;
      // this.trendRenderData.threatSummaries = this.sortThreatOpportunities(this.trendRenderData.threatSummaries);
      // this.trendRenderData.opportunitySummaries = this.sortThreatOpportunities(this.trendRenderData.opportunitySummaries);
      this.trendRenderData.opportunitySummaries.forEach(obj => {
        // obj.shortName = NamingMethod.alphabetBullets(alphabetCount);
        obj.categoryColor = '#78f088';
        obj.category = 'opportunity';
        this.threadOpportunty.push(obj);
        alphabetCount++;
      });
      this.trendRenderData.threatSummaries.forEach(obj => {
        // obj.shortName = NamingMethod.alphabetBullets(alphabetCount);
        obj.categoryColor = '#f07878';
        obj.category = 'threat';
        this.threadOpportunty.push(obj);
        alphabetCount++;
      });
      const sortedThreatOpp = this.sortThreatOpportunities(this.threadOpportunty);
      // this.chartService.getChartData(x, this.trendRenderData);
      // this.chartData = this.chartService.getThreatOpportunityChartData(this.trendRenderData);
      this.chartData = this.chartService.getChartData(sortedThreatOpp, this.trendRenderData);
      this.showChart = true;
      this.sourceName = this.trendRenderData.trendSources.length > 0 ? this.trendRenderData.trendSources[0].linkName : ' ';
    });
  }

  sortThreatOpportunities(data: Array<any>) {
    return OrderByPipe.prototype.transform(data, 'impactScore');
  }

  getShortName(index: number) {
    return NamingMethod.alphabetBullets(index);
  }

  navigationLink() {
    this.showDiv = 1;
    this.countChanged.emit(this.showDiv);
  }

  externalURLFun(data) {
    //  window.open(data.url, '_self');
    window.open(data.url)
  }

}
