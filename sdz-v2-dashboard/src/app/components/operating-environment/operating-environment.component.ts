import {Component, OnInit} from '@angular/core';
import {HttpServiceService} from '../../services/https-service/https.service';
import {Router} from '@angular/router';
import {ObjectOperation, D3Method} from '../../helpers/commonUtils';
import {FiltersService} from '../../services/globalVariableService/filters.service';
import {filter} from 'rxjs/operators';

@Component({
  selector: 'app-operating-environment',
  templateUrl: './operating-environment.component.html',
  styleUrls: ['./operating-environment.component.scss']
})

export class OperatingEnvironmentComponent implements OnInit {
  private operatingEnvironmentData: any = [];
  private filters: any = {};
  private categoryCardWith = ((window.innerWidth - 25) / 8);
  private impactScoreScale: any;
  private categoryLabelVisibility = false;
  timelineFilter: any = {};

  constructor(private httpService: HttpServiceService, private router: Router, private filtersService: FiltersService) {
    filtersService.currentFilter.subscribe((currentFilter) => {
      if (currentFilter) {
        this.filters = currentFilter;
        this.timelineFilter = this.filtersService.getOperatingEnvironmentFilters();
        this.getOperatingEnvironmentData();
      }
    });
  }

  ngOnInit() {
    this.timelineFilter = this.filtersService.getOperatingEnvironmentFilters();
    // this.getOperatingEnvironmentData();
  }

  getOperatingEnvironmentData() {
    this.filters.expectedTimeId = this.timelineFilter.expectedTimeId ? this.timelineFilter.expectedTimeId : null;
    // this.timelineFilter.expectedTimeId = this.filters.expectedTimeId;
    this.httpService.getOperatingEnvironments(this.filters).subscribe(res => {
      const data: any = res;

      if (!this.operatingEnvironmentData.length) {
        this.operatingEnvironmentData = data.length > 8 ? data.splice(0, 8) : data;
      } else {
        this.operatingEnvironmentData.map((d, i) => {
          const dataElem = ObjectOperation.getArrayElemByAttributeValue('category', d.category, data);
          for (const k in d) {
            d[k] = dataElem[k];
          }
          return d;
        });
      }

      const impactScoreRange = ObjectOperation.getMinMax(this.operatingEnvironmentData, 'totalImpactScore');
      this.impactScoreScale = D3Method.getLinearScale(impactScoreRange, [0.7, 1]);
      this.categoryLabelVisibility = true;
    });
  }

  navigationLink(data: any) {
    localStorage.setItem('SubCategoriesDetail', JSON.stringify(data));
    this.router.navigate(['/operative/subCategory']);
  }

  yearFilterSelected(yearID: any) {
    // localStorage.setItem('timeLineYearID', JSON.stringify(yearID));
    this.timelineFilter.expectedTimeId = yearID;
    this.filters.expectedTimeId = this.timelineFilter.expectedTimeId;
    this.httpService.getOperatingEnvironments(this.filters).subscribe(res => {
      const data: any = res;
      this.operatingEnvironmentData.map((d, i) => {
        const dataElem = ObjectOperation.getArrayElemByAttributeValue('category', d.category, data);
        for (const k in d) {
          d[k] = dataElem[k];
        }
        return d;
      });
      const impactScoreRange = ObjectOperation.getMinMax(this.operatingEnvironmentData, 'totalImpactScore');
      this.impactScoreScale = D3Method.getLinearScale(impactScoreRange, [0.7, 1]);
    });
  }

}
