import {Component, OnInit, Input} from '@angular/core';
import * as d3 from 'd3';
import {ObjectOperation} from '../../../helpers/commonUtils';
import {HttpServiceService} from '../../../services/https-service/https.service';

@Component({
  selector: 'app-trend-graphs',
  templateUrl: './trend-graphs.component.html',
  styleUrls: ['./trend-graphs.component.scss']
})
export class TrendGraphsComponent implements OnInit {
  @Input() chartData: any;
  public margin: any = {};
  width: any;
  height: any;
  private expectedTimes: any;

  constructor(private _httpService: HttpServiceService) {
    this.margin = {top: 20, right: 30, bottom: 60, left: 70},
      this.width = 450 - this.margin.left - this.margin.right,
      this.height = 350 - this.margin.top - this.margin.bottom;
  }

  ngOnInit() {
    this._httpService.getExpectedTimes().subscribe(res => {
      this.expectedTimes = res.items;
      this.draw(this.chartData);
    });
  }

  draw(data) {
    const chartContainer = d3.select('#chartContainer');
    const width = Number(chartContainer.style('width').replace('px', ''));
    const height = this.height;
    const expectedTimes = this.expectedTimes;
    const xAxisDomain = expectedTimes.length + 1;

    const svg = chartContainer
      .append('svg')
      .attr('width', width - 40)
      .attr('height', height + this.margin.top + this.margin.bottom)
      .append('g')
      .attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')');

    const defs = svg.append('defs');

    let gradient = defs.append('linearGradient')
      .attr('id', 'yAxisGradient')
      .attr('x1', '30%')
      .attr('x2', '100%')
      .attr('y1', '0%')
      .attr('y2', '100%');

    gradient.append('stop')
      .attr('class', 'start')
      .attr('offset', '0%')
      .attr('stop-color', '#FFF')
      .attr('stop-opacity', 0);

    gradient.append('stop')
      .attr('class', 'end')
      .attr('offset', '100%')
      .attr('stop-color', '#5484D1')
      .attr('stop-opacity', 1);

    gradient = defs.append('linearGradient')
      .attr('id', 'xAxisGradient')
      .attr('x1', '0%')
      .attr('x2', '100%')
      .attr('y1', '0%')
      .attr('y2', '100%');

    gradient.append('stop')
      .attr('class', 'start')
      .attr('offset', '0%')
      .attr('stop-color', '#5484D1')
      .attr('stop-opacity', 1);

    gradient.append('stop')
      .attr('class', 'end')
      .attr('offset', '65%')
      .attr('stop-color', '#FFF')
      .attr('stop-opacity', 0);

    gradient = defs.append('linearGradient')
      .attr('id', 'twoFacedCircle')
      .attr('x1', '100%')
      .attr('x2', '0%')
      .attr('y1', '0%')
      .attr('y2', '0%');

    gradient.append('stop').attr('offset', '50%').style('stop-color', '#f07878');
    gradient.append('stop').attr('offset', '50%').style('stop-color', '#78f088');

    // tslint:disable-next-line:max-line-length

    const yDomain = ObjectOperation.getMinMax(data, 'impactScore');
    const yStartOffset = -3;
    const yEndOffset = 0;
    yDomain[0] = yDomain[0] + yStartOffset;
    yDomain[1] = yDomain[1] + yEndOffset;

    function getExpectedTimeIDForChart(expectedTimeID) {
      for (let i = 0; i < expectedTimes.length; i++) {
        if (expectedTimes[i].id === expectedTimeID) {
          return i + 1;
        }
      }
      return 0;
    }

    // Read the data
    // Add X axis
    const x = d3.scaleLinear()
      .domain([0, xAxisDomain])
      .range([0, width - 50]);

    var xG = svg.append('g')
      .attr('class', 'xAxisScale')
      .attr('transform', 'translate(0,' + height + ')')
      .call(d3.axisBottom(x).ticks(xAxisDomain));

    xG.selectAll('text')
      .each(function (d) {
        if (d === 0 || d === xAxisDomain) {
          d3.select(this.parentNode).style('display', 'none');
        } else {
          d3.select(this).text(function () {
            // return (d === 1 ? '0-2' : d === 2 ? '2-5' : '5+') + ' Years';
            return expectedTimes[(d - 1)].yearRange;
            // yearRange
          });
        }
      });

    xG.select('path').attr('stroke', 'url(#xAxisGradient)').style('stroke-width', '2px').attr('d', function () {
      let pathData = d3.select(this).attr('d');
      pathData = pathData.replace('6V0', '0V0');
      pathData = pathData.replace('5H500', '0H500');
      pathData = pathData.replace('5V6', '5V3');
      return pathData;
    });

    // Add Y axis
    const y = d3.scaleLinear()
      .domain(yDomain)
      .range([height, 0]);
    const yG = svg.append('g')
      .attr('class', 'yAxisScale')
      .call(d3.axisLeft(y));

    const yGText = yG.selectAll('text');
    yGText.each(function (d, i) {
      if (i === 0) {
        d3.select(this).text(0);
      } else if (i === yGText.size() - 1) {
        d3.select(this).text(Math.ceil(yDomain[1]));
      } else {
        d3.select(this.parentNode).style('display', 'none');
      }
    });

    yG.select('path').attr('stroke', 'url(#yAxisGradient)').style('stroke-width', '2px').attr('d', function () {
      let pathData = d3.select(this).attr('d');
      pathData = pathData.replace('M-6', 'M-1');
      pathData = pathData.replace('5H-6', '5H-1');
      return pathData;
    });

    // add the Y gridlines
    svg.append('g')
      .attr('class', 'grid')
      .call(d3.axisLeft(y).ticks(5)
        .tickSize((-width + 140))
        .tickFormat('')
      )

    // Color scale: give me a specie name, I return a color
    const color = d3.scaleOrdinal()
      .domain(['threat', 'opportunity'])
      .range(['#f07878', '#78f088']);

    // Add dots
    const parentG = svg.append('g');
    const circles = parentG
      .selectAll('dot')
      .data(data)
      .enter()
      .append('circle')
      // tslint:disable-next-line:only-arrow-functions
      .attr('cx', function (d) {
        return x(getExpectedTimeIDForChart(d.expectedTime));
      })
      // tslint:disable-next-line:only-arrow-functions
      .attr('cy', function (d) {
        return y(d.impactScore);
      })
      .attr('r', 0)
      // tslint:disable-next-line:only-arrow-functions
      .style('fill', function (d) {
        if (d.category === 'threatOpportunity') {
          return 'url(#twoFacedCircle)';
        } else {
          return color(d.category);
        }
      }).style('stroke', '#fafbfc').style('stroke-width', '1px');

    circles.each(function (d) {
      parentG.append('text').text(function () {
        if (d.items) {
          return d.items.map(function (item) {
            return item.name;
          }).toString();
        } else {
          return undefined;
        }
      })
        .attr('x', function () {
          return x(getExpectedTimeIDForChart(d.expectedTime)) - 12;
        })
        .attr('y', function () {
          return y(d.impactScore) + 4;
        }).attr('class', 'trendChartCategoryText').style('opacity', 0);
    });

    circles.transition().ease(d3.easeBounce, 0).duration(1000).attr('r', 9).on('end', function () {
      parentG.selectAll('text').transition().duration(300).style('opacity', 1);
    });

  }

}
