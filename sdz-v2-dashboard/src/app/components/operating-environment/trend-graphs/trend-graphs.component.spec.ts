import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrendGraphsComponent } from './trend-graphs.component';

describe('TrendGraphsComponent', () => {
  let component: TrendGraphsComponent;
  let fixture: ComponentFixture<TrendGraphsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrendGraphsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrendGraphsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
