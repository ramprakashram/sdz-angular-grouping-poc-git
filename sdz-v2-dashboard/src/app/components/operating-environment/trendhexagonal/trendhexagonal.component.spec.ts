import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrendhexagonalComponent } from './trendhexagonal.component';

describe('TrendhexagonalComponent', () => {
  let component: TrendhexagonalComponent;
  let fixture: ComponentFixture<TrendhexagonalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrendhexagonalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrendhexagonalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
