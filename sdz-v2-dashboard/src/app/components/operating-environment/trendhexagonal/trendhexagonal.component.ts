import {Component, OnInit, Input, ViewChild, ElementRef, Output, EventEmitter} from '@angular/core';
import {Router} from '@angular/router';
import * as d3 from 'd3';

// import * as d3plus from 'd3plus-text';
@Component({
  selector: 'app-trendhexagonal',
  templateUrl: './trendhexagonal.component.html',
  styleUrls: ['./trendhexagonal.component.scss']
})
export class TrendhexagonalComponent implements OnInit {
  @Input() temp: string;
  @ViewChild('detailsViewSecond') searchElementSecond: ElementRef;
  @Output() countChangedSecond: EventEmitter<number> = new EventEmitter();
  @Input() tempSecond: any = {};
  public hexagonalTrendJson: any = {};
  public hexDetails: any = {};
  trendNametemp: string;
  trendNameone: string;
  trendNametwo: string;
  trendNamethree: any;
  showDiv: number;
  timeLineImapctYearID: any;
  isSplitWord: boolean;
  nameSpilt: any;
  countingText: any;
  isStringOne: boolean;
  isStringTwo: boolean;
  isStringThree: boolean;

  constructor(private router: Router) {
  }

  ngOnInit() {
    this.isStringOne = true;
    this.isStringTwo = true;
    this.isStringThree = true;
    this.hexagonalTrendJson = {
      name: '1',
    };
    this.hexDetails = this.temp;
    this.trendNametemp = this.hexDetails.trendDescription;
    // this.hexDetails.sliceTileName = this.hexDetails.trendName.slice(0, 30);
    // this.trendNameone = this.trendNametemp.slice(0, 40);
    // this.trendNametwo = this.trendNametemp.slice(40, 80);
    // this.trendNamethree = this.trendNametemp.slice(80, 120);
    // if (this.trendNametemp.length > 111) {
    //   this.trendNamethree = this.trendNamethree.concat('   ....');
    // }


    this.isSplitWord = true;
    this.nameSpilt = this.trendNametemp.split(' ');
    this.nameSpilt.join(' <br> ');
    this.countingText = 0;

    this.trendNameone = '';
    this.trendNametwo = '';
    this.trendNamethree = '';
    this.nameSpilt.forEach((item, index) => {
      if ((this.trendNameone.length < 45 && this.trendNameone.concat(' ', item).length < 50) && this.isStringOne) {
        this.trendNameone = this.trendNameone.concat(' ', item);
      } else {
        this.isStringOne = false;
        if ((this.trendNametwo.length < 45 && this.trendNametwo.concat(' ', item).length < 50) && this.isStringTwo) {
          this.trendNametwo = this.trendNametwo.concat(' ', item);
        } else {
          this.isStringTwo = false;
          if ((this.trendNamethree.length < 40 && this.trendNamethree.concat(' ', item).length < 45) && this.isStringThree) {
            this.trendNamethree = this.trendNamethree.concat(' ', item);
          } else {
            if (this.trendNametemp.length > 140 && this.isStringThree) {
              this.trendNamethree = this.trendNamethree.concat('  ....');
              this.isStringThree = false;
            } else {
              this.isStringThree = false;
            }

          }
        }

      }
    });
  }


  draw() {
    const chart = d3.select('.hexagonalDetailsSVG');
    // tslint:disable-next-line:only-arrow-functions
    //  d3.select('path.d').style('stroke', function(d) { return '#5484D1'; })
    // tslint:disable-next-line:only-arrow-functions
    // .style('stroke-width', function(d) { return '2.5'; } );
    // d3plus.textwrap()
    // .container(d3.select('#rectResize'))
    // .resize(true)
    // .draw();
  }

  onClickNavigation(trendID) {
    localStorage.setItem('trendIDForAPICall', JSON.stringify(trendID));
    this.timeLineImapctYearID = JSON.parse(localStorage.getItem('timeLineYearID'));
    localStorage.setItem('trendIDForTimeIDAPICall', JSON.stringify(this.timeLineImapctYearID));
    this.showDiv = 1;
    this.countChangedSecond.emit(this.showDiv);
  }
}
