import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AppComponent} from './app.component';
import {TopBarComponent} from './shared/top-bar/top-bar.component';
import {StartingComponentComponent} from './components/landingPage/starting-component/starting-component.component';
import {AuthGuardService} from './services/auth/auth-guard.service';

const routes: Routes = [{
  path: '',
  pathMatch: 'full',
  canActivate: [AuthGuardService],
  component: StartingComponentComponent,
  children: [
    {path: 'TopBarComponent', component: TopBarComponent},
    {path: 'AppComponent', component: AppComponent},
  ]
},
  {
    path: 'operative',
    loadChildren: './components/operating-environment/operating-environment.module#OperatingEnvironmentModule',
    data: {title: 'one'},
    canActivate: [AuthGuardService]
  },
  {
    path: 'threat_opportunity',
    loadChildren: './components/threats-opportunities/threats-opportunities.module#ThreatsOpportunitiesModule',
    data: {title: 'two'},
    canActivate: [AuthGuardService]
  },
  {
    path: 'organize',
    loadChildren: './components/organisation-impact/organisation-impact.module#OrganisationImpactModule',
    data: {title: 'three'},
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
