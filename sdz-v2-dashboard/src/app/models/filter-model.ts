import {AccordionWithCheckboxModel, CheckBoxModel} from '@gravityilabs/sdz-models';

export interface OperatingEnvironmentFilter {
  impactScoreIds: any[];
  likelihoodIds: any[];
  strategyIds: any[];
  capabilityIds: any[];
  divisionId: any;
  branchId: any;
  sectionId: any;
  expectedTimeId: any;
  trendSourceIds: any[];
}

export interface ImpactOrganisationFilter {
  capabilityGroupImpactScore: any;
  capabilityGroupName: any;
  capabilitiesCount: any;
  capabilityGroupId: any;
  expectedTimeId: any;
}
export interface ImpactOrganisationStrategicFilter {
  capabilityGroupImpactScore: any;
  capabilityGroupName: any;
  capabilitiesCount: any;
  capabilityGroupId: any;
  expectedTimeId: any;
}


export interface ThreatChartData {
  expectedTime: number;
  impactScore: number;
  category: string;
  items: ChartItem[];
}

export interface ChartItem {
  id: number;
  name: string;
}

// export class CapabilityStrategyModel {
//   id: number;
//   companyId: number;
//   name: string;
//   status: number;
// }

export class CapabilityGroupModel {
  id: number;
  name: string;
  capabilities: Array<CheckBoxModel>;
}

export class StrategyGroupModel {
  id: number;
  name: string;
  strategies: Array<CheckBoxModel>;
}

export interface FilterInterface {
  branches: Array<ChartItem>;
  capabilityGroups: Array<CapabilityGroupModel>;
  divisions: Array<ChartItem>;
  impactScores: Array<ChartItem>;
  likelihoods: Array<ChartItem>;
  sections: Array<ChartItem>;
  strategyGroups: Array<StrategyGroupModel>;
  trendSources: Array<ChartItem>;
}

export class FilterModel implements FilterInterface {
  branches: Array<ChartItem>;
  capabilityGroups: Array<CapabilityGroupModel>;
  divisions: Array<ChartItem>;
  impactScores: Array<ChartItem>;
  likelihoods: Array<ChartItem>;
  sections: Array<ChartItem>;
  strategyGroups: Array<StrategyGroupModel>;
  trendSources: Array<ChartItem>;
}

// import { MultiSelectCheckBoxModel, AccordionWithCheckboxModel } from '@gravityilabs/sdz-models'
// import { ObjectUtil } from '../../assets/utils/object-util';

export class FilterAccordionModel {
  accordionData: Array<AccordionFilterArray>;

//     constructor(filterModel ?: FilterModel) {
//         if(filterModel) {
//             this.deserialize(filterModel as FilterModel)
//         }
//     }
//
//     private deserialize(filterModel : FilterModel) {
//         const keys = Object.keys(this)
//         for(const key of keys) {
//             if(filterModel.hasOwnProperty(key)) {
//                 switch(key) {
// tslint:disable-next-line:max-line-length
//                     case "checkboxFilters" : this[key] = ObjectUtil.renderArray<MultiSelectCheckBoxModel>(filterModel[key],MultiSelectCheckBoxModel);break;
// tslint:disable-next-line:max-line-length
//                     case "accordionFilters" : this[key] = ObjectUtil.renderArray<AccordionFilterArray>(filterModel[key],AccordionFilterArray);break;
//                     default: this[key] = filterModel[key]
//                 }
//             }
//         }
//     }
}

export class AccordionFilterArray {
  id: number;
  title: string;
  accordionFilterData: Array<AccordionWithCheckboxModel>;

//     constructor(accordionFilterArray ?: AccordionFilterArray) {
//         if(accordionFilterArray) {
//             this.deserialize(accordionFilterArray as AccordionFilterArray)
//         }
//     }
//
//     private deserialize(accordionFilterArray : AccordionFilterArray) {
//         const keys = Object.keys(this)
//         for(const key of keys) {
//             if(accordionFilterArray.hasOwnProperty(key)) {
//                 switch(key) {
// tslint:disable-next-line:max-line-length
//                     case "accordionFilterData" : this[key] = ObjectUtil.renderArray<AccordionWithCheckboxModel>(accordionFilterArray[key],AccordionWithCheckboxModel);break;
//                     default: this[key] = accordionFilterArray[key]
//                 }
//             }
//         }
//     }
}

