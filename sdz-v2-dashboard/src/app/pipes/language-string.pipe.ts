import { Pipe, PipeTransform } from '@angular/core';
import {LanguageStringService} from '../services/languageString/language-string.service';

@Pipe({
  name: 'languageString',
  pure: false
})
export class LanguageStringPipe implements PipeTransform {
  constructor(private languageStringService: LanguageStringService) {}

  transform(languageText: any, moduleType?: any): string {
    return this.languageStringService.translate(languageText, moduleType);
  }

}
