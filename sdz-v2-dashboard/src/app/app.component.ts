import {Component, OnInit} from '@angular/core';
import {LanguageStringService} from './services/languageString/language-string.service';
import {HostListener} from '@angular/core';
import {Subject} from 'rxjs';
import {AuthService} from './services/auth/auth.service';
import {Router} from '@angular/router';
import {AuthGuardService} from './services/auth/auth-guard.service';
import {PlatformLocation} from '@angular/common';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  windowHeight;
  screenHeight: any;
  screenWidth: any;
  isAuthenticated = false;

  userActivity;
  userInactive: Subject<any> = new Subject();

  constructor(
    private languageStringService: LanguageStringService,
    private auth: AuthService,
    private platform: PlatformLocation,
    private guard: AuthGuardService,
    private router: Router
  ) {
    router.events.subscribe(() => {
      this.isAuthenticated = this.auth.isAuthenticated();
    });
    // LogOut User if Inactive
    this.userInactiveTimeOut();
     this.languageStringService.getLanguageStringFromServer();
    this.userInactive.subscribe(() => this.logOut());
  }

  logOut() {
    this.auth.purgeAuth();
    let authBaseUrl: string;
    const origin: string = (this.platform as any).location.origin;
    // if (origin.indexOf('localhost') >= 0) {
    //   authBaseUrl = 'https://sdzdev-hs.azurewebsites.net';
    // } else {
      authBaseUrl = origin;
    // }
    window.location.href = `${authBaseUrl}/account/logout`;
  }

  userInactiveTimeOut(timer = 10000 * 60 * 5) {
    this.userActivity = setTimeout(() => this.userInactive.next(undefined), timer);
  }

  @HostListener('window:mousemove') refreshUserState() {
    clearTimeout(this.userActivity);
    this.userInactiveTimeOut(10000 * 60 * 15);
  }

  // if User Switches Tab
  // @HostListener('window:visibilitychange') isUserOnDifferentTab() {
  //   if (document.hidden) {
  //     clearTimeout(this.userActivity);
  //     this.userInactiveTimeOut(1000 * 60);
  //   }
  // }

  // @HostListener('window:resize', ['$event']) updateTitle() {
  //   this.title = 'Horizon-Scanner';
  // }

  ngOnInit() {
    this.getScreenSize();

  }

  getScreenSize(event?) {
    this.screenHeight = window.innerHeight;
    this.screenWidth = window.innerWidth;
    if (700 > this.screenHeight || this.screenHeight < 850) {
      this.windowHeight = 0.7;
      localStorage.setItem('windowOuterHeight', JSON.stringify(this.windowHeight));
    } else {
      this.windowHeight = 1;
      localStorage.setItem('windowOuterHeight', JSON.stringify(this.windowHeight));
    }
  }
}
