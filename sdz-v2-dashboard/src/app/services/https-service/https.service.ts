import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {SelectedFiltersModel} from '../../shared/filter/filter.component';

@Injectable()
export class HttpServiceService {
  private HORIZAN_URL = '/hsapi';

  constructor(private http: HttpClient) {
  }

  //

  getHorizonSummary() {
    const url = `${this.HORIZAN_URL}/horizonscanning/summary`;
    return this.http.get(url);
  }

  getOperatingEnvironments(data) {
    const url = `${this.HORIZAN_URL}/horizonscanning/operatingenvironment`;
    return this.http.post(url, data);
  }

  getLanguageString(){
    const url = `${this.HORIZAN_URL}/LanguageStrings`;
    return this.http.get(url);
  }


  getTrendImpactedSummary(trendId, timeID, capabilityID, strategyId) {
    // tslint:disable-next-line:max-line-length
    const url = `${this.HORIZAN_URL}/horizonscanning/operatingenvironment/trends/${trendId}` + (timeID === null ? '' : '?expectedTimeId=' + timeID) + (capabilityID === null ? '' : '?capabilityId=' + capabilityID) + (strategyId === null ? '' : '?strategyId=' + strategyId);
    // @ts-ignore
    return this.http.get<any[]> (url, trendId);
  }

  getTrendImpactedSummaryBasedOnFilters(trendId: number, filters: any) {
    const url = `${this.HORIZAN_URL}/horizonscanning/operatingenvironment/trends/${trendId}`;
    return this.http.post<any>(url, filters);
  }


  getHorizonThreatsandOpportunities(data) {
    const url = `${this.HORIZAN_URL}/horizonscanning/threatsandopportunities` + (data === null ? '' : '?expectedTimeId=' + data);
    return this.http.post<any>(url, data);
  }

  getOpportunityCategories(trendId, timeID) {
    // tslint:disable-next-line:max-line-length
    const url = `${this.HORIZAN_URL}/horizonscanning/threatsandopportunities/opportunitycategories/${trendId}` + (timeID === null ? '' : '?expectedTimeId=' + timeID);
    return this.http.post(url, trendId);
  }

  getThreatCategories(trendId, timeID) {
    // tslint:disable-next-line:max-line-length
    const url = `${this.HORIZAN_URL}/horizonscanning/threatsandopportunities/threatcategories/${trendId}` + (timeID === null ? '' : '?expectedTimeId=' + timeID);
    return this.http.post(url, trendId);
  }

  getOrganisationImpactStrategicStreams() {
    const url = `${this.HORIZAN_URL}/horizonscanning/organisationimpact/strategicstreams`;
    return this.http.get(url);
  }

  getOrganisationImpactCapabilityStreams(data) {
    // tslint:disable-next-line:max-line-length
    const url = `${this.HORIZAN_URL}/horizonscanning/organisationimpact/capabilitystreams` + (data === null ? '' : '?expectedTimeId=' + data);
    return this.http.get<any[]>(url);
  }


  getStrategicStreamsummary(data) {
    // tslint:disable-next-line:max-line-length
    const url = `${this.HORIZAN_URL}/horizonscanning/organisationimpact/strategicstreams` + (data === null ? '' : '?expectedTimeId=' + data);
    return this.http.get<any[]>(url);
  }


  getCapabilityGroupSummary(groupId, data) {
    // tslint:disable-next-line:max-line-length
    const url = `${this.HORIZAN_URL}/horizonscanning/organisationimpact/capabilitystreams/${groupId}` + (data === null ? '' : '?expectedTimeId=' + data);
    return this.http.post<any> (url, data);
  }

  getStrategicGroupSummary(groupId, data) {
    // tslint:disable-next-line:max-line-length
    const url = `${this.HORIZAN_URL}/horizonscanning/organisationimpact/strategicstreams/${groupId}` + (data === null ? '' : '?expectedTimeId=' + data);
    return this.http.post<any> (url, data);
  }

  getCapabilityLinkedSummary(groupId, data) {
    // tslint:disable-next-line:max-line-length
    const url = `${this.HORIZAN_URL}/horizonscanning/organisationimpact/capabilitylinkedsummary/${groupId}` + (data === null ? '' : '?expectedTimeId=' + data);
    return this.http.post<any[]> (url, data);
  }

  getStrategyLinkedSummary(groupId, data) {
    // tslint:disable-next-line:max-line-length
    const url = `${this.HORIZAN_URL}/horizonscanning/organisationimpact/strategylinkedsummary/${groupId}` + (data === null ? '' : '?expectedTimeId=' + data);
    return this.http.post<any[]> (url, data);
  }
  getExpectedTimes() {
    const url = `${this.HORIZAN_URL}/ExpectedTimes`;
    return this.http.get<any>(url);
  }
  getThreatByID(threatId) {
    const url = `${this.HORIZAN_URL}/horizonscanning/organisationimpact/threat/${threatId}`;
    return this.http.get(url);
  }
  getOpportunityByID(opportunityId) {
    const url = `${this.HORIZAN_URL}/horizonscanning/organisationimpact/opportunity/${opportunityId}`;
    return this.http.get(url);
  }

  getFilters(filter: any) {
    return this.http.post(`${this.HORIZAN_URL}/horizonscanning/operatingenvironment/filters/`, filter);
  }
}
