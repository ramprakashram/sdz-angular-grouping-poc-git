import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpResponse,
  HttpErrorResponse
} from '@angular/common/http';
import {Observable, throwError, of, EMPTY} from 'rxjs';
import {retry, catchError, finalize, map, tap} from 'rxjs/operators';
import {ObjectOperation} from '../../helpers/commonUtils';
import {Injectable} from '@angular/core';

import {LoaderService} from '../loaders/loader.service';
import {AuthGuardService} from '../auth/auth-guard.service';

@Injectable({
  providedIn: 'root'
})
export class HttpAppInterceptor implements HttpInterceptor {
  constructor(public loader: LoaderService, private guard: AuthGuardService) {
  }

  private cache = new Map<string, any>();

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token = this.guard.auth.getToken();
    // Caching for better performance
    const cachedResponse = this.cache.get(request.url);
    if (cachedResponse) {
      return of(cachedResponse);
    }

    // Show loader
    this.showLoader();

    let newRequest: any;
    if (request.url.indexOf('/hsapi/') >= 0) {
      if (token) {
        newRequest = request.clone({
          setHeaders: {
            Authorization: `Bearer ${token}`,
            withCredentials: 'true',
            'Cache-Control': 'no-cache',
            Pragma: 'no-cache',
            Expires: 'Sat, 01 Jan 2000 00:00:00 GMT'
          }
        });
      } else {
        // no token no req no worries
        return EMPTY;
      }
    } else {
      newRequest = request;
    }

    return next.handle(newRequest)
      .pipe(
        // retry(1),
        tap(event => {
          if (event instanceof HttpResponse) {
            if (this.checkIfCachingRequired(request.url)) {
              this.cache.set(request.url, event);
            }
          }
        }),
        map((event: HttpEvent<any>) => {
          // Converting response structure
          if (event instanceof HttpResponse) {
            return event;
          }
        }),
        catchError((error: HttpErrorResponse) => {
          if (Number(error.status) === 401) {
            this.guard.logout();
          }
          let errorMessage = '';
          if (error.error instanceof ErrorEvent) {
            // client-side error
            errorMessage = `Error: ${error.error.message}`;
          } else {
            // server-side error
            errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
          }
          return throwError(errorMessage);
        }),
        finalize(() => this.hideLoader())
      );
  }

  private showLoader(): void {
    this.loader.show();
  }

  private hideLoader(): void {
    this.loader.hide();
  }

  private checkIfCachingRequired(url) {
    const cachingExcludedAPIs = [
      '/hsapi/horizonscanning/operatingenvironment',
      '/hsapi/horizonscanning/threatsandopportunities',
      '/hsapi/horizonscanning/organisationimpact/capabilitystreams',
      '/hsapi/horizonscanning/organisationimpact/strategicstreams',
      '/hsapi/horizonscanning/operatingenvironment/filters/'
    ];

    if (url.indexOf('/hsapi/horizonscanning/organisationimpact/capabilitystreams') !== -1 && url.indexOf('?') === -1 ) {
      return false;
    }
    if (url.indexOf('/hsapi/horizonscanning/organisationimpact/strategicstreams') !== -1 && url.indexOf('?') === -1 ) {
      return false;
    }
    if (url.indexOf('/hsapi/horizonscanning/organisationimpact/strategicstreams') !== -1) {
      return false;
    }
    return ObjectOperation.inArray(url, cachingExcludedAPIs) === -1;
  }

}
