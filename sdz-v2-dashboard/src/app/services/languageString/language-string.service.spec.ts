import { TestBed } from '@angular/core/testing';

import { LanguageStringService } from './language-string.service';

describe('LanguageStringService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LanguageStringService = TestBed.get(LanguageStringService);
    expect(service).toBeTruthy();
  });
});
