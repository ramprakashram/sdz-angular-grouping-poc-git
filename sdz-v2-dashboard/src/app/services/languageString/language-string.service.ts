import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {HttpServiceService  } from '../https-service/https.service';
@Injectable({
  providedIn: 'root'
})

export class LanguageStringService {

  private languageStringList: any = [];

  constructor(private HttpServiceService: HttpServiceService) {
  }


  getLanguageStringFromServer() {
    if (!this.languageStringList.length) {
      this.HttpServiceService.getLanguageString().subscribe(res => {
        this.languageStringList = res;
      });
    }

  }

  translate(key: string, module: string): string {
    if (this.languageStringList && this.languageStringList.items instanceof Array) {
      const languageEnglish = this.languageStringList.items.filter(name => name.labelName === key );
      return languageEnglish.length ? languageEnglish[0].languageEnglish : key;
    }
  }

}
