import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {AuthService} from './auth.service';
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {PlatformLocation} from '@angular/common';
import {LanguageStringService} from '../../services/languageString/language-string.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {
  authBaseUrl = '';
  redirectPath: string;
  loginRedirect: string;

  constructor(public auth: AuthService, private router: Router, private http: HttpClient, private platform: PlatformLocation,  private languageStringService: LanguageStringService,) {
    const origin: string = (this.platform as any).location.origin;
    // if (origin.indexOf('localhost') >= 0) {
    //   this.authBaseUrl = 'https://sdzdev-hs.azurewebsites.net';
    // } else {
      this.authBaseUrl = origin;
    // }
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    this.redirectPath = state.url;
    this.loginRedirect = location.href;
    // check whether we have a valid token in storage
    if (!this.auth.isAuthenticated()) {
      // No token already or we have an expired one so try to get from SDZ
      // API call to Login
      this.sdzLogin()
        .then(
          (isGotAuthorised) => {
            if (isGotAuthorised && isGotAuthorised === true) {
              // return leads to unknown state so do router navigate
              this.languageStringService.getLanguageStringFromServer();
              this.router.navigate([this.redirectPath]).then(() => {
              });
            } else {
              this.logout();
            }
          }, () => {
            this.logout();
            // we will never reach below return if Auth Failed because App redirects to SDZ
            return false;
          }
        );
    } else {
      // Already have valid token say cheese!!
      return of(true);
    }
  }

  sdzLogin() {
    // let newAuth: TokenResponse | null;
    return new Promise((resolve, reject) => {
      this.http.get(`${this.authBaseUrl}/account/ImplicitLogin`, {
        withCredentials: true,
        responseType: 'json'
      })
        .toPromise()
        .then(
          auth => {
            // Got something it might be token
            // const newAuth = new TokenResponse(auth);
            // Try to set a session with new Auth Received
            // we will get true if everything goes good
            resolve(this.newSession(auth));
          },
          () => {
            // Any Failure
            reject();
          }
        );
    });
  }

  newSession(token: any): boolean {
    // clear if any existing token, can be the expired one
    this.revokeAuth();
    // set new token if it is valid one
    if (token !== null) {
      this.auth.setToken(token);
    } else {
      // invalid token then call for login
      this.callSDZLogin();
      // we will never reach below return if Auth Failed because App redirects to SDZ
      return true;
    }
    return true;
  }

  logout() {
    // remove Token and Call login
    this.revokeAuth();
    this.callSDZLogin();
  }

  revokeAuth() {
    this.auth.purgeAuth();
  }

  callSDZLogin(sdz = `${this.authBaseUrl}/account/login?refUrl=${this.loginRedirect}`) {
    // Go home buddy
    window.location.href = sdz;
  }
}
