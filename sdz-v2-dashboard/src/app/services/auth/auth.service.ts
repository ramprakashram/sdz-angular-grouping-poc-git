import {Injectable} from '@angular/core';
import {JwtHelperService} from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  tokenName = 'hsSdzAuth';
  jwtHelper: JwtHelperService = new JwtHelperService();

  constructor() {
   //  localStorage.setItem("hsSdzAuth", "eyJhbGciOiJSUzI1NiIsImtpZCI6IjlCRjU2OTBCNzBCOTlDNTFENjk3RDg3RTMyMzhDQTBFRTlEQkEzNjUiLCJ4NXQiOiJtX1ZwQzNDNW5GSFdsOWgtTWpqS0R1bmJvMlUiLCJ0eXAiOiJKV1QifQ.eyJzZHpjb21wYW55aWQiOiIzMTg1Iiwic2R6dXNlcmlkIjoiMSIsInNkenJvbGVpZCI6IjIiLCJuYmYiOjE1NjM4NzQxMzgsImV4cCI6MTU2Mzg4MTY1MiwiaWF0IjoxNTYzODc0MTM4LCJpc3MiOiJTZHoifQ.6J_bpQg1zlh6yS2_VtzhpWXThGOlk8Y76hYa1oDHbir1ba1Hfj5scASWUYsV0Q6ATaM38f7ly3Mr9JFS4p41-hoiJmE3MPD2r1tST5IM2JjWLrRbukBc7jH2Ds6y3jpIyf6HrTuNGSgE6sjhlw8Mz7SVMHl9OpCSOSE_xnNvKl13GjFmF5-fs-5hqY2fhxy5OimD_bNs81FIIfpp5aKWDVu3aE-fdGb-XF3LmDTvHqBE9NkuX14C51WmoEB2xuMkKd3xut0O3XD_nWNV-i9g4rzZmK6N705_VDTUU3pDistQY3S99GF8iwNSvmrGFp3mFQC-n83saQkt1YHlJzuhuA");
  }

  public isAuthenticated(): boolean {
    // Check whether the token is expired and return
    if (this.getToken() !== null) {
      const token = this.getToken();
      return !this.jwtHelper.isTokenExpired(token);
    } else {
      return false;
    }
  }

  public setToken(token: any): any {
    return localStorage.setItem(this.tokenName, token);
  }

  public getToken(): any {
    return localStorage.getItem(this.tokenName);
  }

  public purgeAuth(): any {
    return localStorage.removeItem(this.tokenName);
  }

  public getUserId(): any {
    return this.jwtHelper.decodeToken(this.getToken()).sdzuserid;
  }
}
