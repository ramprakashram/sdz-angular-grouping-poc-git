import {Injectable} from '@angular/core';
import {ChartItem, OperatingEnvironmentFilter, ThreatChartData} from '../../models/filter-model';
// tslint:disable-next-line:import-spacing
import {ImpactOrganisationFilter} from '../../models/filter-model';
import {ImpactOrganisationStrategicFilter} from '../../models/filter-model';

import {BehaviorSubject} from 'rxjs';
import {SelectedFiltersModel} from '../../shared/filter/filter.component';
import {filter} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class FiltersService {
  private operatingEnvironmentFilters: OperatingEnvironmentFilter = {
    impactScoreIds: [],
    likelihoodIds: [],
    strategyIds: [],
    capabilityIds: [],
    divisionId: null,
    branchId: null,
    sectionId: null,
    expectedTimeId: null,
    trendSourceIds: []
  };
  private impactOrganisationFilter: ImpactOrganisationFilter = {
    capabilityGroupImpactScore: null,
    capabilityGroupName: null,
    capabilitiesCount: null,
    capabilityGroupId: null,
    expectedTimeId: null,
  };
  private impactOrganisationStrategicFilter: ImpactOrganisationStrategicFilter = {
    capabilityGroupImpactScore: null,
    capabilityGroupName: null,
    capabilitiesCount: null,
    capabilityGroupId: null,
    expectedTimeId: null,
  };
  private ThreatChartData: ThreatChartData = {
    expectedTime: null,
    impactScore: null,
    category: null,
    items: [],
  };

  constructor() {
  }

  private filterChanged = new BehaviorSubject({});
  currentFilter = this.filterChanged.asObservable();

  private isFilterCompleted = new BehaviorSubject(false);
  currentFilterState = this.isFilterCompleted.asObservable();

  updateFilters(updatedFilter: SelectedFiltersModel) {
    this.filterChanged.next(updatedFilter);
  }

  updateFilterState(state: boolean) {
    this.isFilterCompleted.next(state);
  }

  getOperatingEnvironmentFilters() {
    return this.operatingEnvironmentFilters;
  }

  ThreatOpportunityData() {
    return this.ThreatChartData;
  }

  getimpactOrganisationFilter() {
    return this.impactOrganisationFilter;
  }
  getimpactOrganisationStrategicFilter() {
    return this.impactOrganisationStrategicFilter;
  }
}
