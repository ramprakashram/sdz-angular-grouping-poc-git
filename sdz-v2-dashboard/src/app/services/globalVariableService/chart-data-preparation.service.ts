import {Injectable} from '@angular/core';
import {ThreatChartData} from '../../models/filter-model';
import { OrderByPipe } from 'src/app/pipes/order-by.pipe';
import { NamingMethod } from 'src/app/helpers/commonUtils';

@Injectable({
  providedIn: 'root'
})
export class ChartDataPreparationService {

  constructor() {
  }

  getThreatElementExpectedTime(threatsByExpectedTimeObject, threatID) {
    for (const i in threatsByExpectedTimeObject) {
      for (let j = 0; j < threatsByExpectedTimeObject[i].length; j++) {
        if (threatsByExpectedTimeObject[i][j].id === threatID) {
          return threatsByExpectedTimeObject[i][j];
        }
      }
    }
  }

  checkIfThreatOpportunityExists(arr, expectedTime, impactScore) {
    return arr.filter(d => {
      return d.expectedTime === expectedTime && d.impactScore === impactScore;
    })[0];
  }

  getChartData(sortedData, rawData) {
    // console.log('z', sortedData);
    // console.log('y', rawData);
    const chartData: ThreatChartData[] = [];
    for (let i = 0; i < sortedData.length; i++) {
      const data = sortedData[i];
      const threatExpectedTimeObj = this.getThreatElementExpectedTime(data.category === 'threat' ?
      rawData.threatsByExpectedTimeId : rawData.opportunitiesByExpectedTimeId, data.id);
      // console.log(rawData.threatsByExpectedTimeId, data.id);
      // console.log('c', threatExpectedTimeObj);
      const expectedTimeID = Number(threatExpectedTimeObj.expectedTimeId);
      const existingChartData = this.checkIfThreatOpportunityExists(chartData, expectedTimeID, data.impactScore);
      if (existingChartData) {
        existingChartData.items.push({
          id: 1,
          name: NamingMethod.alphabetBullets(i)
        });

        if (existingChartData.category === data.category) {
          existingChartData.category = 'threatOpportunity';
        }
      } else {
        chartData.push({
          expectedTime: Number(threatExpectedTimeObj.expectedTimeId),
          impactScore: data.impactScore,
          category: data.category,
          items: [{id: 1, name: NamingMethod.alphabetBullets(i)}]
        });
      }
    }

    // console.log('test', chartData);
    return chartData;
  }

  getThreatOpportunityChartData(rawData) {
    const chartData: ThreatChartData[] = [];
    const threatSummaries = rawData.threatSummaries;
    // this.sortThreatOpportunities(rawData.threatSummaries);
    const opportunitySummaries = rawData.opportunitySummaries;

    for (let i = 0; i < threatSummaries.length; i++) {
      const threat = threatSummaries[i];
      const threatExpectedTimeObj = this.getThreatElementExpectedTime(rawData.threatsByExpectedTimeId, threat.id);
      const expectedTimeID = Number(threatExpectedTimeObj.expectedTimeId);
      const existingChartData = this.checkIfThreatOpportunityExists(chartData, expectedTimeID, threat.impactScore);
      if (existingChartData) {
        existingChartData.items.push({
          id: 1,
          name: threat.shortName
        });
      } else {
        chartData.push({
          expectedTime: Number(threatExpectedTimeObj.expectedTimeId),
          impactScore: threat.impactScore,
          category: 'threat',
          items: [{id: 1, name: threat.shortName}]
        });
      }
    }


    for (let i = 0; i < opportunitySummaries.length; i++) {
      const opportunity = opportunitySummaries[i];
      const opportunityExpectedTimeObj = this.getThreatElementExpectedTime(rawData.opportunitiesByExpectedTimeId, opportunity.id);
      const expectedTimeID = Number(opportunityExpectedTimeObj.expectedTimeId);
      const existingChartData = this.checkIfThreatOpportunityExists(chartData, expectedTimeID, opportunity.impactScore);
      if (existingChartData) {
        existingChartData.items.push({
          id: 1,
          name: opportunity.shortName
        });

        if (existingChartData.category === 'threat') {
          existingChartData.category = 'threatOpportunity';
        }
      } else {
        chartData.push({
          expectedTime: Number(opportunityExpectedTimeObj.expectedTimeId),
          impactScore: opportunity.impactScore,
          category: 'opportunity',
          items: [{id: 1, name: opportunity.shortName}]
        });
      }
    }

    // console.log('original', chartData)

    return chartData;
  }
}
