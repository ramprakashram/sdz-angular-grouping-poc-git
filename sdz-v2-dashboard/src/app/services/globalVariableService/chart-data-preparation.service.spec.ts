import { TestBed } from '@angular/core/testing';

import { ChartDataPreparationService } from './chart-data-preparation.service';

describe('ChartDataPreparationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ChartDataPreparationService = TestBed.get(ChartDataPreparationService);
    expect(service).toBeTruthy();
  });
});
