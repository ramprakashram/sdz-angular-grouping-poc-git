import { TestBed } from '@angular/core/testing';

import { LandingPageAnimationService } from './landing-page-animation.service';

describe('LandingPageAnimationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LandingPageAnimationService = TestBed.get(LandingPageAnimationService);
    expect(service).toBeTruthy();
  });
});
